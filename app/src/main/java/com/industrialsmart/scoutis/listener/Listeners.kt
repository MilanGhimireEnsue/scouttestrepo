package com.industrialsmart.scoutis.listener

import com.industrialsmart.scoutis.models.GDUDataItem
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.models.Sensor
import com.industrialsmart.scoutis.models.ToBeCompared

interface AzureLoginResponseListener {
    fun onSuccess(token: String)
    fun onError(error: String)
}

interface DateRangeListener {
    fun onDateRangeSelected(startDate: String?, endDate: String?)
}

interface CompareItemListener {
    fun onCompareItemClicked(toComparedList: List<ToBeCompared?>?)
}

interface ScoutClickListener {
    fun onScoutClicked(scoutModel: ScoutModel?)
    fun onScoutSettingClicked(scoutModel: ScoutModel?)
    fun onScoutNotificationClicked(scoutModel: ScoutModel?)
    fun onAddScoutNotificationClicked(scoutModel: ScoutModel?)
    fun onSensorClicked(scoutModel: ScoutModel?, sensor: Sensor?)
}

interface EditDeleteListener {
    fun onClicked(dataItem: Any?)
    fun onEdit(dataItem: Any?)
    fun onDelete(dataItem: Any?)
}

interface UIEventListener {
    fun onRightButtonClicked()
    fun onRightIconClicked()
}