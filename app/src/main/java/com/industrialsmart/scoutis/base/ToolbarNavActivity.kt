package com.industrialsmart.scoutis.base

import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.listener.UIEventListener
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.view.appinfo.AppInfoFragment
import com.industrialsmart.scoutis.view.dashboard.calendar.addevent.AddEventFragment
import com.industrialsmart.scoutis.view.dashboard.map.ISMapsFragment
import com.industrialsmart.scoutis.view.dashboard.notification.NotificationFragment
import com.industrialsmart.scoutis.view.dashboard.notification.setting.AddNotificationSettingFragment
import com.industrialsmart.scoutis.view.dashboard.notification.setting.NotificationSettingFragment
import com.industrialsmart.scoutis.view.gdu.AddGDUFragment
import com.industrialsmart.scoutis.view.gdu.GDUFragment
import com.industrialsmart.scoutis.view.references.ReferenceFragment
import com.industrialsmart.scoutis.view.scout.setting.ScoutSettingFragment
import com.industrialsmart.scoutis.view.setting.UserSettingFragment
import com.industrialsmart.scoutis.view.webview.WebViewFragment
import kotlinx.android.synthetic.main.activity_toolbar_nav.*
import org.threeten.bp.LocalDate


class ToolbarNavActivity : BaseActivity() {
    override val layoutId = R.layout.activity_toolbar_nav

    var viewType: ViewType? = null

    override fun initView() {
        super.initView()
        backButton.setOnClickListener {
            onBackPressed()
        }
        addButton.setOnClickListener {
            val uiEventListener = supportFragmentManager.findFragmentById(R.id.container) as UIEventListener
            uiEventListener.onRightIconClicked()
        }
        rightButton.setOnClickListener {
            val uiEventListener = supportFragmentManager.findFragmentById(R.id.container) as UIEventListener
            uiEventListener.onRightButtonClicked()
        }
        openView()
    }

    private fun openView() {
        //return from here if no intent type is present
        if (!intent.hasExtra(VIEW_TYPE_INTENT))
            return
        viewType = intent?.extras?.getSerializable(VIEW_TYPE_INTENT) as ViewType
        when (intent?.extras?.getSerializable(VIEW_TYPE_INTENT) as ViewType) {
            ViewType.SCOUT_SETTING -> changeFragment(
                ScoutSettingFragment.newInstance(intent.getParcelableExtra<ScoutModel?>(EXTRA_SCOUT_DATA)),
                addToBackStack = false
            )
            ViewType.GDU_LISTING -> changeFragment(GDUFragment.newInstance(), addToBackStack = false)
            ViewType.ADD_GDU -> changeFragment(AddGDUFragment.newInstance(), addToBackStack = false)
            ViewType.USER_SETTING -> changeFragment(UserSettingFragment.newInstance(), addToBackStack = false)
            ViewType.ADD_EVENT -> changeFragment(
                AddEventFragment.newInstance(intent.getSerializableExtra(EXTRA_DATE_DATA) as LocalDate?, intent.getParcelableExtra(EXTRA_PARAM_1)),
                addToBackStack = false
            )
            ViewType.NOTIFICATION_SETTING -> changeFragment(NotificationSettingFragment.newInstance(), addToBackStack = false)
            ViewType.ADD_NOTIFICATION_SETTING -> changeFragment(
                AddNotificationSettingFragment.newInstance(
                    intent.getParcelableExtra<ScoutModel?>(
                        EXTRA_SCOUT_DATA
                    )
                ), addToBackStack = false
            )
            ViewType.REFERENCE -> changeFragment(ReferenceFragment.newInstance(), addToBackStack = false)
            ViewType.NOTIFICATION -> changeFragment(
                NotificationFragment.newInstance(intent.getParcelableExtra<ScoutModel?>(EXTRA_SCOUT_DATA)),
                addToBackStack = false
            )
            ViewType.WEB_VIEW -> changeFragment(
                WebViewFragment.newInstance(
                    intent.getStringExtra(EXTRA_PARAM_1),
                    intent.getStringExtra(EXTRA_PARAM_2)
                ), addToBackStack = false
            )
            ViewType.APP_INFO -> changeFragment(AppInfoFragment.newInstance(), addToBackStack = false)
            ViewType.MAP_WITH_SCOUT -> changeFragment(ISMapsFragment.newInstance(intent.getParcelableExtra<ScoutModel?>(EXTRA_SCOUT_DATA)), addToBackStack = false)
        }
    }

    fun setToolbarTitle(title: String?, rightIcon: Int? = null, rightText: String? = null) {
        pageTitle.text = title
        if (rightIcon != null) {
            addButton.setImageResource(rightIcon)
            addButton.show()
        } else
            addButton.hide()
        if (rightText.isNullOrEmpty()) {
            rightButton.hide()
        } else {
            rightButton.show()
            rightButton.text = rightText
        }
    }

    fun showToolbar() {
        headerLayout.show()
    }

    fun hideToolbar() {
        headerLayout.hide()
    }

    companion object {
        val VIEW_TYPE_INTENT = "view_type_intent"
        val EXTRA_SCOUT_DATA = "extra_scout_data"
        val EXTRA_DATE_DATA = "extra_date_data"
        val EXTRA_PARAM_1 = "extra_param_1"
        val EXTRA_PARAM_2 = "extra_param_2"
    }
}