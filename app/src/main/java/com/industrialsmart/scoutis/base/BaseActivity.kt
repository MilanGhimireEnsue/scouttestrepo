package com.industrialsmart.scoutis.base

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Resources
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.industrialsmart.scoutis.MyApplication
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.interfaces.DataSource
import com.tbruyelle.rxpermissions2.RxPermissions
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import io.reactivex.disposables.CompositeDisposable


/**
 * Created by rakezb on 4/19/2020.
 */
abstract class BaseActivity : AppCompatActivity() {

    abstract val layoutId: Int

    protected lateinit var context: Context
    var mDisposable: CompositeDisposable? = CompositeDisposable()
    var mRxPermission: RxPermissions? = null
    protected var mProgressDialog: ProgressDialog? = null

    lateinit var dataSource: DataSource

    lateinit var res: Resources

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        context = this
        dataSource = (application as MyApplication).getDataRepo().dataSource
        mProgressDialog = ProgressDialog(context)
        mProgressDialog?.setMessage(getString(R.string.please_wait))
        mProgressDialog?.setCancelable(false)
        mRxPermission = RxPermissions(this)
        initView()
    }

    open fun initView() {
    }

    /**
     * change/open fragment
     */
    fun changeFragment(
        fragment: Fragment,
        containerId: Int = R.id.container,
        cleanStack: Boolean = false,
        addToBackStack: Boolean = true
    ) {
        val ft = supportFragmentManager.beginTransaction()
        if (cleanStack) {
            clearBackStack()
        }
        ft.replace(containerId, fragment)
        if (addToBackStack)
            ft.addToBackStack(null)
        ft.commit()
    }

    fun changeChildFragment(
        fragment: Fragment,
        containerId: Int = R.id.container,
        cleanStack: Boolean = false,
        addToBackStack: Boolean = true
    ) {
        val ft = supportFragmentManager.beginTransaction()
        if (cleanStack) {
            clearBackStack()
        }
        ft.replace(containerId, fragment)
        if (addToBackStack)
            ft.addToBackStack(null)
        ft.commit()
    }

    /**
     * clear fragment back stack
     */
    fun clearBackStack() {
        val manager = supportFragmentManager
        if (manager.backStackEntryCount > 0) {
            val first = manager.getBackStackEntryAt(0)
            manager.popBackStack(first.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    /**
     * display progress dialog if currently not showing
     */
    fun showProgressDialog() {
        if (mProgressDialog != null && !mProgressDialog!!.isShowing)
            mProgressDialog!!.show()
    }

    fun dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing)
            mProgressDialog!!.dismiss()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase!!))
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mDisposable?.isDisposed == false)
            mDisposable?.dispose()
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        val v: View? = currentFocus
        if (v != null &&
            (ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE) &&
            v is EditText &&
            !v.javaClass.name.startsWith("android.webkit.")
        ) {
            val scrcoords = IntArray(2)
            v.getLocationOnScreen(scrcoords)
            val x: Float = ev.rawX + v.getLeft() - scrcoords[0]
            val y: Float = ev.rawY + v.getTop() - scrcoords[1]
            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom()) hideKeyboard(this)
        }
        return super.dispatchTouchEvent(ev)
    }

    open fun hideKeyboard(activity: Activity?) {
        if (activity != null && activity.window != null && activity.window.decorView != null) {
            val imm: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
        }
    }

    override fun onBackPressed() {
        // if there is a fragment and the back stack of this fragment is not empty,
        // then emulate 'onBackPressed' behaviour, because in default, it is not working

        // if there is a fragment and the back stack of this fragment is not empty,
        // then emulate 'onBackPressed' behaviour, because in default, it is not working
        val fm = supportFragmentManager
        for (frag in fm.fragments) {
            if (frag.isVisible) {
                val childFm = frag.childFragmentManager
                if (childFm.backStackEntryCount > 1) {
                    childFm.popBackStack()
                    return
                }
            }
        }
        super.onBackPressed()
    }

    fun showDialog(
        message: String,
        title: String? = null,
        positiveText: String = getString(android.R.string.yes),
        negativeText: String = getString(android.R.string.no),
        positiveClicked: () -> Unit
    ) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveText, DialogInterface.OnClickListener { dialog, which ->
                positiveClicked.invoke()
                // Continue with delete operation
            }) // A null listener allows the button to dismiss the dialog and take no further action.
            .setNegativeButton(negativeText, null)
            .show()
    }
}