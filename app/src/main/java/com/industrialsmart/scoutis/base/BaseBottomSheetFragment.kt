package com.industrialsmart.scoutis.base

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.interfaces.DataSource
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable

abstract class BaseBottomSheetFragment: BottomSheetDialogFragment() {

    protected var mProgressDialog: ProgressDialog? = null
    var mDisposable: CompositeDisposable? = CompositeDisposable()

    lateinit var dataSource: DataSource

    abstract val layoutId: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(layoutId, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataSource = (activity as BaseActivity).dataSource
        mProgressDialog = ProgressDialog(context)
        mProgressDialog?.setMessage(getString(R.string.please_wait))
        mProgressDialog?.setCancelable(false)
        initView()
    }

    open fun initView() {

    }

    fun showProgressDialog() {
        if (mProgressDialog != null && !mProgressDialog!!.isShowing)
            mProgressDialog!!.show()
    }


    fun dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing)
            mProgressDialog!!.dismiss()
    }


    override fun onDestroy() {
        super.onDestroy()
        if (mDisposable?.isDisposed == false)
            mDisposable?.dispose()
    }

    fun showInfoDialog(
        message: String,
        title: String? = null,
        positiveText: String = getString(android.R.string.ok),
        positiveClicked: () -> Unit
    ) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveText, DialogInterface.OnClickListener { dialog, which ->
                positiveClicked.invoke()
                // Continue with delete operation
            })
            .show()
    }

    fun showDialog(
        message: String,
        title: String? = null,
        positiveText: String = getString(android.R.string.yes),
        negativeText: String = getString(android.R.string.no),
        positiveClicked: () -> Unit
    ) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveText, DialogInterface.OnClickListener { dialog, which ->
                positiveClicked.invoke()
                // Continue with delete operation
            }) // A null listener allows the button to dismiss the dialog and take no further action.
            .setNegativeButton(negativeText, null)
            .show()
    }
}