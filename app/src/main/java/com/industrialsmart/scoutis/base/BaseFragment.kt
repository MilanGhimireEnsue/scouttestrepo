package com.industrialsmart.scoutis.base

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.interfaces.DataSource
import com.industrialsmart.scoutis.listener.UIEventListener
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable


/**
 * Created by rakezb on 2/19/2019.
 */
abstract class BaseFragment : Fragment(), UIEventListener {

    abstract val layoutId: Int

    private var mProgressDialog: ProgressDialog? = null
    private var mRxPermission: RxPermissions? = null
    var mDisposable: CompositeDisposable? = CompositeDisposable()

    lateinit var dataSource: DataSource

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(layoutId, container, false)
        return rootView
    }

    override fun onRightButtonClicked() {

    }

    override fun onRightIconClicked() {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dataSource = (activity as BaseActivity).dataSource
        mRxPermission = RxPermissions(activity!!)
        mProgressDialog = ProgressDialog(context)
        mProgressDialog?.setMessage(getString(R.string.please_wait))
        mProgressDialog?.setCancelable(false)
        initView()
    }

    open fun initView() {

    }


    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        if (!userVisibleHint) {
            return
        }

        onFragmentVisible()
        //INSERT CUSTOM CODE HERE
    }

    open fun onFragmentVisible() {

    }

    fun changeFragment(
        fragment: Fragment,
        containerId: Int = R.id.container,
        cleanStack: Boolean = false,
        addToBackStack: Boolean = true
    ) {
        (activity as BaseActivity).changeFragment(fragment, containerId, cleanStack, addToBackStack)
    }

    fun changeChildFragment(
        fragment: Fragment,
        containerId: Int = R.id.container,
        cleanStack: Boolean = false,
        addToBackStack: Boolean = true
    ) {
        val ft = childFragmentManager.beginTransaction()
        if (cleanStack) {
            clearBackStack()
        }
        ft.replace(containerId, fragment)
        if (addToBackStack)
            ft.addToBackStack(null)
        ft.commit()
    }

    private fun clearBackStack() {
        val manager = activity?.supportFragmentManager
        if (manager?.backStackEntryCount as Int > 0) {
            val first = manager.getBackStackEntryAt(0)
            manager.popBackStack(first.id, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    /**
     * display progress dialog if currently not showing
     */
    fun showProgressDialog() {
        if (mProgressDialog != null && !mProgressDialog!!.isShowing)
            mProgressDialog!!.show()
    }


    fun dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing)
            mProgressDialog!!.dismiss()
    }


    override fun onDestroy() {
        super.onDestroy()
        if (mDisposable?.isDisposed == false)
            mDisposable?.dispose()
    }

    fun showDialog(
        message: String,
        title: String? = null,
        positiveText: String = getString(android.R.string.yes),
        negativeText: String = getString(android.R.string.no),
        positiveClicked: () -> Unit
    ) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveText, DialogInterface.OnClickListener { dialog, which ->
                positiveClicked.invoke()
                // Continue with delete operation
            }) // A null listener allows the button to dismiss the dialog and take no further action.
            .setNegativeButton(negativeText, null)
            .show()
    }


}