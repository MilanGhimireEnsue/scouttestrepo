package com.industrialsmart.scoutis

import android.app.Application
import com.chibatching.kotpref.Kotpref
import com.chibatching.kotpref.gsonpref.gson
import com.google.gson.Gson
import com.jakewharton.threetenabp.AndroidThreeTen
import com.microsoft.identity.client.IAccount
import com.microsoft.identity.client.IMultipleAccountPublicClientApplication
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import io.reactivex.plugins.RxJavaPlugins


class MyApplication : Application() {

    private lateinit var dataRepo: DataRepo

    private var mLoginAccount: IAccount? = null
    private var mMultipleAccountApp: IMultipleAccountPublicClientApplication? = null

    override fun onCreate() {
        super.onCreate()
        dataRepo = DataRepo(this)
        RxJavaPlugins.setErrorHandler {
        }
        initFont()
        Kotpref.gson = Gson()
        AndroidThreeTen.init(this)
    }

    fun setLoginAccount(
        iAccount: IAccount?,
        mMultipleAccountApp: IMultipleAccountPublicClientApplication?
    ) {
        this.mLoginAccount = iAccount
        this.mMultipleAccountApp = mMultipleAccountApp
    }

    fun getLoginAccount(): IAccount? {
        return mLoginAccount
    }

    fun getMultipleAccount(): IMultipleAccountPublicClientApplication?{
        return mMultipleAccountApp
    }

    private fun initFont() {
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath(getString(R.string.fontRegular))
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )
    }

    fun getDataRepo(): DataRepo {
        return dataRepo
    }

}