package com.industrialsmart.scoutis.interfaces

import com.industrialsmart.scoutis.models.*
import io.reactivex.Completable
import io.reactivex.Single

interface DataSource {
    fun getScoutsList(): Single<List<ScoutModel?>>

    fun getReadingTypeList(): Single<List<ReadingTypeItem?>>

    fun getSensorData(getSensorDataRequest: GetSensorDataRequest?): Single<List<ReadingDataItem?>>

    fun getUserData(): Single<UserDataResponse?>

    fun postUserData(userDataResponse: UserDataResponse?): Single<UserDataResponse?>

    fun postScoutInfo(scoutModelList: List<ScoutModel?>): Completable

    fun postSensorInfo(sensorList: List<Sensor?>): Completable

    fun getGduList(): Single<List<GDUDataItem?>>

    fun getRawScoutList(): Single<List<ScoutItemResponse?>>

    fun getSubjectList(): Single<List<AppSubjectItem?>>

    fun requestAddGDU(gduItem: GDUResponse?): Single<GDUResponse?>

    fun requestGDUDelete(id: String?): Single<Boolean?>

    fun getRegionList(): Single<List<AppRegionItem?>>

    fun getOccasionList(): Single<List<OccasionItem?>>

    fun requestAddOccasion(occasionItem: OccasionItem?): Single<OccasionItem?>

    fun requestDeleteOccasion(id: String?): Single<Boolean?>

    fun getNotificationList(): Single<List<NotificationItem?>>

    fun postNotificationRead(notificationItem: NotificationItem?): Completable

    fun postBulkNotificationRead(ids: String?): Completable

    fun getNotificationSettingList(): Single<List<NotificationSettingItem?>>

    fun requestDeleteNotificationSetting(id: String?): Single<Boolean?>

    fun requestAddNotificationSetting(notificationSettingItem: NotificationSettingItem?): Single<NotificationSettingItem?>

    fun requestPostFCMToken(fcmTokenPostData: FCMTokenPostData?): Completable

    fun getUnreadNotificationCount(): Single<Int?>

    fun getReferencesList(): Single<List<ReferenceItem?>>

    fun getSysInfo(): Single<SysInfoResponse?>

    fun clearAllData()
}