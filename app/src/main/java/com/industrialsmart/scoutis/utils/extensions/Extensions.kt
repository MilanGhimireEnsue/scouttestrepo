package com.industrialsmart.scoutis.utils.extensions

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.provider.Settings
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.industrialsmart.scoutis.MyApplication
import com.industrialsmart.scoutis.models.SoilECUnit
import com.industrialsmart.scoutis.models.TemperatureUnit
import com.industrialsmart.scoutis.models.UserInfo
import com.industrialsmart.scoutis.models.UserSetting
import com.industrialsmart.scoutis.view.login.LoginActivity
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import org.threeten.bp.DayOfWeek
import org.threeten.bp.temporal.WeekFields
import java.util.*


fun Context.logoutApplication() {
    UserInfo.apply {
        loginStatus = false
        accessToken = ""
        loginIAccout = ""
    }
    (this.applicationContext as MyApplication).getDataRepo().dataSource.clearAllData()
    this.startActivity(intentFor<LoginActivity>().clearTop().clearTask().newTask())
}

internal fun Context?.getDrawableCompat(@DrawableRes drawable: Int) = ContextCompat.getDrawable(this!!, drawable)

internal fun Context?.getColorCompat(@ColorRes color: Int) = ContextCompat.getColor(this!!, color)

internal fun TextView.setTextColorRes(@ColorRes color: Int) = setTextColor(context.getColorCompat(color))

fun daysOfWeekFromLocale(): Array<DayOfWeek> {
    val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
    var daysOfWeek = DayOfWeek.values()
    // Order `daysOfWeek` array so that firstDayOfWeek is at index 0.
    if (firstDayOfWeek != DayOfWeek.MONDAY) {
        val rhs = daysOfWeek.sliceArray(firstDayOfWeek.ordinal..daysOfWeek.indices.last)
        val lhs = daysOfWeek.sliceArray(0 until firstDayOfWeek.ordinal)
        daysOfWeek = rhs + lhs
    }
    return daysOfWeek
}

fun GradientDrawable.setCornerRadius(
    topLeft: Float = 0F,
    topRight: Float = 0F,
    bottomRight: Float = 0F,
    bottomLeft: Float = 0F
) {
    cornerRadii = arrayOf(
        topLeft, topLeft,
        topRight, topRight,
        bottomRight, bottomRight,
        bottomLeft, bottomLeft
    ).toFloatArray()
}

fun Context.dpToPixels(dps: Float): Int {
//    val scale = this.resources.displayMetrics.density9o
//    return (dps * scale + 0.5f).toInt()
//    return (dps * (this.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    var px = (TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dps, resources.displayMetrics
    ))
    return px.toInt()


}

var EditText.textValue: String
    get() = text.toString()
    set(v) {
        setText(v)
    }

fun updateUserSetting() {
    if (UserInfo.userData?.userSettingsInfo?.units?.find { it.name == "Temperature" }?.value == TemperatureUnit.CELSIUS.unitCode)
        UserSetting.temperatureUnit = TemperatureUnit.CELSIUS
    else
        UserSetting.temperatureUnit = TemperatureUnit.FAHRENHEIT

    if (UserInfo.userData?.userSettingsInfo?.units?.find { it.name == "SoilEC" }?.value == SoilECUnit.USCM.unitCode)
        UserSetting.soilECUnit = SoilECUnit.USCM
    else
        UserSetting.soilECUnit = SoilECUnit.DSM
}

fun View.getMeasurments(): Pair<Int, Int> {
    measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
    val width = measuredWidth
    val height = measuredHeight
    return width to height
}

fun getDeviceUniqueId(context: Context?): String {
    var androidId: String = ""
    try {
        androidId = Settings.Secure.getString(context?.contentResolver, Settings.Secure.ANDROID_ID)
    } catch (e: Exception) {
        e.printStackTrace()
        androidId = UUID.randomUUID().toString()
    }
    return androidId
}
