package com.industrialsmart.scoutis.utils.extensions

import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.view.scout.setting.ScoutSettingFragment.Companion.HIDDEN
import java.math.RoundingMode
import java.text.DecimalFormat

fun List<ScoutModel?>.applyVisibilitySetting(): List<ScoutModel?> {
    val scoutList: MutableList<ScoutModel?> = this.toMutableList()
    scoutList.forEach { scoutModel ->
        scoutModel?.isActive = true
        val scoutSetting = UserInfo.userData?.userSettingsInfo?.scoutSettings?.find { it.id == scoutModel?.id }
        if (scoutSetting != null && scoutSetting.value?.importance == HIDDEN)
            scoutModel?.isActive = false
        scoutModel?.sensors?.forEach { sensorModel ->
            sensorModel.isActive = true
            val sensorSetting = UserInfo.userData?.userSettingsInfo?.sensorSettings?.find { it.id == sensorModel.id }
            if (sensorSetting != null && sensorSetting?.value?.importance == HIDDEN)
                sensorModel.isActive = false
            sensorModel.readings?.forEach {
                //check if the temperature unit needs to be converted, then make the conversion
                if (((it.unit == (TemperatureUnit.CELSIUS.unitCode)) || (it.unit == (TemperatureUnit.FAHRENHEIT.unitCode))) && (it.unit != UserSetting.temperatureUnit.unitCode)) {
                    when (UserSetting.temperatureUnit) {
                        TemperatureUnit.FAHRENHEIT -> {
                            it.value = convertTemperature(it.value?.toDouble(), TemperatureUnit.FAHRENHEIT).toString()
                            it.unit = TemperatureUnit.FAHRENHEIT.unitCode
                        }
                        TemperatureUnit.CELSIUS -> {
                            it.value = convertTemperature(it.value?.toDouble(), TemperatureUnit.CELSIUS).toString()
                            it.unit = TemperatureUnit.CELSIUS.unitCode
                        }
                    }
                }
                //check if the soilEC unit needs to be converted, then make the conversion
                if (((it.unit == (SoilECUnit.USCM.unitCode)) || (it.unit == (SoilECUnit.DSM.unitCode))) && (it.unit != UserSetting.soilECUnit.unitCode)) {
                    when (UserSetting.soilECUnit) {
                        SoilECUnit.USCM -> {
                            it.value = convertSoilEC(it.value?.toDouble(), SoilECUnit.USCM).toString()
                            it.unit = SoilECUnit.USCM.unitCode
                        }
                        SoilECUnit.DSM -> {
                            it.value = convertSoilEC(it.value?.toDouble(), SoilECUnit.DSM).toString()
                            it.unit = SoilECUnit.DSM.unitCode
                        }
                    }
                }
            }
        }
        //make scout also inactive, if all the sensors are inactive
        if ((scoutModel?.sensors?.filter { it.isActive == true })?.size!! == 0)
            scoutModel.isActive = false
}
    return scoutList
}

fun convertTemperature(value: Double?, toUnit: TemperatureUnit): Double {
    if (value == null)
        return 0.0
    return when (toUnit) {
        TemperatureUnit.CELSIUS -> ((value - 32) / 1.8).roundUp(2)
        TemperatureUnit.FAHRENHEIT -> ((value * 1.8) + 32).roundUp(2)
    }

}

fun convertSoilEC(value: Double?, toUnit: SoilECUnit): Double {
    if (value == null)
        return 0.0
    return when (toUnit) {
        SoilECUnit.DSM -> (value / 1000.0).roundUp(3)
        SoilECUnit.USCM -> (value * 1000.0).roundUp(3)
    }
}

fun Double.roundUp(decimalPlace: Int): Double {
    return this.toBigDecimal().setScale(decimalPlace, RoundingMode.HALF_DOWN).toDouble()
}

fun String?.addDegreeIfTempUnit(): String? {
    return if ((this == TemperatureUnit.CELSIUS.unitCode) || (this == TemperatureUnit.FAHRENHEIT.unitCode))
        "\u00B0$this"
    else
        this
}

fun List<AppSubjectItem?>.subjectToIdCodeList(): List<IdCodeItem>{
    return this.map { IdCodeItem(code = it?.code, id = it?.id, name =  it?.name) }
}


fun List<AppRegionItem?>.regionToIdCodeList(): List<IdCodeItem>{
    return this.map { IdCodeItem(code = it?.code, id = it?.id, name =  it?.name) }
}