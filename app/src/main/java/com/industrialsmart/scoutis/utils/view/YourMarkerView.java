package com.industrialsmart.scoutis.utils.view;

import android.content.Context;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.industrialsmart.scoutis.R;

import static com.industrialsmart.scoutis.utils.extensions.DateExtKt.getFormattedDateForXAxis;

public class YourMarkerView extends MarkerView {

    private TextView tvContent;
    private Context context;
    private int colorRes;

    public YourMarkerView(Context context, int layoutResource, int colorRes) {
        super(context, layoutResource);

        // find your layout components
        tvContent = (TextView) findViewById(R.id.tvContent);
        tvContent.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_circle, 0, 0, 0);
        this.context = context;
        this.colorRes = colorRes;
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
// content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {

        tvContent.setText(getFormattedDateForXAxis(e.getX()) + " : " + e.getY());
        tvContent.getCompoundDrawables()[0].setTint(ContextCompat.getColor(context, colorRes));
        // this will perform necessary layouting
        super.refreshContent(e, highlight);
    }

    private MPPointF mOffset;

    @Override
    public MPPointF getOffset() {

        if (mOffset == null) {
            // center the marker horizontally and vertically
            mOffset = new MPPointF(-(getWidth() / 2), -getHeight());
        }

        return mOffset;
    }
}
