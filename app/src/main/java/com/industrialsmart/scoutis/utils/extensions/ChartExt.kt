package com.industrialsmart.scoutis.utils.extensions

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ReadingDataItem
import com.industrialsmart.scoutis.models.ReadingTypeItem
import com.industrialsmart.scoutis.models.ToBeCompared

fun getLineData(sensorData: List<ReadingDataItem?>?, colorTint: Int? = null): LineDataSet? {
    if (sensorData == null) {
        return null
    }
    val entries = ArrayList<Entry>()
    for (item in sensorData) {
        entries.add(Entry(item?.getUnixTimestampForDate()!!.toFloat(), item?.value!!.toFloat(), null, colorTint))
    }
    return LineDataSet(entries, "Some values")
}

fun List<ToBeCompared?>.setRandomColors(): List<ToBeCompared?> {
    this.forEachIndexed { index, readingTypeItem ->
        when (index) {
            0 -> this[index]?.foreGroundTint = R.color.reading_color_1
            1 -> this[index]?.foreGroundTint = R.color.reading_color_2
            2 -> this[index]?.foreGroundTint = R.color.reading_color_3
            3 -> this[index]?.foreGroundTint = R.color.reading_color_4
            4 -> this[index]?.foreGroundTint = R.color.reading_color_5
            5 -> this[index]?.foreGroundTint = R.color.reading_color_6
            6 -> this[index]?.foreGroundTint = R.color.reading_color_7
            7 -> this[index]?.foreGroundTint = R.color.reading_color_8
            8 -> this[index]?.foreGroundTint = R.color.reading_color_9
            9 -> this[index]?.foreGroundTint = R.color.reading_color_10
            else -> this[index]?.foreGroundTint = R.color.colorPrimary
        }
    }
    return this
}