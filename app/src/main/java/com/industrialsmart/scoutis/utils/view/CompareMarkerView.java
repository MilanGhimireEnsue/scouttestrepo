package com.industrialsmart.scoutis.utils.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.industrialsmart.scoutis.R;
import com.industrialsmart.scoutis.models.ReadingDataItem;
import com.industrialsmart.scoutis.models.ToBeCompared;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.industrialsmart.scoutis.utils.extensions.DateExtKt.getFormattedDateForXAxis;

public class CompareMarkerView extends MarkerView {

    private final List<ToBeCompared> dataList;
    private TextView tvContent;
    private Context context;
    private int colorRes;

    public CompareMarkerView(Context context, int layoutResource, List<ToBeCompared> dataMap) {
        super(context, layoutResource);

        // find your layout components
        tvContent = (TextView) findViewById(R.id.tvContent);
        tvContent.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_circle, 0, 0, 0);
        this.context = context;
        this.dataList = dataMap;
    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
// content (user-interface)
    int deltaY = 0;
    int lastYPos = 0;

    float xPoint = 0f;
    int count = 0;

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        tvContent.setText(getFormattedDateForXAxis(e.getX()) + " : " + e.getY());
//        lastYPos = (int) highlight.getDrawY();
//        Log.d("TAG", "refreshContent draw: " + highlight.getDrawX() + " : " + highlight.getDrawY());
//        int[] firstPointLocation = new int[2];
//        tvContent.getLocationOnScreen(firstPointLocation);
//        int x = firstPointLocation[0];
//        int y = firstPointLocation[1];
        // this will perform necessary layouting
        if (xPoint == 0 || xPoint != e.getX()) {
            count = 0;
            xPoint = e.getX();
        } else {
            count++;
        }
        if (count >= dataList.size())
            count = 0;
        int colorRes = dataList.get(count).getForeGroundTint();
        tvContent.getCompoundDrawables()[0].setTint(ContextCompat.getColor(context, colorRes));
        super.refreshContent(e, highlight);
    }

    private MPPointF mOffset;

    @Override
    public MPPointF getOffset() {

        if (mOffset == null) {
            // center the marker horizontally and vertically
            mOffset = new MPPointF(-(getWidth() / 2), -getHeight());
//            if (lastYPos != 0)
//                mOffset = new MPPointF(-(getWidth() / 2), 700);
//            else
//                mOffset = new MPPointF(-(getWidth() / 2), -getHeight());
        }

        return mOffset;
    }

    @Override
    public void draw(Canvas canvas, float posX, float posY) {
        super.draw(canvas, posX, posY);
    }
}
