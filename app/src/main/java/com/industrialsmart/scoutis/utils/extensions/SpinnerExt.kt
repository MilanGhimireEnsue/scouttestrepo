package com.industrialsmart.scoutis.utils.extensions

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.industrialsmart.scoutis.R

fun setHintSpinnerAdapter(dataList: ArrayList<String>, spinner: Spinner, prompt: String) {
    val adapter = ArrayAdapter(spinner.context, R.layout.row_spn, dataList)
    adapter.setDropDownViewResource(R.layout.row_spn_dropdown)

    spinner.adapter = HintSpinnerAdapter(adapter, R.layout.row_spn_spinner_hint, spinner.context, prompt)
}


fun Spinner.setHintSpinnerAdapter(dataList: List<String>?, prompt: String = "Select") {
    var strDataList = ArrayList<String>()

    if (dataList != null)
        for (data in dataList)
            if (data != null)
                strDataList.add(data)

    setHintSpinnerAdapter(strDataList, this, prompt)
}

fun Spinner.onItemSelected(itemSelected: (View?, Int?, Long?) -> Unit) {
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
            if (position != 0)
                itemSelected.invoke(view, position, id)
        }

        override fun onNothingSelected(parent: AdapterView<*>) {
            // sometimes you need nothing here
        }
    }
}