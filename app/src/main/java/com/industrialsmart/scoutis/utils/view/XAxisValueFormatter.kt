package com.industrialsmart.scoutis.utils.view

import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.industrialsmart.scoutis.utils.extensions.getFormattedDateForXAxis

class XAxisValueFormatter : IndexAxisValueFormatter() {
    override fun getFormattedValue(value: Float): String {
        return getFormattedDateForXAxis(value)
    }
}