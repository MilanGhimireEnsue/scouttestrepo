package com.industrialsmart.scoutis.utils
import com.industrialsmart.scoutis.BuildConfig

object Constants {
    enum class DateType {
        START_DATE,
        END_DATE
    }

    val BASE_URL = BuildConfig.BASE_URL
    val isLocalDataSource = false
    val API_VERSION = "2.0.0"


    //azure conf
    val AZURE_TENANT = "earthscout.onmicrosoft.com"
    val AZURE_CLIENT_ID = "938bb782-2f82-4b38-90a4-ded620c32ed1"
    val AZURE_REDIRECT_URI = "http://localhost:4200/auth-callback"
    val AZURE_POST_LOGOUT_REDIRECT_URI = "http://localhost:4200/login"
    val AZURE_NONCE_ID = "b7751557-1658-45b9-ac9e-79b401ba9e8e"

    const val NotificationChannelId = "com.industrialsmart.scoutis.ANDROID"
    const val NotificationChannelName = "ANDROID CHANNEL"
    const val NOTIFICATION_GROUP_NAME = "SMART IS NOTIFICATION GROUP"
    const val SUMMARY_NOTIFICATION_ID = 0

    //B2C configuration
    const val AUTHORITY = "https://earthscoutb2c.b2clogin.com/tfp/earthscoutb2c.onmicrosoft.com/B2C_1A_TermsOfService"
    const val TENANT = "earthscoutb2c.onmicrosoft.com"
    const val CLIENT_ID = "029548e4-af7c-4638-8089-e4184dc1b9af"
    const val SCOPES_DEV = "https://earthscoutb2c.onmicrosoft.com/029548e4-af7c-4638-8089-e4184dc1b9af/user_impersonation"
    const val SCOPES_PROD = "https://earthscoutb2c.onmicrosoft.com/8e5a645c-32dc-49f9-909d-44bae7d8ec3c/user_impersonation"
    const val API_URL = "https://fabrikamb2chello.azurewebsites.net/hello"

    const val SISU_POLICY = "B2C_1A_TermsOfService"
    const val EDIT_PROFILE_POLICY = "B2C_1A_TermsOfService"

    /*    static let MSAL_TENANT = "earthscoutb2c.onmicrosoft.com"
    static let MSAL_AUTHORITY_INSTANCE = "earthscoutb2c.b2clogin.com"
    static let MSAL_SIGNIN_POLICY = "B2C_1A_TermsOfService"
    static let MSAL_SIGNUP_POLICY = "B2C_1A_TermsOfService"
    static let MSAL_PASSWORD_RESET_POLICY = "B2C_1_ResetPassword"*/

    const val GDU_SENSOR_ID = "gdu_sensor_id"

}