package com.industrialsmart.scoutis.utils.view

import android.content.Context
import android.view.LayoutInflater
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ScoutMarkerItem

class CustomMarkerRenderer(
    val context: Context?, map: GoogleMap,
    clusterManager: ClusterManager<ScoutMarkerItem?>
): DefaultClusterRenderer<ScoutMarkerItem>(context, map, clusterManager) {
    override fun onBeforeClusterItemRendered(item: ScoutMarkerItem, markerOptions: MarkerOptions) {
        val markerView = (context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.layout_marker, null)
        val tvMarkerView = markerView.findViewById<TextView>(R.id.textView)
        tvMarkerView.text = item.scoutModel?.title
        val iconFactory = IconGenerator(context)
        iconFactory.setBackground(ContextCompat.getDrawable(context, android.R.color.transparent))
        iconFactory.setContentView(markerView)
        markerOptions.apply {
            icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon()))
        }
        super.onBeforeClusterItemRendered(item, markerOptions)
    }

    override fun shouldRenderAsCluster(cluster: Cluster<ScoutMarkerItem>): Boolean {
        return cluster.size > 1
    }
}