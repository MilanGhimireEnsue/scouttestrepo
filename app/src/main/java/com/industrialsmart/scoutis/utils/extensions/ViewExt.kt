package com.industrialsmart.scoutis.utils.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View

/**
 * Created by munnadroid on 2/5/18.
 */
fun View.showIf(show: Boolean) {
    if (show) {
        show()
    } else {
        hide()
    }
}

fun View.show(): Unit {
    visibility = View.VISIBLE
}

fun View.hide(): Unit {
    visibility = View.GONE
}

fun View.invisible(): Unit {
    visibility = View.INVISIBLE
}


fun View.toggleVisibility(): Unit = if (visibility == View.VISIBLE) hide() else show()

fun View.showWithAnimateSlideDown() {
    // Prepare the View for the animation
    this.show()
    this.alpha = 0.0f
    // Start the animation
    this.animate()
        .translationYBy(this.height.toFloat())
        .alpha(1.0f)
        .setListener(null)
}

fun View.hideWithAnimateSlideUp() {
    val view = this
    this.animate()
        .translationYBy(-this.height.toFloat())
        .alpha(0.0f)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                view.hide()
            }
        })
}