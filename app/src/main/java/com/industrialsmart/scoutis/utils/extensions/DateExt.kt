package com.industrialsmart.scoutis.utils.extensions

import com.industrialsmart.scoutis.utils.d
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/*05-May-2020 10:28:59*/
fun String?.getFormattedDate(): String {
    if (this.isNullOrEmpty())
        return ""
    return try {
        var dateFormat = SimpleDateFormat("dd-MMM-yyyy HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        val date = dateFormat.parse(this)
        dateFormat = SimpleDateFormat("MMM dd, yyyy hh:mm aa")
        dateFormat.format(date)
    } catch (ex: Exception) {
        ex.printStackTrace()
        ""
    }
}

fun String?.getFormattedDateFromServerDate(): String {
    if (this.isNullOrEmpty())
        return ""
    return try {
        var dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        val date = dateFormat.parse(this)
        dateFormat = SimpleDateFormat("yyyy-MM-dd")
        dateFormat.format(date)
    } catch (ex: Exception) {
        ex.printStackTrace()
        ""
    }
}

fun String?.getCalendarFromServerDate(): Calendar {
    val cal = Calendar.getInstance()
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH)
    cal.time = sdf.parse(this) // all done
    return cal
}

fun String?.getFormattedDateTimeFromServerDate(): String {
    if (this.isNullOrEmpty())
        return ""
    return try {
        var dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        val date = dateFormat.parse(this)
        dateFormat = SimpleDateFormat("E dd MMM, yyyy hh:mm aa")
        dateFormat.format(date)
    } catch (ex: Exception) {
        try {
            var dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
            dateFormat.timeZone = TimeZone.getTimeZone("UTC")
            val date = dateFormat.parse(this)
            dateFormat = SimpleDateFormat("E dd MMM, yyyy hh:mm aa")
            dateFormat.format(date)
        } catch (ex: Exception) {
            try {
                //2020-08-19T17:07:38.57Z
                var dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                dateFormat.timeZone = TimeZone.getTimeZone("UTC")
                val date = dateFormat.parse(this)
                dateFormat = SimpleDateFormat("E dd MMM, yyyy hh:mm aa")
                dateFormat.format(date)
            } catch (ex: Exception) {
                ex.printStackTrace()
                ""
            }
        }
    }
}


fun LocalDate?.getFormattedTimeFromServerDateForEvent(): String {
    if (this == null)
        return ""
    return try {
        DateTimeFormatter.ofPattern("MMMM, yyyy").format(this)
    } catch (ex: Exception) {
        ex.printStackTrace()
        ""
    }
}

fun LocalDate?.getFormattedDateFromServerDateForEvent(isAllDayEvent: Boolean? = false): String {
    if (this == null)
        return ""
    return try {
        if (isAllDayEvent == true) {
            DateTimeFormatter.ofPattern("E dd MMM, yyyy").format(this) + " (All Day Event)"
        } else
            DateTimeFormatter.ofPattern("E dd MMM, yyyy").format(this)
    } catch (ex: Exception) {
        ex.printStackTrace()
        ""
    }
}

fun String?.getLocalizedDate(): String? {
    if (this.isNullOrEmpty())
        return ""
    val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    dateFormat.timeZone = TimeZone.getTimeZone("UTC")
    val date = dateFormat.parse(this)
    dateFormat.timeZone = TimeZone.getDefault()
    return dateFormat.format(date)
}

fun String?.addMinutes(minutes: Int?): String? {
    if (this.isNullOrEmpty())
        return ""
    return try {
        var dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        var date = dateFormat.parse(this)
        date = Date(date.time + (minutes?.times(60) ?: 0) * 1000)
        dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
        dateFormat.format(date)
    } catch (ex: Exception) {
        ex.printStackTrace()
        ""
    }
}

fun String?.getLocalDateFromServerFormat(): LocalDate? {
    return LocalDate.parse(this, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"))
}

fun LocalDate?.getFormattedMonthYearForEvent(): String? {
    return DateTimeFormatter.ofPattern("MMMM, yyyy").format(this)
}

fun LocalDate?.getWeekDayString(): String? {
    return DateTimeFormatter.ofPattern("EEE").format(this)
}

fun LocalDate?.getMonthDayString(): String? {
    return DateTimeFormatter.ofPattern("dd").format(this)
}

fun LocalDate.isDateBetween(startDate: String?, endDate: String?): Boolean {
    if (startDate == null || endDate == null)
        return false
    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
    formatter.withZone(ZoneId.of("UTC"))
    val mStartDate = LocalDate.parse(startDate, formatter)
    val mEndDate = LocalDate.parse(endDate, formatter)
    return this >= mStartDate && this <= mEndDate
}

fun inBetweenDateRange(startDate: String?, endDate: String?): List<LocalDate> {
    if (startDate == null || endDate == null)
        return emptyList()
    val inBetweenDateList: MutableList<LocalDate> = ArrayList()
    val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
    formatter.withZone(ZoneId.of("UTC"))
    val mStartDate = LocalDate.parse(startDate, formatter)
    val mEndDate = LocalDate.parse(endDate, formatter)
    var tempStartDate = mStartDate.plusDays(1)
    while (tempStartDate <= mEndDate) {
        inBetweenDateList.add(tempStartDate)
        tempStartDate = tempStartDate.plusDays(1)
    }
    return inBetweenDateList
}

fun getCurrentDate(): String {
    val cal = Calendar.getInstance()
    return "${SimpleDateFormat("yyyy-MM-dd").format(cal.time)}T12:00:00.00"
}

fun getLastWeekDate(): String {
    val cal = Calendar.getInstance()
    cal.add(Calendar.WEEK_OF_MONTH, -1)
    return "${SimpleDateFormat("yyyy-MM-dd").format(cal.time)}T12:00:00.00"
}

fun getLastMonthDate(): String {
    val cal = Calendar.getInstance()
    cal.add(Calendar.MONTH, -1);
    return "${SimpleDateFormat("yyyy-MM-dd").format(cal.time)}T12:00:00.00"
}

fun getFormattedDateForXAxis(dateTime: Float): String {
    return try {
        SimpleDateFormat("MM/dd hh:mm aa").format(dateTime)
    } catch (ex: Exception) {
        ""
    }
}

/**
 * below function required for date picker dialogs
 */
const val datePickerFormat = "yyyy-MM-dd"
fun getDateString(year: Int, month: Int, day: Int): String {
    val formatter = DecimalFormat("00")
    return "$year-${formatter.format(month + 1)}-${formatter.format(day)}"
}

fun getDateTimeString(date: Calendar): String {
    val formatter = SimpleDateFormat("E dd MMM, yyyy hh:mm aa")
    return formatter.format(date.time)
}

fun getFormattedDateTime(dateString: String?): String {
    //2018-12-03T08:00:00Z
    val formatter = SimpleDateFormat("E dd MMM, yyyy hh:mm aa")
    val date = formatter.parse(dateString)
    val serverFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    serverFormat.timeZone = TimeZone.getTimeZone("UTC")
    return serverFormat.format(date)
}

fun getTimeDiffInMinutes(startDate: String?, endDate: String?): Int {
    d("TAG", "okhttp startDate $startDate : $endDate")
    //2018-12-03T08:00:00Z
    val formatter = SimpleDateFormat("E dd MMM, yyyy hh:mm aa")
    val startDate = formatter.parse(startDate)
    val endDate = formatter.parse(endDate)
    val diff = ((endDate.time - startDate.time) / (1000 * 60)).toInt() // converting millisecond to minutes
    return diff
}

fun getEndDateTimeFromStartDate(startDate: String?): String {
    //2018-12-03T08:00:00Z
    val formatter = SimpleDateFormat("E dd MMM, yyyy hh:mm aa")
    val startDate = formatter.parse(startDate)
    val endDateFormat = SimpleDateFormat("E dd MMM, yyyy")
    d("TAG", "okhttp endDate : " + endDateFormat.format(startDate) + " 11:59 PM")
    return endDateFormat.format(startDate) + " 11:59 PM"
}


fun getDayFromDateString(dateString: String?): Int {
    return try {
        var dateFormat = SimpleDateFormat(datePickerFormat)
        var date = dateFormat.parse(dateString)
        dateFormat = SimpleDateFormat("dd")
        dateFormat.format(date).toInt()
    } catch (e: Exception) {
        getCurrentDay()
    }
}

fun getMonthFromDateString(dateString: String?): Int {
    return try {
        var dateFormat = SimpleDateFormat(datePickerFormat)
        var date = dateFormat.parse(dateString)
        dateFormat = SimpleDateFormat("MM")
        dateFormat.format(date).toInt().minus(1)
    } catch (e: Exception) {
        getCurrentMonth()
    }
}


fun getYearFromDateString(dateString: String?): Int {
    return try {
        var dateFormat = SimpleDateFormat(datePickerFormat)
        var date = dateFormat.parse(dateString)
        dateFormat = SimpleDateFormat("yyyy")
        dateFormat.format(date).toInt()
    } catch (e: Exception) {
        getCurrentYear()
    }
}

fun getCurrentDay(): Int {
    var dateFormat = SimpleDateFormat("dd")
    return dateFormat.format(getCurrentUTCDate()).toInt()
}

fun getCurrentMonth(): Int {
    var dateFormat = SimpleDateFormat("MM")
    return dateFormat.format(getCurrentUTCDate()).toInt().minus(1)
}

fun getCurrentYear(): Int {
    var dateFormat = SimpleDateFormat("yyyy")
    return dateFormat.format(getCurrentUTCDate()).toInt()
}

fun getCurrentUTCDate(): Long {
    return System.currentTimeMillis()
}
