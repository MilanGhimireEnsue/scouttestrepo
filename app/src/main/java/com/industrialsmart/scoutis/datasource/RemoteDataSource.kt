package com.industrialsmart.scoutis.datasource

import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.service.ApiService
import com.industrialsmart.scoutis.interfaces.DataSource
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.extensions.*
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe

class RemoteDataSource(private val apiService: ApiService) : DataSource {
    //for managing the required data all over the application
    private val scoutDataManager: ScoutDataManager? = ScoutDataManager()

    override fun getGduList(): Single<List<GDUDataItem?>> {
        val handler = SingleOnSubscribe<List<GDUDataItem?>> { emitter ->
            val gduList: MutableList<GDUDataItem?> = ArrayList()
            run {
                try {
                    apiService.getGduList().subscribe({
                        val scoutList = scoutDataManager?.getRawScoutList()
                        it?.forEach { gduItem ->
                            if (gduItem != null) {
                                gduList.add(
                                    GDUDataItem(
                                        gduItem.id,
                                        gduItem.name,
                                        gduItem.code,
                                        gduItem.description,
                                        gduItem.subjectWhenHarvesting,
                                        gduItem.subjectWhenPlanting,
                                        gduItem.subjectId,
                                        gduItem.holdingId,
                                        scoutList?.find { it?.holdingId == gduItem.holdingId },
                                        gduItem.subjectTypeId
                                    )
                                )
                            }
                        }
                        emitter.onSuccess(gduList)
                    }, {
                        it.printStackTrace()
                        emitter.onError(it)
                    })
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun getRawScoutList(): Single<List<ScoutItemResponse?>> {
        val handler = SingleOnSubscribe<List<ScoutItemResponse?>> { emitter ->
            if (scoutDataManager?.getRawScoutList()?.isNullOrEmpty() == false) {
                emitter.onSuccess(scoutDataManager.getRawScoutList()!!)
            } else {
                apiService.getScoutsList().subscribe({
                    scoutDataManager?.setRawScoutList(it)
                    emitter.onSuccess(it)
                }, {
                    emitter.onError(it)
                })
            }
        }
        return Single.create(handler)
    }

    override fun getSubjectList() = apiService.getSubjectList()

    override fun requestAddGDU(gduItem: GDUResponse?) = apiService.requestAddGDU(gduItem)

    override fun requestGDUDelete(id: String?) = apiService.requestGDUDelete(id)

    override fun getRegionList() = apiService.getRegionList()

    override fun getOccasionList(): Single<List<OccasionItem?>> {
        val handler = SingleOnSubscribe<List<OccasionItem?>> { emitter ->
            apiService.getOccasionList().subscribe({
                it?.forEach { occasion ->
                    occasion?.startDateTime = occasion?.startDateTime?.getLocalizedDate()
                    occasion?.endDateTime = occasion?.startDateTime?.addMinutes(occasion.duration)
                }
                emitter.onSuccess(it)
            }, {
                emitter.onError(it)
            })
        }
        return Single.create(handler)
    }

    override fun requestAddOccasion(occasionItem: OccasionItem?): Single<OccasionItem?> {
        val handler = SingleOnSubscribe<OccasionItem?> { emitter ->
            apiService.requestAddOccasion(occasionItem)?.subscribe({
                if (it != null) {
                    emitter.onSuccess(it)
                } else {
                    emitter.onError(Throwable(message = "Error occurred. Please try again."))
                }
            }, {
                emitter.onError(it)
            })
        }
        return Single.create(handler)
    }

    override fun requestDeleteOccasion(id: String?) = apiService.requestDeleteOccasion(id)
    override fun getNotificationList() = apiService.getNotificationList()
    override fun postNotificationRead(notificationItem: NotificationItem?) = apiService.postNotificationRead(notificationItem)
    override fun postBulkNotificationRead(ids: String?) = apiService.postBulkNotificationRead(ids)
    override fun getNotificationSettingList() = apiService.getNotificationSettingList()
    override fun requestDeleteNotificationSetting(id: String?) = apiService.requestDeleteNotificationSetting(id)
    override fun requestAddNotificationSetting(notificationSettingItem: NotificationSettingItem?) =
        apiService.requestAddNotificationSetting(notificationSettingItem)

    override fun requestPostFCMToken(fcmTokenPostData: FCMTokenPostData?) = apiService.requestPostFCMToken(fcmTokenPostData)
    override fun getUnreadNotificationCount() = apiService.getUnreadNotificationCount()
    override fun getReferencesList() = apiService.getReferencesList()
    override fun getSysInfo() = apiService.getSysInfo()
    override fun postSensorInfo(sensorList: List<Sensor?>) = apiService.postSensorInfo(sensorList)
    override fun postScoutInfo(scoutModelList: List<ScoutModel?>) = apiService.postScoutInfo(scoutModelList)
    override fun postUserData(userDataResponse: UserDataResponse?) = apiService.postUserData(userDataResponse)
    override fun getUserData() = apiService.getUserData()
    override fun getSensorData(getSensorDataRequest: GetSensorDataRequest?): Single<List<ReadingDataItem?>> {
        val handler = SingleOnSubscribe<List<ReadingDataItem?>> { emitter ->
            apiService.getSensorData(
                getSensorDataRequest?.whenFrom,
                getSensorDataRequest?.whenTo,
                getSensorDataRequest?.readingTypeId,
                getSensorDataRequest?.sensorId
            ).subscribe({
                if (getSensorDataRequest?.categoryCode == TemperatureUnit.CELSIUS.unitCode || getSensorDataRequest?.categoryCode == TemperatureUnit.FAHRENHEIT.unitCode) {
                    if (UserSetting.temperatureUnit == TemperatureUnit.FAHRENHEIT) {
                        it.forEach {
                            it?.value = convertTemperature(it?.value, TemperatureUnit.FAHRENHEIT)
                        }
                    }
                }
                if (getSensorDataRequest?.categoryCode == SoilECUnit.USCM.unitCode || getSensorDataRequest?.categoryCode == SoilECUnit.DSM.unitCode) {
                    if (UserSetting.soilECUnit == SoilECUnit.DSM) {
                        it.forEach {
                            it?.value = convertSoilEC(it?.value, SoilECUnit.DSM)
                        }
                    }
                }
                emitter.onSuccess(it)
            }, {
                emitter.onError(it)
            })
        }
        return Single.create(handler)


    }

    override fun getReadingTypeList(): Single<List<ReadingTypeItem?>> {
        val handler = SingleOnSubscribe<List<ReadingTypeItem?>> { emitter ->
            if (scoutDataManager?.getReadingTypeList()?.isNullOrEmpty() == false) {
                emitter.onSuccess(scoutDataManager.getReadingTypeList()!!)
            } else {
                apiService.getReadingTypeList().subscribe({
                    scoutDataManager?.setReadingTypeList(it)
                    emitter.onSuccess(it)
                }, {
                    emitter.onError(it)
                })
            }
        }
        return Single.create(handler)
    }

    override fun getScoutsList(): Single<List<ScoutModel?>> {
        val handler = SingleOnSubscribe<List<ScoutModel?>> { emitter ->
            if (scoutDataManager?.getScoutList()?.isNullOrEmpty() == false) {
                emitter.onSuccess(scoutDataManager.getScoutList()!!)
            } else {
                val scoutList: MutableList<ScoutModel?> = ArrayList()
                run {
                    try {
                        //request scout list
                        apiService.getScoutsList().subscribe({
                            it?.forEach { scoutItem ->
                                scoutDataManager?.setRawScoutList(it)
                                if (scoutItem != null) {
                                    scoutList.add(
                                        ScoutModel(
                                            scoutItem.id,
                                            scoutItem.name,
                                            scoutItem.assetName,
                                            coordinateLatitude = scoutItem.coordinateLatitude,
                                            coordinateLongitude = scoutItem.coordinateLongitude,
                                            notificationConfirmationCount = scoutItem.notificationConfirmationCount
                                        )
                                    )
                                }
                            }
                            //request sensor list
                            apiService.getSensorsList().subscribe({
                                val sensorsList: MutableList<Sensor> = ArrayList()
                                it?.forEach { sensorItem ->
                                    //calculating required sensor reading from desc string
                                    if (!sensorItem?.description.isNullOrEmpty()) {
                                        var dateTimeString = ""
                                        val sensorReading: MutableList<SensorReading> = ArrayList()
                                        val labelList: MutableList<String> = ArrayList()
                                        val unitList: MutableList<String> = ArrayList()
                                        val valueList: MutableList<String> = ArrayList()
                                        val codeList: MutableList<String> = ArrayList()
                                        val descList =
                                            sensorItem?.description?.split(",") as MutableList<String>
                                        if (!descList.isNullOrEmpty()) {
                                            dateTimeString = descList.first()
                                            descList.removeAt(0)
                                            descList.forEachIndexed { index, s ->
                                                when (index % 5) {
                                                    0 -> codeList.add(s)
                                                    1 -> labelList.add(s)
                                                    3 -> valueList.add(s)
                                                    4 -> unitList.add(s)
                                                }
                                            }
                                            labelList.forEachIndexed { index, s ->
                                                var foreGroudTint = R.color.colorForegroundDarkest
                                                when (index) {
                                                    0 -> foreGroudTint = R.color.reading_color_1
                                                    1 -> foreGroudTint = R.color.reading_color_2
                                                    2 -> foreGroudTint = R.color.reading_color_3
                                                }
                                                if (valueList[index].isNotEmpty())
                                                    sensorReading.add(
                                                        SensorReading(
                                                            s,
                                                            valueList[index],
                                                            unitList[index],
                                                            codeList[index],
                                                            foreGroundTint = foreGroudTint
                                                        )
                                                    )
                                            }
                                        }
                                        //add the modified value to sensor list
                                        sensorsList.add(
                                            Sensor(
                                                sensorItem.id,
                                                sensorItem.name,
                                                sensorItem.assetName,
                                                dateTimeString,
                                                sensorItem.scoutId,
                                                sensorItem.description,
                                                sensorReading
                                            )
                                        )
                                        //assign the sensor list to particular scout model
                                        scoutList.forEach {
                                            it?.sensors =
                                                sensorsList.filter { sensorItem -> sensorItem.scoutId == it?.id }
                                            it?.dateTime = it?.sensors?.maxBy { it.dateTime }?.dateTime.getFormattedDate()
                                        }
                                    }
                                }
                                scoutDataManager?.setScoutList(scoutList.sortedBy { it?.title.toString() })
                                emitter.onSuccess(scoutList.sortedBy { it?.title.toString() })
                            }, {
                                it.printStackTrace()
                                emitter.onError(it)
                            })
                        }, {
                            it.printStackTrace()
                            emitter.onError(it)
                        })
                    } catch (e: Exception) {
                        e.printStackTrace()
                        emitter.onError(e)
                    }
                }
            }
        }
        return Single.create(handler)
    }

    override fun clearAllData() {
        scoutDataManager?.clearScoutManagerData()
    }
}