package com.industrialsmart.scoutis.datasource

import android.content.Context
import com.google.gson.Gson
import com.industrialsmart.scoutis.interfaces.DataSource
import com.industrialsmart.scoutis.models.*
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe

class LocalDataSource(val context: Context) : DataSource {

    override fun requestAddGDU(gduItem: GDUResponse?): Single<GDUResponse?> {
        val handler = SingleOnSubscribe<GDUResponse?> { emitter ->
            run {
                try {
                    emitter.onSuccess(GDUResponse())
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun requestGDUDelete(id: String?): Single<Boolean?> {
        val handler = SingleOnSubscribe<Boolean?> { emitter ->
            run {
                try {
                    emitter.onSuccess(true)
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun getRegionList(): Single<List<AppRegionItem?>> {
        TODO("Not yet implemented")
    }

    override fun getOccasionList(): Single<List<OccasionItem?>> {
        TODO("Not yet implemented")
    }

    override fun requestAddOccasion(occasionItem: OccasionItem?): Single<OccasionItem?> {
        TODO("Not yet implemented")
    }

    override fun requestDeleteOccasion(id: String?): Single<Boolean?> {
        TODO("Not yet implemented")
    }

    override fun getNotificationList(): Single<List<NotificationItem?>> {
        TODO("Not yet implemented")
    }

    override fun postNotificationRead(notificationItem: NotificationItem?): Completable {
        TODO("Not yet implemented")
    }

    override fun postBulkNotificationRead(ids: String?): Completable {
        TODO("Not yet implemented")
    }

    override fun getNotificationSettingList(): Single<List<NotificationSettingItem?>> {
        TODO("Not yet implemented")
    }

    override fun requestDeleteNotificationSetting(id: String?): Single<Boolean?> {
        TODO("Not yet implemented")
    }

    override fun requestAddNotificationSetting(notificationSettingItem: NotificationSettingItem?): Single<NotificationSettingItem?> {
        TODO("Not yet implemented")
    }

    override fun requestPostFCMToken(fcmTokenPostData: FCMTokenPostData?): Completable {
        TODO("Not yet implemented")
    }

    override fun getUnreadNotificationCount(): Single<Int?> {
        TODO("Not yet implemented")
    }

    override fun getReferencesList(): Single<List<ReferenceItem?>> {
        TODO("Not yet implemented")
    }

    override fun getSysInfo(): Single<SysInfoResponse?> {
        TODO("Not yet implemented")
    }

    override fun clearAllData() {
        TODO("Not yet implemented")
    }

    override fun getGduList(): Single<List<GDUDataItem?>> {
        val handler = SingleOnSubscribe<List<GDUDataItem?>> { emitter ->
            run {
                try {
                    emitter.onSuccess(emptyList())
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun getRawScoutList(): Single<List<ScoutItemResponse?>> {
        val handler = SingleOnSubscribe<List<ScoutItemResponse?>> { emitter ->
            run {
                try {
                    emitter.onSuccess(emptyList())
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun getSubjectList(): Single<List<AppSubjectItem?>> {
        val handler = SingleOnSubscribe<List<AppSubjectItem?>> { emitter ->
            run {
                try {
                    emitter.onSuccess(emptyList())
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun postSensorInfo(sensorList: List<Sensor?>): Completable {
        return Completable.create {
            return@create
        }
    }

    override fun postScoutInfo(scoutModelList: List<ScoutModel?>): Completable {
        return Completable.create {
            return@create
        }
    }

    override fun postUserData(userDataResponse: UserDataResponse?): Single<UserDataResponse?> {
        val handler = SingleOnSubscribe<UserDataResponse?> { emitter ->
            run {
                try {
                    val jsonString = context.assets.open("data/readingtype.json").bufferedReader()
                        .use { it.readText() }
                    val value = Gson().fromJson(jsonString, UserDataResponse::class.java)
                    emitter.onSuccess(value)
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun getUserData(): Single<UserDataResponse?> {
        val handler = SingleOnSubscribe<UserDataResponse?> { emitter ->
            run {
                try {
                    val jsonString = context.assets.open("data/readingtype.json").bufferedReader()
                        .use { it.readText() }
                    val value = Gson().fromJson(jsonString, UserDataResponse::class.java)
                    emitter.onSuccess(value)
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun getSensorData(getSensorDataRequest: GetSensorDataRequest?): Single<List<ReadingDataItem?>> {
        val handler = SingleOnSubscribe<List<ReadingDataItem?>> { emitter ->
            run {
                try {
                    emitter.onSuccess(emptyList())
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun getReadingTypeList(): Single<List<ReadingTypeItem?>> {
        val handler = SingleOnSubscribe<List<ReadingTypeItem?>> { emitter ->
            run {
                try {
                    val jsonString = context.assets.open("data/readingtype.json").bufferedReader()
                        .use { it.readText() }
                    val value = Gson().fromJson(jsonString, Array<ReadingTypeItem?>::class.java)
                    emitter.onSuccess(emptyList())
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }

    override fun getScoutsList(): Single<List<ScoutModel?>> {
        val handler = SingleOnSubscribe<List<ScoutModel?>> { emitter ->
            run {
                try {
                    val jsonString = context.assets.open("data/scouts.json").bufferedReader()
                        .use { it.readText() }
                    val value = Gson().fromJson(jsonString, Array<ScoutModel?>::class.java)
                    emitter.onSuccess(value.asList())
                } catch (e: Exception) {
                    e.printStackTrace()
                    emitter.onError(e)
                }
            }
        }
        return Single.create(handler)
    }
}