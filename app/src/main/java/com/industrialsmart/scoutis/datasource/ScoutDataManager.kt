package com.industrialsmart.scoutis.datasource

import com.industrialsmart.scoutis.models.ReadingTypeItem
import com.industrialsmart.scoutis.models.ScoutItemResponse
import com.industrialsmart.scoutis.models.ScoutModel

class ScoutDataManager {
    private var scoutList: List<ScoutModel?>? = null
    private var readingTypeList: List<ReadingTypeItem?>? = null
    private var rawScoutList: List<ScoutItemResponse?>? = null

    fun getRawScoutList(): List<ScoutItemResponse?>?{
        return rawScoutList
    }

    fun setRawScoutList(list: List<ScoutItemResponse?>?){
        rawScoutList = list
    }

    fun getScoutList(): List<ScoutModel?>? {
        return scoutList
    }

    fun setScoutList(list: List<ScoutModel?>) {
        scoutList = list
    }

    fun getReadingTypeList(): List<ReadingTypeItem?>? {
        return readingTypeList
    }

    fun setReadingTypeList(list: List<ReadingTypeItem?>) {
        readingTypeList = list
    }

    fun clearScoutManagerData(){
        scoutList = null
        readingTypeList = null
        rawScoutList = null
    }
}