package com.industrialsmart.scoutis.view.dashboard.notification.setting

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.listener.EditDeleteListener
import com.industrialsmart.scoutis.models.NotificationSettingItem
import com.industrialsmart.scoutis.models.ReadingTypeItem
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.models.Sensor
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.utils.successToast
import com.industrialsmart.scoutis.view.dashboard.notification.setting.adapter.NotificationSettingAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_notification_setting.*
import kotlinx.android.synthetic.main.layout_empty.*

class NotificationSettingFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_notification_setting

    private var notificationSettingResponse: MutableList<NotificationSettingItem?>? = null
    private var scoutListResponse: List<ScoutModel?>? = null
    private var readingTypeList: List<ReadingTypeItem?>? = null

    override fun initView() {
        super.initView()
        requestScoutList()
        requestReadingType()
    }

    override fun onResume() {
        super.onResume()
        (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.notification_settings), R.drawable.ic_add_white)
        requestNotificationSetting()
    }

    override fun onRightIconClicked() {
        super.onRightIconClicked()
        if (scoutListResponse != null && readingTypeList != null)
            changeFragment(AddNotificationSettingFragment.newInstance(scoutListResponse!!, readingTypeList!!))
    }

    private fun requestNotificationSetting() {
        showProgressDialog()
        mDisposable?.add(dataSource.getNotificationSettingList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    notificationSettingResponse = it.toMutableList()
                    handleNotificationSetting(it)
                    dismissProgressDialog()
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun requestScoutList() {
        mDisposable?.add(
            dataSource.getScoutsList().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    scoutListResponse = it
                }, {
                })
        )
    }

    private fun requestReadingType() {
        mDisposable?.add(
            dataSource.getReadingTypeList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    readingTypeList = it
                }, {
                })
        )
    }

    private fun handleNotificationSetting(it: List<NotificationSettingItem?>?) {
        if (it?.isNullOrEmpty() == true) {
            settingRV.hide()
            emptyLayout.show()
        } else {
            settingRV.show()
            emptyLayout.hide()
        }

        val allSensorList: MutableList<Sensor?> = ArrayList()
        scoutListResponse?.forEach {
            it?.sensors?.let { it1 -> allSensorList.addAll(it1) }
        }
        it?.forEach { notificationSetting ->
            notificationSetting?.sensorItem = allSensorList.find { it?.id == notificationSetting?.dataProviderId }
            val scoutId = notificationSetting?.sensorItem?.scoutId
            notificationSetting?.scoutItem = scoutListResponse?.find { it?.id == scoutId }
            notificationSetting?.readingTypeItem = readingTypeList?.find { it?.readingTypeId == notificationSetting?.readingTypeId }
        }
        val filteredList = it?.filter { it?.scoutItem != null }
        settingRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = filteredList?.let { it1 ->
                NotificationSettingAdapter(it1, object : EditDeleteListener {
                    override fun onClicked(dataItem: Any?) {
                        if (scoutListResponse != null && readingTypeList != null)
                            changeFragment(
                                AddNotificationSettingFragment.newInstance(
                                    scoutListResponse!!,
                                    readingTypeList!!,
                                    dataItem as NotificationSettingItem?
                                )
                            )
                    }

                    override fun onEdit(dataItem: Any?) {
                        if (scoutListResponse != null && readingTypeList != null)
                            changeFragment(
                                AddNotificationSettingFragment.newInstance(
                                    scoutListResponse!!,
                                    readingTypeList!!,
                                    dataItem as NotificationSettingItem?
                                )
                            )
                    }

                    override fun onDelete(dataItem: Any?) {
                        val mDataItem = dataItem as NotificationSettingItem?
                        showDialog(
                            message = "Are you sure you want to permanently delete this setting?",
                            title = "Delete ${mDataItem?.periodTitle}",
                            positiveText = "Delete",
                            negativeText = "Cancel"
                        ) {
                            requestNotificationSettingDelete(mDataItem)
                        }
                    }
                })
            }
        }
    }

    private fun requestNotificationSettingDelete(mDataItem: NotificationSettingItem?) {
        showProgressDialog()
        mDisposable?.add(
            dataSource.requestDeleteNotificationSetting(mDataItem?.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it == true) {
                        context?.successToast("Setting deleted successfully.")
                        notificationSettingResponse?.remove(mDataItem)
                        notificationSettingResponse?.toList()?.let { it1 -> handleNotificationSetting(it1) }
                    }
                }, {
                    dismissProgressDialog()
                })
        )
    }

    companion object {
        fun newInstance(): NotificationSettingFragment {
            val fragment = NotificationSettingFragment()
            return fragment
        }
    }
}