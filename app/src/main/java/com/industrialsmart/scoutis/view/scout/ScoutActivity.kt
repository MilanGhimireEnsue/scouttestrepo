package com.industrialsmart.scoutis.view.scout

import android.content.res.Configuration
import android.view.WindowManager
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseActivity
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.Constants
import com.industrialsmart.scoutis.utils.extensions.dpToPixels
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.view.dashboard.adapter.ViewPagerAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_scout.*
import org.jetbrains.anko.startActivity


class ScoutActivity : BaseActivity() {
    override val layoutId: Int
        get() = R.layout.activity_scout

    private var scoutData: ScoutModel? = null
    private var preSelectedSensor: Sensor? = null
    private var scoutChartType: SCOUT_CHART_TYPE? = null
    private var gduDataItem: GDUDataItem? = null
    private var isNavigationFromMap = false //to hide the location icon if navigated from map

    private var readingTypeList: List<ReadingTypeItem?>? = null

    //list of sensor with reading type for comparision
    private var compareSensorDataList: MutableList<CompareSensorData?> = ArrayList()

    //chart data date type currently shown
    private var chartDataDate: CHART_DATA_DATE = CHART_DATA_DATE.WEEKLY

    //currently selected startDate and endDate
    private var selectedStartDate: String? = null
    private var selectedEndDate: String? = null

    private var isReadingTypeFetched = false
    private var isGDUFetched = false

    private var scoutRelatedGDU: List<GDUDataItem?>? = null

    var isLandscape = false

    override fun initView() {
        super.initView()
        scoutData = intent.getParcelableExtra<ScoutModel?>(EXTRA_SCOUT_DATA)
        preSelectedSensor = intent.getParcelableExtra<Sensor?>(EXTRA_SENSOR_DATA)
        gduDataItem = intent.getParcelableExtra<GDUDataItem?>(EXTRA_GDU_DATA)
        if (intent.hasExtra(EXTRA_NAVIGATION_FROM_MAP))
            isNavigationFromMap = intent.getBooleanExtra(EXTRA_NAVIGATION_FROM_MAP, false)
        scoutChartType = intent.getSerializableExtra(EXTRA_SCOUT_CHART_TYPE) as SCOUT_CHART_TYPE?
        scoutData?.sensors = scoutData?.sensors?.filter { it.isActive == true }
        scoutName.text = scoutData?.title
        backButton.setOnClickListener {
            onBackPressed()
        }
        if (isNavigationFromMap)
            locationImageView.hide()
        locationImageView.setOnClickListener {
            startActivity<ToolbarNavActivity>(
                ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.MAP_WITH_SCOUT,
                ToolbarNavActivity.EXTRA_SCOUT_DATA to scoutData
            )
        }
        requestReadingType()
        requestGDUList()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        val scoutFragment = ((sensorViewPager.adapter as ViewPagerAdapter?)?.getItem(sensorViewPager.currentItem) as ScoutFragment?)
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isLandscape = true
            headerLayout.hide()
            window.clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
            window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            scoutFragment?.let { scoutFragment.configurationChange(true) }
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            isLandscape = false
            headerLayout.show()
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            window.addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN)
            scoutFragment?.let { scoutFragment.configurationChange(false) }
        }
    }

    private fun requestReadingType() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getReadingTypeList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    dismissProgressDialog()
                    handleReadingTypeResponse(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleReadingTypeResponse(it: List<ReadingTypeItem?>) {
        it.forEach {
            //change the temperature category code(unit) according to user setting
            if (it?.categoryCode == TemperatureUnit.CELSIUS.unitCode && UserSetting.temperatureUnit == TemperatureUnit.FAHRENHEIT) {
                it?.categoryCode = TemperatureUnit.FAHRENHEIT.unitCode
            }
            //change the soil category code(unit) according to user setting
            if (it?.categoryCode == SoilECUnit.USCM.unitCode && UserSetting.soilECUnit == SoilECUnit.DSM) {
                it?.categoryCode = SoilECUnit.DSM.unitCode
            }
        }
        readingTypeList = it
        isReadingTypeFetched = true
        if (isGDUFetched && scoutData?.sensors?.isNotEmpty() == true)
            prepareChartData()
    }

    private fun requestGDUList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getGduList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (!it.isNullOrEmpty())
                        handleGduResponse(it)
                }, {
                    dismissProgressDialog()
                    it.printStackTrace()
                })
        )
    }

    private fun handleGduResponse(gduListResponse: List<GDUDataItem?>?) {
        isGDUFetched = true
        scoutRelatedGDU = gduListResponse?.filter { it?.rawScoutData?.id == scoutData?.id }
        if (isReadingTypeFetched)
            prepareChartData()
    }

    private fun prepareChartData() {
        prepareSensorComparableList()
        if (scoutChartType == SCOUT_CHART_TYPE.SCOUT_CHART) {
            setUpScoutViewPager()
        } else {
            changeFragment(ScoutGDUFragment.newInstance(gduDataItem, scoutData), addToBackStack = false)
        }
    }

    private fun setUpScoutViewPager() {
        sensorViewPager.pageMargin = context.dpToPixels(10f)
        val pagerAdapter = ViewPagerAdapter(supportFragmentManager, context)
        scoutData?.sensors?.forEach {
            pagerAdapter.addFragment(ScoutFragment.newInstance(it))
        }
        sensorViewPager.adapter = pagerAdapter
        circleIndicator.setViewPager(sensorViewPager)
        if (preSelectedSensor != null) { //handling directly sensor item selection from dashboard
            val pos = scoutData?.sensors?.indexOf(preSelectedSensor!!)
            if (pos != null)
                sensorViewPager.currentItem = pos
        }
        onConfigurationChanged(resources.configuration) //for setting initial  configuration changes
    }


    /**
     * Handling switching from compare screen when only one compare type is selected
     */
    fun switchSensorAndReading(toBeCompared: ToBeCompared?) {
        //to handle the population of same chart remaining in compare screen
        val toBeSwitchedSensor = scoutData?.sensors?.find { it.id == toBeCompared?.sensorId }
        val toBeSwitchedSensorIndex = scoutData?.sensors?.indexOf(toBeSwitchedSensor)
        if (toBeCompared?.scoutChartType == SCOUT_CHART_TYPE.SCOUT_CHART) { //to generate scout chart
            setUpScoutViewPager()
            if (toBeSwitchedSensorIndex != null) {
                container.hide() //to hide the overlay GDU screen # com.industrialsmart.scoutis.view.scout.ScoutCompareFragment)
                sensorViewPager.currentItem = toBeSwitchedSensorIndex
                ((sensorViewPager.adapter as ViewPagerAdapter).getItem(toBeSwitchedSensorIndex) as ScoutFragment).changeReadingType(toBeCompared.comparableReadingType) //update reading type
            }
        } else {
            container.show()
            val gduItem = scoutRelatedGDU?.find { it?.id == toBeCompared?.sensorId }
            changeFragment(ScoutGDUFragment.newInstance(gduItem, scoutData), addToBackStack = false, cleanStack = true)
        }
    }

    /**
     * Prepare the list of comparable sensor
     */
    private fun prepareSensorComparableList() {
        val comparableSensorList: MutableList<ComparableSensor?> = ArrayList()
        scoutData?.sensors?.forEachIndexed { index, sensor ->
            val toBeComparedList: MutableList<ToBeCompared?> = ArrayList()
            sensor.readings?.forEach {
                toBeComparedList.add(ToBeCompared(sensor?.title, sensor?.id, getReadingTypeFromCode(it.code)))
            }
            val comparableSensor = ComparableSensor(sensor.title, sensor.id, toBeComparedList)
            comparableSensorList.add(comparableSensor)
        }
        compareSensorDataList.add(CompareSensorData("Sensors", comparableSensorList))
        val comparableReadingType: MutableList<ToBeCompared?> = ArrayList()
        val comparableSensorList1: MutableList<ComparableSensor?> = ArrayList()
        scoutRelatedGDU?.forEach {
            comparableReadingType.add(
                ToBeCompared(
                    "Derived Data 1",
                    it?.id,
                    ReadingTypeItem(readingTypeId = "E10B3A95-5080-4C53-B148-43011AB33105", name = it?.name, categoryCode = "gdd"),
                    scoutChartType = ScoutActivity.Companion.SCOUT_CHART_TYPE.GDU_CHART
                )
            )
        }
        comparableSensorList1.add(ComparableSensor("Growing Data Unit", Constants.GDU_SENSOR_ID, comparableReadingType))
        compareSensorDataList.add(CompareSensorData("Derived Data 1", comparableSensorList1))
    }

    /**
     * Returns reading type object from reading type code
     */
    fun getReadingTypeFromCode(readingCode: String?): ReadingTypeItem? {
        return readingTypeList?.find { it?.code == readingCode }
    }

    /**
     * Returns reading type id from reading type code
     */
    fun getReadingIdFromCode(readingCode: String?): String? {
        return readingTypeList?.find { it?.code == readingCode }?.readingTypeId
    }

    fun getRelatedGDUList(): List<GDUDataItem?>? {
        return scoutRelatedGDU
    }

    fun getComparableListOfSensor(): List<CompareSensorData?>? {
        return compareSensorDataList
    }

    fun setChartDataDate(chartDataDate: CHART_DATA_DATE) {
        this.chartDataDate = chartDataDate
    }

    fun getChartDataDate(): CHART_DATA_DATE {
        return chartDataDate
    }

    fun setSelectedStartEndDate(selectedStartDate: String?, selectedEndDate: String?) {
        this.selectedStartDate = selectedStartDate
        this.selectedEndDate = selectedEndDate
    }

    fun getSelectedStartDate(): String? {
        return selectedStartDate
    }

    fun getSelectedEndDate(): String? {
        return selectedEndDate
    }

    fun showContainer() {
        container.show()
    }

    companion object {
        enum class CHART_DATA_DATE {
            WEEKLY,
            MONTHLY,
            CUSTOM_RANGE
        }

        enum class SCOUT_CHART_TYPE {
            SCOUT_CHART,
            GDU_CHART
        }

        const val EXTRA_SCOUT_DATA = "extra_scout_data"
        const val EXTRA_GDU_DATA = "extra_gdu_data"
        const val EXTRA_SCOUT_CHART_TYPE = "extra_scout_chart_type"
        const val EXTRA_NAVIGATION_FROM_MAP = "extra_navigation_from_map"
        const val EXTRA_SENSOR_DATA = "extra_sensor_data"
    }
}