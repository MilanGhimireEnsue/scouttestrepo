package com.industrialsmart.scoutis.view.references

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.ReferenceItem
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.view.references.adapter.ReferenceAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_reference.*
import kotlinx.android.synthetic.main.layout_empty.*
import org.jetbrains.anko.startActivity

class ReferenceFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_reference

    override fun initView() {
        super.initView()
        (activity as ToolbarNavActivity).setToolbarTitle("References")
        requestReferenceList()
    }

    private fun requestReferenceList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getReferencesList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    handleReferenceList(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleReferenceList(it: List<ReferenceItem?>) {
        if (it.isEmpty()) {
            referenceRV.hide()
            emptyLayout.show()
        } else {
            referenceRV.show()
            emptyLayout.hide()
        }

        referenceRV?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = ReferenceAdapter(it) {
                context?.startActivity<ToolbarNavActivity>(
                    ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.WEB_VIEW,
                    ToolbarNavActivity.EXTRA_PARAM_1 to it?.content,
                    ToolbarNavActivity.EXTRA_PARAM_2 to it?.name
                )
            }
        }
    }

    companion object {
        fun newInstance(): ReferenceFragment {
            val fragment = ReferenceFragment()
            return fragment
        }
    }
}