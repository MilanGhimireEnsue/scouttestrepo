package com.industrialsmart.scoutis.view.scout.compare.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ComparableSensor
import com.industrialsmart.scoutis.models.ToBeCompared
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import kotlinx.android.synthetic.main.item_sensor_compare.view.*

class SensorCompareAdapter(private var comparableSensorList: List<ComparableSensor?>, val itemCheckChange: (ToBeCompared, Boolean) -> Unit) :
    RecyclerView.Adapter<SensorCompareAdapter.MyViewHolder>() {

    lateinit var mRecyclerView: RecyclerView

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sensor_compare, parent, false)
        return MyViewHolder(view, parent.context, viewPool, itemCheckChange)
    }

    override fun getItemCount(): Int {
        return comparableSensorList.size
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.mRecyclerView = recyclerView
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(comparableSensorList[position], position) { position, hiddenStatus ->
            comparableSensorList[position]?.isHidden = hiddenStatus
            mRecyclerView.post {
                notifyDataSetChanged()
            }
        }
    }

    class MyViewHolder(
        view: View,
        private val mContext: Context,
        private val viewPool: RecyclerView.RecycledViewPool,
        private val itemCheckChange: (ToBeCompared, Boolean) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(comparableSensor: ComparableSensor?, position: Int, isHiddenCallBack: (Int, Boolean) -> Unit) {
            itemView.sensorName.text = "${comparableSensor?.sensorName} (${comparableSensor?.comparableReadingType?.size})"
            if (comparableSensor?.comparableReadingType?.isNotEmpty() == true)
                itemView.readingTypeListRV.apply {
                    layoutManager = LinearLayoutManager(mContext, RecyclerView.VERTICAL, false)
                    adapter = SensorReadingCompareAdapter(
                        comparableSensor.comparableReadingType,
                        itemCheckChange
                    )
                    setRecycledViewPool(viewPool)
                    isNestedScrollingEnabled = false
                }
            if (comparableSensor?.isHidden == true) {
                itemView.readingTypeListRV.hide()
                itemView.sensorName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_right, 0, 0, 0)
            } else {
                itemView.readingTypeListRV.show()
                itemView.sensorName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_down, 0, 0, 0)
            }
            itemView.sensorName.setOnClickListener {
                if (comparableSensor?.isHidden == true)
                    isHiddenCallBack.invoke(position, false)
                else
                    isHiddenCallBack.invoke(position, true)
            }
        }
    }
}