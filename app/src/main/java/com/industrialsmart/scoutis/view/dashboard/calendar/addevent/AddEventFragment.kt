package com.industrialsmart.scoutis.view.dashboard.calendar.addevent

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.OccasionItem
import com.industrialsmart.scoutis.models.UserInfo
import com.industrialsmart.scoutis.utils.*
import com.industrialsmart.scoutis.utils.extensions.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_add_event.*
import kotlinx.android.synthetic.main.fragment_add_event.addButton
import kotlinx.android.synthetic.main.fragment_add_event.descriptionEditText
import kotlinx.android.synthetic.main.fragment_add_event.endDateEditText
import kotlinx.android.synthetic.main.fragment_add_event.startDateEditText
import kotlinx.android.synthetic.main.fragment_add_gdu.*
import org.threeten.bp.LocalDate
import java.util.*


class AddEventFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_add_event

    private var preSelectedDate: LocalDate? = null
    private var existingDataItem: OccasionItem? = null

    var isShowAllOption = false

    override fun initView() {
        super.initView()
        if (existingDataItem == null) {
            (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.new_event))
            addButton.text = "ADD"
        } else {
            (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.edit_event))
            addButton.text = "DONE"
        }
        setInitialStartDate()
        showAllOptions.setOnClickListener {
            handleShowAllOption()
        }
        startDateEditText.setOnClickListener {
            showDateTimePicker(Constants.DateType.START_DATE)
        }
        endDateEditText.setOnClickListener {
            showDateTimePicker(Constants.DateType.END_DATE)
        }
        addButton.setOnClickListener {
            validateData()
        }
        populateEditDataIfAvailable()
    }

    /**
     * Populate edit event data
     */
    private fun populateEditDataIfAvailable() {
        if (existingDataItem != null) {
            titleEditText.textValue = existingDataItem?.name.toString()
            val startCalendarDate = existingDataItem?.startDateTime?.getCalendarFromServerDate()
            if (startCalendarDate != null)
                startDateEditText.textValue = getDateTimeString(startCalendarDate)
            //fill end date
            if (!existingDataItem?.endDateTime.isNullOrEmpty()) {
                val endCalendarDate = existingDataItem?.endDateTime?.getCalendarFromServerDate()
                if (endCalendarDate != null) {
                    endDateEditText.textValue = getDateTimeString(endCalendarDate)
                }
            }
            //setup all day check
            if (existingDataItem?.isAllDay == true) {
                isAllDayEventVisibility.isChecked = true
            }
            //fill location
            if (!existingDataItem?.where.isNullOrEmpty()) {
                locationEditText.textValue = existingDataItem?.where.toString()
            }
            //fill description
            if (!existingDataItem?.description.isNullOrEmpty()) {
                descriptionEditText.textValue = existingDataItem?.description.toString()
            }
        }
    }

    /**
     * Set initial start date according to user action
     */
    private fun setInitialStartDate() {
        val date = Calendar.getInstance()
        if (preSelectedDate != null) {
            date.set(preSelectedDate!!.year, preSelectedDate!!.monthValue - 1, preSelectedDate!!.dayOfMonth)
        }
        date.set(Calendar.HOUR_OF_DAY, 8)
        date.set(Calendar.MINUTE, 0)
        startDateEditText.textValue = getDateTimeString(date)
    }

    private fun showDateTimePicker(dateType: Constants.DateType) {
        val currentDate: Calendar = Calendar.getInstance()
        val preSelectedYear = if (preSelectedDate != null)
            preSelectedDate?.year
        else
            currentDate.get(Calendar.YEAR)

        val preSelectedMonth: Int? = if (preSelectedDate != null)
            preSelectedDate!!.monthValue - 1
        else
            currentDate.get(Calendar.MONTH)

        val preSelectedDay = if (preSelectedDate != null)
            preSelectedDate?.dayOfMonth
        else
            currentDate.get(Calendar.DATE)


        val date = Calendar.getInstance()
        DatePickerDialog(context!!, OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            date.set(year, monthOfYear, dayOfMonth)
            TimePickerDialog(context, OnTimeSetListener { view, hourOfDay, minute ->
                date.set(Calendar.HOUR_OF_DAY, hourOfDay)
                date.set(Calendar.MINUTE, minute)
                when (dateType) {
                    Constants.DateType.START_DATE -> startDateEditText.textValue = getDateTimeString(date)
                    Constants.DateType.END_DATE -> endDateEditText.textValue = getDateTimeString(date)
                }
            }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show()
            /*if (!isAllDayEventVisibility.isChecked) {
                TimePickerDialog(context, OnTimeSetListener { view, hourOfDay, minute ->
                    date.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    date.set(Calendar.MINUTE, minute)
                    when (dateType) {
                        Constants.DateType.START_DATE -> startDateEditText.textValue = getDateTimeString(date)
                        Constants.DateType.END_DATE -> endDateEditText.textValue = getDateTimeString(date)
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show()
            } else {
                date.set(Calendar.HOUR_OF_DAY, 8)
                date.set(Calendar.MINUTE, 0)
                when (dateType) {
                    Constants.DateType.START_DATE -> startDateEditText.textValue = getDateTimeString(date)
                    Constants.DateType.END_DATE -> endDateEditText.textValue = getDateTimeString(date)
                }
            }*/

        }, preSelectedYear!!, preSelectedMonth!!, preSelectedDay!!).show()
    }

    private fun validateData() {
        if (titleEditText.textValue.isEmpty()) {
            context?.warningToast("Please enter event title.")
            return
        }
        if (startDateEditText.textValue.isEmpty()) {
            context?.warningToast("Please enter start date.")
            return
        }
        requestAddEvent()
    }

    private fun requestAddEvent() {
        var occasionItem: OccasionItem? = null
        if (existingDataItem == null) {
            occasionItem = OccasionItem(
                occasionTypeId = "99c78f17-0b8e-4915-ae6e-aefa00455bd9",
                startDateTime = getFormattedDateTime(startDateEditText.textValue),
                name = titleEditText.textValue,
                customerId = UserInfo.userData?.customerId,
                isAllDay = isAllDayEventVisibility.isChecked,
                code = UUID.randomUUID().toString()
            )
        } else {
            occasionItem = existingDataItem?.apply {
                name = titleEditText.textValue
                startDateTime = getFormattedDateTime(startDateEditText.textValue)
                isAllDay = isAllDayEventVisibility.isChecked
            }
        }
        if (isAllDayEventVisibility.isChecked)
            occasionItem?.duration = getTimeDiffInMinutes(
                startDateEditText.textValue,
                getEndDateTimeFromStartDate(startDateEditText.textValue)
            )
        occasionItem?.apply {
            where = if (locationEditText.textValue.isEmpty())
                "N/A"
            else
                locationEditText.textValue
            description = descriptionEditText.textValue
        }
        if (endDateEditText.textValue.isNotEmpty()) {
            occasionItem?.duration = getTimeDiffInMinutes(startDateEditText.textValue, endDateEditText.textValue)
        }
        showProgressDialog()
        mDisposable?.add(
            dataSource.requestAddOccasion(occasionItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (existingDataItem == null)
                        context?.successToast("Event added successfully.")
                    else
                        context?.successToast("Event edited successfully.")
                    requireActivity().onBackPressed()
                }, {
                    dismissProgressDialog()
                    showDialog(message = getString(R.string.unable_to_edit_event), positiveText = getString(R.string.ok), negativeText = "") {
                        requireActivity().onBackPressed()
                    }
                })
        )

    }


    private fun handleShowAllOption() {
        isShowAllOption = !isShowAllOption
        if (isShowAllOption) {
            showAllOptions.text = "Hide Optional Fields"
            showAllOptions.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_remove_black, 0, 0, 0)
            endTimeContainer.show()
            allDayEventContainer.show()
            locationContainer.show()
            descriptionContainer.show()
            descriptionEditText.show()
        } else {
            showAllOptions.text = "Show All Options"
            showAllOptions.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_add_white, 0, 0, 0)
            endTimeContainer.hide()
            allDayEventContainer.hide()
            locationContainer.hide()
            descriptionContainer.hide()
            descriptionEditText.hide()
        }
    }

    companion object {
        fun newInstance(preSelectedDate: LocalDate? = null, occasionItem: OccasionItem?): AddEventFragment {
            val fragment = AddEventFragment()
            fragment.preSelectedDate = preSelectedDate
            fragment.existingDataItem = occasionItem
            return fragment
        }
    }
}