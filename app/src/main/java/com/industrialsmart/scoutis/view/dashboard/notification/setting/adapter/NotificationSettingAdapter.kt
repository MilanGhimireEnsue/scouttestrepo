package com.industrialsmart.scoutis.view.dashboard.notification.setting.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.listener.EditDeleteListener
import com.industrialsmart.scoutis.models.NotificationSettingItem
import kotlinx.android.synthetic.main.item_notification_setting.view.*

class NotificationSettingAdapter(private val settingList: List<NotificationSettingItem?>, private val gduClickListener: EditDeleteListener?) :
    RecyclerSwipeAdapter<NotificationSettingAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notification_setting, parent, false)
        return MyViewHolder(view, mItemManger, parent.context, gduClickListener)
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipeLayout
    }

    override fun getItemCount(): Int {
        return settingList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(settingList[position])
        mItemManger.bindView(holder.itemView, position)
    }

    class MyViewHolder(
        view: View,
        val mItemManger: SwipeItemRecyclerMangerImpl,
        private val mContext: Context,
        private val gduClickListener: EditDeleteListener?
    ) : RecyclerView.ViewHolder(view) {
        fun bind(item: NotificationSettingItem?) {
            itemView.settingTitle.text = "${item?.readingTypeItem?.name} - (${item?.sensorItem?.title})"
            itemView.settingInfo.text = "${item?.scoutItem?.title} (${item?.scoutItem?.code})"
            itemView.settingDesc.text = item?.watchText
            when (item?.severity) {
                0 -> {
                    itemView.severityLabel.text = mContext.getString(R.string.severity_0)
                    itemView.severityLabel.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_0))
                }
                1 -> {
                    itemView.severityLabel.text = mContext.getString(R.string.severity_1)
                    itemView.severityLabel.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_1))
                }
                2 -> {
                    itemView.severityLabel.text = mContext.getString(R.string.severity_2)
                    itemView.severityLabel.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_2))
                }
                3 -> {
                    itemView.severityLabel.text = mContext.getString(R.string.severity_3)
                    itemView.severityLabel.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_3))
                }
                4 -> {
                    itemView.severityLabel.text = mContext.getString(R.string.severity_4)
                    itemView.severityLabel.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_4))
                }
            }
            itemView.dragItem.setOnClickListener {
                gduClickListener?.onClicked(item)
            }
            itemView.editButton.setOnClickListener {
                itemView.swipeLayout.close()
                gduClickListener?.onEdit(item)
            }
            itemView.deleteButton.setOnClickListener {
                itemView.swipeLayout.close()
                gduClickListener?.onDelete(item)
            }
            itemView.swipeLayout.addSwipeListener(object : SimpleSwipeListener() {
                override fun onStartOpen(layout: SwipeLayout) {
                    mItemManger.closeAllExcept(layout)
                }
            })
        }
    }
}