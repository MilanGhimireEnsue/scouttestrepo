package com.industrialsmart.scoutis.view.dashboard.home

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.util.Base64
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.listener.ScoutClickListener
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.models.Sensor
import com.industrialsmart.scoutis.models.UserInfo
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.*
import com.industrialsmart.scoutis.utils.successToast
import com.industrialsmart.scoutis.view.dashboard.DashBoardActivity
import com.industrialsmart.scoutis.view.dashboard.home.adapter.ScoutAdapter
import com.industrialsmart.scoutis.view.scout.ScoutActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*
import org.jetbrains.anko.startActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class HomeFragment : BaseFragment() {
    override val layoutId: Int get() = R.layout.fragment_home

    private var scoutList: List<ScoutModel?>? = null
    private var isSearchEditTextVisible = false

    //for lifecycle
    private var isVisibleToUser: Boolean = false
    private var isStarted = false

    private fun initViewForLifeCycle() {
        mDisposable = CompositeDisposable()
        navIcon.setOnClickListener {
            (activity as DashBoardActivity).toggleDrawerState()
        }
        handleScoutSearch()
        scoutList?.let { populateListView(it.applyVisibilitySetting()) }
        pullToRefresh.setOnRefreshListener {
            pullToRefresh.isRefreshing = false
            requestScoutList()
        }
    }

    private fun handleScoutSearch() {
        searchIcon.setOnClickListener {
            if (isSearchEditTextVisible)
                scoutSearchContainer.hideWithAnimateSlideUp()
            else
                scoutSearchContainer.showWithAnimateSlideDown()
            isSearchEditTextVisible = !isSearchEditTextVisible
            scoutSearchEditText.textValue = ""
        }
        scoutSearchEditText.afterTextChanged {
            if (it.isNotEmpty()) {
                clearIcon.show()
                val filteredList =
                    scoutList?.filter { scoutModel ->
                        ((scoutModel?.title?.contains(it, true) == true) || (scoutModel?.code?.contains(
                            it,
                            true
                        ) == true))
                    }
                filteredList?.let { populateListView(it.applyVisibilitySetting()) }
            } else {
                clearIcon.hide()
                scoutList?.let { populateListView(it.applyVisibilitySetting()) }
            }
        }
        clearIcon.setOnClickListener {
            scoutSearchEditText.textValue = ""
        }
    }

    fun getHashkey() {
        try {
            val info: PackageInfo? = context?.getPackageManager()?.getPackageInfo(
                context?.applicationContext?.getPackageName(),
                PackageManager.GET_SIGNATURES
            )
            for (signature in info!!.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.i("Base64", Base64.encodeToString(md.digest(), Base64.NO_WRAP))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Log.d("Name not found", e.message, e)
        } catch (e: NoSuchAlgorithmException) {
            Log.d("Error", e.message, e)
        }
    }


    override fun onResume() {
        super.onResume()
        d("okhttp fcm token ${UserInfo.fcmToken}")
        //updating the data in recycler view with out loosing it's previous state
        val recyclerViewState = scoutRV.layoutManager?.onSaveInstanceState()
        scoutList?.let { populateListView(it.applyVisibilitySetting()) }
        scoutRV.layoutManager?.onRestoreInstanceState(recyclerViewState)
    }

    private fun populateListView(it: List<ScoutModel?>) {
        scoutRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = ScoutAdapter(it, object : ScoutClickListener {
                override fun onScoutClicked(scoutModel: ScoutModel?) {
                    context?.startActivity<ScoutActivity>(
                        ScoutActivity.EXTRA_SCOUT_DATA to scoutModel,
                        ScoutActivity.EXTRA_SCOUT_CHART_TYPE to ScoutActivity.Companion.SCOUT_CHART_TYPE.SCOUT_CHART
                    )
                }

                override fun onScoutSettingClicked(scoutModel: ScoutModel?) {
                    context?.startActivity<ToolbarNavActivity>(
                        ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.SCOUT_SETTING,
                        ToolbarNavActivity.EXTRA_SCOUT_DATA to scoutModel
                    )
                }

                override fun onScoutNotificationClicked(scoutModel: ScoutModel?) {
                    context?.startActivity<ToolbarNavActivity>(
                        ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.NOTIFICATION,
                        ToolbarNavActivity.EXTRA_SCOUT_DATA to scoutModel
                    )
                }

                override fun onAddScoutNotificationClicked(scoutModel: ScoutModel?) {
                    context?.startActivity<ToolbarNavActivity>(
                        ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.ADD_NOTIFICATION_SETTING,
                        ToolbarNavActivity.EXTRA_SCOUT_DATA to scoutModel
                    )
                }

                override fun onSensorClicked(scoutModel: ScoutModel?, sensor: Sensor?) {
                    context?.startActivity<ScoutActivity>(
                        ScoutActivity.EXTRA_SCOUT_DATA to scoutModel,
                        ScoutActivity.EXTRA_SCOUT_CHART_TYPE to ScoutActivity.Companion.SCOUT_CHART_TYPE.SCOUT_CHART,
                        ScoutActivity.EXTRA_SENSOR_DATA to sensor
                    )
                }

            })
        }
        scoutRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val pos = (scoutRV?.layoutManager as LinearLayoutManager?)?.findFirstCompletelyVisibleItemPosition()
                pullToRefresh.isEnabled = pos == 0
            }
        })
    }

    private fun requestScoutList() {
        showProgressDialog()
        dataSource.clearAllData()
        mDisposable?.add(
            dataSource.getScoutsList().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    dismissProgressDialog()
                    (activity as DashBoardActivity).setScoutList(it)
                    scoutList = it
                    scoutList?.let { populateListView(it.applyVisibilitySetting()) }
                }, {
                    dismissProgressDialog()
                })
        )
    }

    override fun onStart() {
        super.onStart()
        isStarted = true
        if (isVisibleToUser && isStarted) {
            //api call here
            initViewForLifeCycle()
        } else
            disposeRequestHandler()
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        isVisibleToUser = visible
        if (visible && isStarted) {
            //api call here
            initViewForLifeCycle()
        } else
            disposeRequestHandler()
    }

    private fun disposeRequestHandler() {
        if (activity is DashBoardActivity)
            mDisposable?.dispose()
    }

    override fun onStop() {
        super.onStop()
        isStarted = false
    }

    companion object {
        fun newInstance(scoutList: List<ScoutModel?>?): HomeFragment {
            val fragment = HomeFragment()
            fragment.scoutList = scoutList
            return fragment
        }
    }
}