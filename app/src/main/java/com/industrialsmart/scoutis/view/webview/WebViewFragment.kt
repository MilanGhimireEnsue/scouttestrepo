package com.industrialsmart.scoutis.view.webview

import android.content.Context
import android.os.Build
import android.view.View
import android.webkit.*
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import kotlinx.android.synthetic.main.fragment_web_view.*

class WebViewFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_web_view

    private var urlPath: String? = null
    private var pageTitle: String? = null

    override fun initView() {
        super.initView()
        (activity as ToolbarNavActivity).setToolbarTitle(pageTitle)
        initWebView(webView)
        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                progressBar?.progress = newProgress
                if (newProgress == 100)
                    progressBar?.hide()
                else
                    progressBar?.show()
            }
        }
        webView.loadUrl(urlPath)
    }

    inner class WebViewJavascriptInterface(private val context: Context?) {
        @JavascriptInterface
        fun receiveMessageFromJS(message: String) {
            //Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }
    }

    private fun initWebView(webView: WebView) {


        val settings = webView.settings
        settings.apply {
            javaScriptEnabled = true
            domStorageEnabled = true
            loadWithOverviewMode = true
            useWideViewPort = true
            builtInZoomControls = false
            allowContentAccess = true
            javaScriptCanOpenWindowsAutomatically = true
            loadsImagesAutomatically = true
            setSupportMultipleWindows(true)
            setSupportZoom(true)
            domStorageEnabled = true
        }

        webView.addJavascriptInterface(WebViewJavascriptInterface(context), "myOwnJSHandler")

        //        webView.setInitialScale(ZOOM_LEVEL);
        // Allow third party cookies for Android Lollipop
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val cookieManager = CookieManager.getInstance()
            cookieManager.setAcceptThirdPartyCookies(webView, true)
        } else
            CookieManager.getInstance().setAcceptCookie(true)

        settings.setRenderPriority(WebSettings.RenderPriority.HIGH)
        settings.cacheMode = WebSettings.LOAD_NO_CACHE

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }
        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                return false
            }
        })

    }


    companion object {
        fun newInstance(urlPath: String?, pageTitle: String? = ""): WebViewFragment {
            val fragment = WebViewFragment()
            fragment.urlPath = urlPath
            fragment.pageTitle = pageTitle
            return fragment
        }
    }
}