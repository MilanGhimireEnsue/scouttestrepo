package com.industrialsmart.scoutis.view.dashboard.notification.setting

import android.widget.TextView
import androidx.core.content.ContextCompat
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.errorToast
import com.industrialsmart.scoutis.utils.extensions.*
import com.industrialsmart.scoutis.utils.successToast
import com.industrialsmart.scoutis.utils.warningToast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_add_notification_setting.*
import kotlinx.android.synthetic.main.layout_marker.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class AddNotificationSettingFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_add_notification_setting

    private var readingTypeList: List<ReadingTypeItem?>? = null
    private var scoutList: List<ScoutModel?>? = null
    private var existingNotificationSettingItem: NotificationSettingItem? = null
    private var scoutItemFromDashboard: ScoutModel? = null

    private var selectedScout: ScoutModel? = null
    private var selectedSensor: Sensor? = null
    private var selectedReadingType: SensorReading? = null
    private var selectedAlertLevel: Int? = null

    override fun initView() {
        super.initView()
        if (existingNotificationSettingItem == null) {
            (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.add_notification_settings))
            addButton.text = "ADD"

        } else {
            (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.edit_notification_settings))
            addButton.text = "DONE"
        }
        if (scoutList == null || readingTypeList == null) {
            requestScoutList()
            requestReadingType()
        } else
            setUpScoutList()
        setUpAlertLevel()
        addButton.setOnClickListener {
            validateData()
        }
        setUpExistingData()
    }

    private fun setUpExistingData() {
        if (existingNotificationSettingItem != null) {
            messageEditText.textValue = existingNotificationSettingItem?.watchText.toString()
        }
    }

    var isScoutListFetched = false
    private fun requestScoutList() {
        mDisposable?.add(
            dataSource.getScoutsList().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    scoutList = it
                    isScoutListFetched = true
                    handleScoutAndReadingResponse()
                }, {
                })
        )
    }

    var isReadingTypeFetched = false
    private fun requestReadingType() {
        mDisposable?.add(
            dataSource.getReadingTypeList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    readingTypeList = it
                    isReadingTypeFetched = true
                    handleScoutAndReadingResponse()
                }, {
                })
        )
    }


    private fun handleScoutAndReadingResponse() {
        if (isReadingTypeFetched && isScoutListFetched) {
            setUpScoutList()
        }
    }

    private fun setUpAlertLevel() {
        val alertLevelList = listOf("Default", "Info", "Warning", "Error", "Critical")
        alertLevelSpinner.setHintSpinnerAdapter(alertLevelList, "Required")
        alertLevelSpinner.onItemSelected { view, position, l ->
            position?.let {
                selectedAlertLevel = it - 1
                when (position - 1) {
                    0 -> {
                        (view as TextView).setTextColor(context!!.getColorCompat(R.color.color_severity_0))
                    }
                    1 -> {
                        (view as TextView).setTextColor(context!!.getColorCompat(R.color.color_severity_1))
                    }
                    2 -> {
                        (view as TextView).setTextColor(context!!.getColorCompat(R.color.color_severity_2))
                    }
                    3 -> {
                        (view as TextView).setTextColor(context!!.getColorCompat(R.color.color_severity_3))
                    }
                    4 -> {
                        (view as TextView).setTextColor(context!!.getColorCompat(R.color.color_severity_4))
                    }
                }
            }
        }
        alertLevelSpinner.setSelection(1)
        if (existingNotificationSettingItem?.severity != null)
            alertLevelSpinner.setSelection(existingNotificationSettingItem?.severity!! + 1)


    }

    private fun setUpScoutList() {
        //disable other spinner
        readingSpinner.setHintSpinnerAdapter(emptyList(), "Required")
        sensorSpinner.setHintSpinnerAdapter(emptyList(), "Required")
        val textList = scoutList?.map { it?.title!! }
        scoutSpinner.setHintSpinnerAdapter(textList, "Required")
        scoutSpinner.onItemSelected { view, position, l ->
            selectedScout = position?.let { pos -> scoutList?.get(pos - 1) }
            setUpSensorList()
        }
        if (existingNotificationSettingItem != null && !existingNotificationSettingItem?.dataProviderId.isNullOrEmpty()) {
            val allSensorList: MutableList<Sensor?> = ArrayList()
            scoutList?.forEach {
                it?.sensors?.let { it1 -> allSensorList.addAll(it1) }
            }
            existingNotificationSettingItem?.sensorItem = allSensorList.find { it?.id == existingNotificationSettingItem?.dataProviderId }
            val scoutId = existingNotificationSettingItem?.sensorItem?.scoutId
            existingNotificationSettingItem?.scoutItem = scoutList?.find { it?.id == scoutId }
            val selectedScoutPos = scoutList?.indexOf(scoutList?.find { it?.id == scoutId })
            if (selectedScoutPos != null) {
                scoutSpinner.setSelection(selectedScoutPos + 1)
                scoutSpinner.isEnabled = false
            }
        }
        //for handling scout click from dashboard
        if (scoutItemFromDashboard != null) {
            val selectedScoutPos = scoutList?.indexOf(scoutList?.find { it?.id == scoutItemFromDashboard?.id })
            if (selectedScoutPos != null) {
                scoutSpinner.setSelection(selectedScoutPos + 1)
                scoutSpinner.isEnabled = false
            }
        }
    }

    private fun setUpSensorList() {
        //disable other spinner
        readingSpinner.setHintSpinnerAdapter(emptyList(), "Required")
        //reset selected
        selectedSensor = null
        selectedReadingType = null
        val textList = selectedScout?.sensors?.map { it.title }
        sensorSpinner.setHintSpinnerAdapter(textList, "Required")
        sensorSpinner.onItemSelected { view, position, l ->
            selectedSensor = position?.let { pos -> selectedScout?.sensors?.get(pos - 1) }
            setUpReadingType()
        }
        if (existingNotificationSettingItem != null && !existingNotificationSettingItem?.dataProviderId.isNullOrEmpty()) {
            val selectedSensorPosition =
                selectedScout?.sensors?.indexOf(selectedScout?.sensors?.find { it.id == existingNotificationSettingItem?.dataProviderId })
            if (selectedSensorPosition != null) {
                sensorSpinner?.setSelection(selectedSensorPosition + 1)
                sensorSpinner.isEnabled = false
            }
        }
    }

    private fun setUpReadingType() {
        //reset selected
        selectedReadingType = null
        val textList = selectedSensor?.readings?.map { it.name } as List<String>?
        readingSpinner.setHintSpinnerAdapter(textList, "Required")
        readingSpinner.onItemSelected { view, position, l ->
            selectedReadingType = position?.let { pos -> selectedSensor?.readings?.get(pos - 1) }
            minValueUnit.text = selectedReadingType?.unit?.addDegreeIfTempUnit()
            maxValueUnit.text = selectedReadingType?.unit?.addDegreeIfTempUnit()
        }
        if (existingNotificationSettingItem != null && existingNotificationSettingItem?.readingTypeId != null) {
            val readingTypeItem = getReadingTypeFromId(existingNotificationSettingItem?.readingTypeId)
            val selectedReading = selectedSensor?.readings?.find { it.code == readingTypeItem?.code }
            val selectedReadingTypeIndex = selectedSensor?.readings?.indexOf(selectedReading)
            if (selectedReadingTypeIndex != null) {
                readingSpinner.setSelection(selectedReadingTypeIndex + 1)
                readingSpinner.isEnabled = false
            }
            //setting min and max according to user pref
            var minValue = existingNotificationSettingItem?.watchMin
            var maxValue = existingNotificationSettingItem?.watchMax
            if (selectedReading?.unit == TemperatureUnit.FAHRENHEIT.unitCode) {
                minValue = convertTemperature(minValue, TemperatureUnit.FAHRENHEIT)
                maxValue = convertTemperature(maxValue, TemperatureUnit.FAHRENHEIT)
            }
            if (selectedReading?.unit == SoilECUnit.DSM.unitCode) {
                minValue = convertSoilEC(minValue, SoilECUnit.DSM)
                maxValue = convertSoilEC(maxValue, SoilECUnit.DSM)
            }
            minValueEditText.textValue = minValue.toString()
            maxValueEditText.textValue = maxValue.toString()
        }
    }

    private fun validateData() {
        if (selectedScout == null) {
            context?.warningToast("Please select scout.")
            return
        }
        if (selectedSensor == null) {
            context?.warningToast("Please selected sensor.")
            return
        }
        if (selectedReadingType == null) {
            context?.warningToast("Please select reading type.")
            return
        }
        if (selectedAlertLevel == null) {
            context?.warningToast("Please select alert level.")
            return
        }
        if (minValueEditText.textValue.isEmpty()) {
            context?.warningToast("Please enter min value.")
            return
        }
        if (maxValueEditText.textValue.isEmpty()) {
            context?.warningToast("Please enter max value.")
            return
        }
        if (messageEditText.textValue.isEmpty()) {
            context?.warningToast("Please enter message.")
            return
        }
        if (minValueEditText.textValue.toDouble() > maxValueEditText.textValue.toDouble()) {
            context?.warningToast("Minimum value has to be less than maximum value.")
            return
        }
        if (minValueEditText.textValue.toDouble() == maxValueEditText.textValue.toDouble()) {
            context?.warningToast("Minimum value cannot be same as maximum value.")
            return
        }
        requestAddNotificationSetting()
    }

    private fun requestAddNotificationSetting() {
        showProgressDialog()
        var minValue = minValueEditText.textValue.toDoubleOrNull()
        var maxValue = maxValueEditText.textValue.toDoubleOrNull()
        //check if the temperature unit needs to be converted, then make the conversion
        if (selectedReadingType?.unit == TemperatureUnit.FAHRENHEIT.unitCode) {
            minValue = convertTemperature(minValue, TemperatureUnit.CELSIUS)
            maxValue = convertTemperature(maxValue, TemperatureUnit.CELSIUS)
        }
        //check if the soilEC unit needs to be converted, then make the conversion
        if (selectedReadingType?.unit == SoilECUnit.DSM.unitCode) {
            minValue = convertSoilEC(minValue, SoilECUnit.USCM)
            maxValue = convertSoilEC(maxValue, SoilECUnit.USCM)
        }
        var postData: NotificationSettingItem? = null
        if (existingNotificationSettingItem == null) {
            postData = NotificationSettingItem()
            postData.apply {
                watchText = messageEditText.textValue
                watchMin = minValue
                watchMax = maxValue
                severity = selectedAlertLevel
                targetId = selectedSensor?.id
                dataProviderId = selectedSensor?.id
                targetTypeCode = "Sensor" //Todo : to confirm with IOS why only accepting Sensor
                dataProviderTypeCode = "Sensor"
                readingTypeId = getReadingTypeFromCode(selectedReadingType?.code)?.readingTypeId
            }
        } else {
            postData = existingNotificationSettingItem?.apply {
                watchText = messageEditText.textValue
                watchMin = minValue
                watchMax = maxValue
                severity = selectedAlertLevel
            }
        }
        mDisposable?.add(
            dataSource.requestAddNotificationSetting(postData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it == null)
                        context?.errorToast("Error occurred. Please try again.")
                    else {
                        context?.successToast("Notification setting saved successfully.")
                        requireActivity().onBackPressed()
                    }
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun getReadingTypeFromCode(readingCode: String?): ReadingTypeItem? {
        return readingTypeList?.find { it?.code == readingCode }
    }

    private fun getReadingTypeFromId(readingTypeId: String?): ReadingTypeItem? {
        return readingTypeList?.find { it?.readingTypeId == readingTypeId }
    }

    companion object {
        //For add
        fun newInstance(
            scoutListResponse: List<ScoutModel?>?, readingTypeList: List<ReadingTypeItem?>?
        ): AddNotificationSettingFragment {
            val fragment = AddNotificationSettingFragment()
            fragment.scoutList = scoutListResponse
            fragment.readingTypeList = readingTypeList
            return fragment
        }

        //For editing
        fun newInstance(
            scoutListResponse: List<ScoutModel?>,
            readingTypeList: List<ReadingTypeItem?>,
            notificationSettingItem: NotificationSettingItem?
        ): AddNotificationSettingFragment {
            val fragment = AddNotificationSettingFragment()
            fragment.scoutList = scoutListResponse
            fragment.readingTypeList = readingTypeList
            fragment.existingNotificationSettingItem = notificationSettingItem
            return fragment
        }

        //For add from dashboard
        fun newInstance(
            scoutItem: ScoutModel?
        ): AddNotificationSettingFragment {
            val fragment = AddNotificationSettingFragment()
            fragment.scoutItemFromDashboard = scoutItem
            return fragment
        }
    }
}