package com.industrialsmart.scoutis.view.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.industrialsmart.scoutis.R
import kotlinx.android.synthetic.main.fragment_info_dialog.*


class InfoDialogFragment : DialogFragment() {

    private var title: String? = null
    private var infoText: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_info_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        titleTextView.text = title
        infoTextView.text = Html.fromHtml(infoText)
        closeIcon.setOnClickListener {
            dismiss()
        }
    }

    companion object {
        fun newInstance(title: String?, infoText: String?): InfoDialogFragment {
            val fragment = InfoDialogFragment()
            fragment.title = title
            fragment.infoText = infoText
            return fragment
        }
    }
}