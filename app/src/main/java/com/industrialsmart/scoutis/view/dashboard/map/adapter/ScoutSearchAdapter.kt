package com.industrialsmart.scoutis.view.dashboard.map.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ScoutModel
import kotlinx.android.synthetic.main.item_scout_search.view.*

class ScoutSearchAdapter(private var items: List<ScoutModel?>, val itemClicked: (ScoutModel?) -> Unit) :
    RecyclerView.Adapter<ScoutSearchAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_scout_search, parent, false)
        return MyViewHolder(view, itemClicked)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])
    }

    class MyViewHolder(view: View, private val itemClicked: (ScoutModel?) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bind(scoutItem: ScoutModel?) {
            itemView.scoutTitle.text = scoutItem?.title
            itemView.scoutIdentifier.text = scoutItem?.code
            itemView.scoutDateTime.text = scoutItem?.dateTime
            itemView.setOnClickListener {
                itemClicked.invoke(scoutItem)
            }
        }
    }
}