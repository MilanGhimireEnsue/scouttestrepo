package com.industrialsmart.scoutis.view.dashboard

import android.graphics.Typeface
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.viewpager.widget.ViewPager
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.badgeable.secondaryItem
import co.zsmb.materialdrawerkt.draweritems.divider
import co.zsmb.materialdrawerkt.draweritems.expandable.expandableItem
import com.industrialsmart.scoutis.MyApplication
import com.industrialsmart.scoutis.view.dashboard.map.ISMapsFragment
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseActivity
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.service.FCMIdRegistration
import com.industrialsmart.scoutis.service.getConfigRawFile
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.getDeviceUniqueId
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.utils.extensions.updateUserSetting
import com.industrialsmart.scoutis.view.dashboard.adapter.ViewPagerAdapter
import com.industrialsmart.scoutis.view.dashboard.calendar.CalendarFragment
import com.industrialsmart.scoutis.view.dashboard.calendar.CalendarParentFragment
import com.industrialsmart.scoutis.view.dashboard.home.HomeFragment
import com.industrialsmart.scoutis.view.dashboard.notification.NotificationFragment
import com.microsoft.identity.client.IMultipleAccountPublicClientApplication
import com.microsoft.identity.client.IPublicClientApplication
import com.microsoft.identity.client.PublicClientApplication
import com.microsoft.identity.client.exception.MsalException
import com.mikepenz.materialdrawer.AccountHeader
import com.mikepenz.materialdrawer.Drawer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.custom_tab_layout.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.uiThread

class DashBoardActivity : BaseActivity() {
    override val layoutId: Int get() = R.layout.activity_dashboard

    private var scoutListResponse: List<ScoutModel?>? = null
    private var occasionList: List<OccasionItem?>? = null

    private var result: Drawer? = null
    private var headerResult: AccountHeader? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpDrawer(savedInstanceState)
    }

    override fun initView() {
        super.initView()
        checkForLoginAccount()
    }

    private fun startDataFetch() {
        getUserData()
    }

    /**
     * Setup FCM token if not set already
     */
    private fun setUpFCMToken() {
        if (!UserInfo.fcmTokenSentStatus && UserInfo.fcmToken.isNotEmpty()) {
            val postData = FCMTokenPostData(code = getDeviceUniqueId(this), key = UserInfo.fcmToken)
            FCMIdRegistration.requestPostFCMToken(dataSource, postData)
        }
    }

    private fun requestScoutList() {
        mDisposable?.add(
            dataSource.getScoutsList().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    dismissProgressDialog()
                    scoutListResponse = it
                    setUpBottomBarTab(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    /**
     * Get notification count
     */
    var unreadNotificationCount: Int? = null
    private fun requestNotificationCount() {
        mDisposable?.add(
            dataSource.getUnreadNotificationCount()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    unreadNotificationCount = it
                }, {
                })
        )
    }

    fun getScoutList(): List<ScoutModel?>? {
        return scoutListResponse
    }

    fun setScoutList(scoutList: List<ScoutModel?>) {
        scoutListResponse = scoutList
    }

    private fun getUserData() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    handleUserDataResponse(it)
                    requestOtherData()
                }, {
                })
        )
    }

    private fun requestOtherData() {
        requestScoutList()
        requestNotificationCount()
        setUpFCMToken()
    }

    private fun handleUserDataResponse(userDataResponse: UserDataResponse?) {
        UserInfo.userData = userDataResponse
        updateUserSetting()
    }

    fun setOccasionList(occasionList: List<OccasionItem?>) {
        this.occasionList = occasionList
    }

    fun getOccasionList(): List<OccasionItem?>? {
        return occasionList
    }

    private fun setUpBottomBarTab(it: List<ScoutModel?>?) {
        val pagerAdapter = ViewPagerAdapter(supportFragmentManager, context)
        pagerAdapter.addAllFragment(
            listOf(
                HomeFragment.newInstance(it),
                ISMapsFragment.newInstance(),
                CalendarParentFragment.newInstance(),
                NotificationFragment.newInstance()
            )
        ) //to get fragment to show in bottom bar
        viewPager.adapter = pagerAdapter
        viewPager.addOnPageChangeListener(onPageChangeListener)
        slidingTabs.setupWithViewPager(viewPager)
        val length = slidingTabs.tabCount
        for (i in 0 until length)
            slidingTabs.getTabAt(i)?.customView = pagerAdapter.getTabView(i)
        //select particular tabItem, which is passed as tabItemToSelect
        selectedTab(0)
        setNotificationBadgeCount(unreadNotificationCount)
    }

    private fun selectedTab(position: Int) {
        slidingTabs.getTabAt(position)?.apply {
            select()
            customView?.apply {
                tabNameTextView.setTextColor(
                    ContextCompat.getColor(
                        baseContext,
                        R.color.colorPrimary
                    )
                )
                tabImageView.setImageResource(ViewPagerAdapter.getActiveTabBottomImg(position)!!)
            }
        }
    }

    fun setNotificationBadgeCount(count: Int?) {
        if (slidingTabs == null)
            return
        if (count == null || count == 0) {
            slidingTabs.getTabAt(3)?.apply {
                customView?.badgeCountTextView?.hide()
            }
            return
        }
        slidingTabs.getTabAt(3)?.apply {
            customView?.badgeCountTextView?.show()
            if (count > 99)
                customView?.badgeCountTextView?.text = "99+"
            else
                customView?.badgeCountTextView?.text = count.toString()
        }
    }

    /**
     *  viewpager change listener
     */
    private val onPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            //adding shadow in selected text view
            for (i in 0 until slidingTabs.tabCount) {
                val mTabNameTextView =
                    slidingTabs.getTabAt(i)?.customView!!.tabNameTextView as TextView
                val mTabImageView = slidingTabs.getTabAt(i)?.customView!!.tabImageView as ImageView
                if (i == position) {
                    mTabNameTextView.setTextColor(
                        ContextCompat.getColor(
                            baseContext,
                            R.color.colorPrimary
                        )
                    )
                    mTabImageView.setImageResource(ViewPagerAdapter.getActiveTabBottomImg(i)!!)

                } else {
                    mTabNameTextView.setTextColor(
                        ContextCompat.getColor(
                            baseContext,
                            R.color.colorForegroundDark
                        )
                    )
                    mTabImageView.setImageResource(ViewPagerAdapter.getInactiveTabBottomImg(i)!!)
                }
            }

            if (position == 0)
                result?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            else
                result?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }

        override fun onPageScrollStateChanged(state: Int) {

        }
    }


    fun toggleDrawerState() {
        if (result != null) {
            if (result!!.isDrawerOpen)
                result?.closeDrawer()
            else
                result?.openDrawer()
        }
    }

    private fun setUpDrawer(savedInstanceState: Bundle?) {
        val mTypeface = Typeface.createFromAsset(assets, getString(R.string.fontRegular))
        result = drawer {
            hasStableIds = true
            savedInstance = savedInstanceState
            showOnFirstLaunch = false
            closeOnClick = true
            selectedItemByPosition = -1
            headerViewRes = R.layout.layout_nav_header_view

            expandableItem("Derived Data") {
                arrowRotationAngleStart = 270
                icon = R.drawable.ic_gdu_settings
                typeface = mTypeface
                selectable = false
                textColorRes = R.color.colorForegroundDark
                divider {}
                secondaryItem("GDU") {
                    icon = R.drawable.ic_gdu_settings
                    textColorRes = R.color.colorForegroundDark
                    typeface = mTypeface
                    selectable = false
                    onClick { _ ->
                        result?.closeDrawer()
                        startActivity<ToolbarNavActivity>(ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.GDU_LISTING)
                        true
                    }
                }
            }
            divider {}
            primaryItem("App References") {
                textColorRes = R.color.colorForegroundDark
                typeface = mTypeface
                icon = R.drawable.ic_references
                selectable = false
                onClick { _ ->
                    result?.closeDrawer()
                    startActivity<ToolbarNavActivity>(ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.REFERENCE)
                    true
                }
            }
            divider {}
            primaryItem("User Settings") {
                textColorRes = R.color.colorForegroundDark
                selectable = false
                typeface = mTypeface
                icon = R.drawable.icons_account
                onClick { _ ->
                    result?.closeDrawer()
                    startActivity<ToolbarNavActivity>(ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.USER_SETTING)
                    true
                }
            }
            divider {}
            primaryItem("App Info") {
                textColorRes = R.color.colorForegroundDark
                typeface = mTypeface
                icon = R.drawable.ic_about_inactive
                onClick { _ ->
                    result?.closeDrawer()
                    startActivity<ToolbarNavActivity>(ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.APP_INFO)
                    true
                }
            }
            divider {}
        }
        result?.drawerLayout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        result?.saveInstanceState(outState)
        headerResult?.saveInstanceState(outState)
    }

    private fun openMapWithScoutInfo(scoutData: ScoutModel?) {
        selectedTab(1)
        val mapFragment = ((viewPager.adapter as ViewPagerAdapter?)?.getItem(1) as ISMapsFragment?)
        mapFragment?.openScoutInfo(scoutData)
    }

    override fun onBackPressed() {
        if (result?.isDrawerOpen == true)
            result?.closeDrawer()
        else
            super.onBackPressed()
    }


    private fun checkForLoginAccount() {
        if ((application as MyApplication).getLoginAccount() == null) {
            PublicClientApplication.createMultipleAccountPublicClientApplication(context,
                getConfigRawFile(),
                object : IPublicClientApplication.IMultipleAccountApplicationCreatedListener {
                    override fun onCreated(application: IMultipleAccountPublicClientApplication) {
                        fetchLoggedInAccount(application)
                    }

                    override fun onError(exception: MsalException) {
                        //Log Exception Here
                        d("b2c Failed creation ${exception.message}")
                        d("b2c Failed creation ${exception.stackTrace}")
                    }
                })
        } else
            startDataFetch()
    }

    private fun fetchLoggedInAccount(application: IMultipleAccountPublicClientApplication) {
        doAsync {
            val account = application.getAccount(UserInfo.loginIAccout)
            uiThread {
                (applicationContext as MyApplication).setLoginAccount(account, application)
                startDataFetch() //start data fetch only after updating application info
            }
        }

    }

}