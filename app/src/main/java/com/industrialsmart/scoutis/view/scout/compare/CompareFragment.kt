package com.industrialsmart.scoutis.view.scout.compare

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.listener.CompareItemListener
import com.industrialsmart.scoutis.models.CompareSensorData
import com.industrialsmart.scoutis.models.ToBeCompared
import com.industrialsmart.scoutis.utils.Constants
import com.industrialsmart.scoutis.utils.ScreenUtils
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.view.scout.ScoutActivity
import com.industrialsmart.scoutis.view.scout.compare.adapter.CompareAdapter
import kotlinx.android.synthetic.main.fragment_compare.*
import org.jetbrains.anko.toast


class CompareFragment : BottomSheetDialogFragment() {
    private var compareListener: CompareItemListener? = null
    private var compareSensorDataList: List<CompareSensorData?>? = null

    private var toBeComparedList: MutableList<ToBeCompared?> = ArrayList()

    //to pre set the checked sensor item of currently compared
    private var alreadyCheckedList: List<ToBeCompared?>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_compare, container, false)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        view?.postDelayed(
            {
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<View>(bottomSheet)
                val params = bottomSheet.layoutParams
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    params.height = ((ScreenUtils(context!!).height) / 100) * 100 //take about 100 percentage of screen height
                } else {
                    params.height = ((ScreenUtils(context!!).height) / 100) * 70 //take about 70 percentage of screen height
                }
                bottomSheet.layoutParams = params
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }, 200
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //for bottom sheet to take entire width so that date slider inside should not be impacted
        view.postDelayed(
            {
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<View>(bottomSheet)
                val params = bottomSheet.layoutParams
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    params.height = ((ScreenUtils(context!!).height) / 100) * 100 //take about 100 percentage of screen height
                } else {
                    params.height = ((ScreenUtils(context!!).height) / 100) * 70 //take about 70 percentage of screen height
                }
                bottomSheet.layoutParams = params
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }, 200
        )
        checkChangeForAlreadyCheckedData()
        setOnClickListener()
    }

    private fun checkChangeForAlreadyCheckedData() {
        if (compareSensorDataList?.isNotEmpty() == true) {
            //making all previously checked uncheck
            compareSensorDataList?.forEach { it?.comparableSensorList?.forEach { it?.comparableReadingType?.forEach { it?.isChecked = false } } }
            //making default checked item true
            alreadyCheckedList?.forEach { alreadyCheckedItem ->
                compareSensorDataList?.forEach {
                    it?.comparableSensorList?.find {
                        (it?.sensorId == alreadyCheckedItem?.sensorId) || (it?.sensorId == Constants.GDU_SENSOR_ID)
                    }?.comparableReadingType?.find {
                        it?.comparableReadingType?.readingTypeId == alreadyCheckedItem?.comparableReadingType?.readingTypeId
                    }?.isChecked = true
                }
            }
            setUpCompareRV()
        }
    }

    private fun setUpCompareRV() {
        compareRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = CompareAdapter(compareSensorDataList!!) { readingTypeItem, checked ->
                onCheckChange(readingTypeItem, checked)
            }
            isNestedScrollingEnabled = false
        }
    }

    private fun onCheckChange(readingTypeItem: ToBeCompared, checked: Boolean) {
        if (checked) {
            toBeComparedList.add(readingTypeItem) //temporary add for
            val distinctCategoryCode = toBeComparedList.distinctBy { it?.comparableReadingType?.categoryCode }.toMutableList()
            if (distinctCategoryCode.size > 2) {
                toBeComparedList.remove(readingTypeItem)
                CompareLimitationFragment.newInstance(distinctCategoryCode, readingTypeItem) { it ->
                    it.forEach { toBeCompared ->
                        val toBeRemovedItem =
                            toBeComparedList.filter { it?.comparableReadingType?.categoryCode == toBeCompared?.comparableReadingType?.categoryCode }
                        toBeComparedList.removeAll(toBeRemovedItem)
                    }
                    toBeComparedList.add(readingTypeItem)
                    alreadyCheckedList = ArrayList(toBeComparedList)
                    toBeComparedList.clear()
                    checkChangeForAlreadyCheckedData()
                }.show(fragmentManager!!, "TAG")
                alreadyCheckedList = ArrayList(toBeComparedList)
                toBeComparedList.clear()
                checkChangeForAlreadyCheckedData()
            }
        } else
            toBeComparedList.remove(readingTypeItem)
        changePositiveLabel()
    }

    /**
     * Change the label of positive button depending upon the length of comparable items
     */
    private fun changePositiveLabel() {
        if (toBeComparedList.size > 1) {
            //the size is greater than 1, so it should show compare
            doneButton.text = getString(R.string.compare_label)
        } else {
            //the size is less than one, so it should show done
            doneButton.text = getString(R.string.done_label)
        }
    }

    private fun setOnClickListener() {
        cancelButton.setOnClickListener {
            dialog?.dismiss()
        }
        doneButton.setOnClickListener {
            toBeComparedList = toBeComparedList.distinct().toMutableList()
            val distinctCategoryCode = toBeComparedList?.distinctBy { it?.comparableReadingType?.categoryCode }
            if (distinctCategoryCode.size > 2) {
                //more than 2 value type are selected, so should prompt to deselect any one
                context?.toast("Cannot compare more than 2 different unit types")
                return@setOnClickListener
            }
            dialog?.dismiss()
            compareListener?.onCompareItemClicked(toBeComparedList)
        }
    }

    companion object {
        fun newInstance(
            compareSensorDataList: List<CompareSensorData?>,
            alreadyCheckedList: List<ToBeCompared?>,
            compareListener: CompareItemListener?
        ): CompareFragment {
            val fragment = CompareFragment()
            fragment.compareSensorDataList = compareSensorDataList
            fragment.alreadyCheckedList = alreadyCheckedList
            fragment.compareListener = compareListener
            return fragment
        }
    }
}