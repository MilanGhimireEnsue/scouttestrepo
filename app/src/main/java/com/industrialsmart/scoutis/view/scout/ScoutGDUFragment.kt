package com.industrialsmart.scoutis.view.scout

import android.graphics.drawable.GradientDrawable
import android.widget.Button
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.listener.CompareItemListener
import com.industrialsmart.scoutis.listener.DateRangeListener
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.extensions.*
import com.industrialsmart.scoutis.utils.extensions.getColorCompat
import com.industrialsmart.scoutis.utils.view.XAxisValueFormatter
import com.industrialsmart.scoutis.utils.view.YourMarkerView
import com.industrialsmart.scoutis.view.calendar.DateRangePickerFragment
import com.industrialsmart.scoutis.view.scout.compare.CompareFragment
import com.industrialsmart.scoutis.view.scout.compare.adapter.ComparedItemAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_scout_compare.*
import kotlinx.android.synthetic.main.fragment_scout_compare.chartLoading
import kotlinx.android.synthetic.main.fragment_scout_compare.chartView
import kotlinx.android.synthetic.main.fragment_scout_compare.compareButton
import kotlinx.android.synthetic.main.fragment_scout_compare.customDateButtom
import kotlinx.android.synthetic.main.fragment_scout_compare.lastMonthButton
import kotlinx.android.synthetic.main.fragment_scout_compare.lastWeekButton


class ScoutGDUFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_scout_compare

    private var gduDataItem: GDUDataItem? = null

    private var toBeComparedList: MutableList<ToBeCompared>? = null

    //currently selected startDate and endDate
    private var selectedStartDate: String? = null
    private var selectedEndDate: String? = null

    //to persist all the readings for comparable sensors
    private var readingResponseMap: MutableMap<ToBeCompared?, List<ReadingDataItem?>> = HashMap()

    override fun initView() {
        super.initView()
        pageSubTitle.text = gduDataItem?.name
        toBeComparedList = mutableListOf(
            ToBeCompared(
                "Derived Data",
                null,
                ReadingTypeItem(name = gduDataItem?.name, categoryCode = "gdd", readingTypeId = "E10B3A95-5080-4C53-B148-43011AB33105"),
                foreGroundTint = R.color.reading_color_1
            )
        )
        setClickListener()
        setSensorData()
    }

    /**
     * Handle click for week/month/custom date
     */
    private fun setClickListener() {
        lastWeekButton.setOnClickListener {
            (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.WEEKLY)
            changeButtonStyle(lastWeekButton)
            requestSensorData(getLastWeekDate(), getCurrentDate())
            //make the start date and end date null
            selectedStartDate = null
            selectedEndDate = null
        }
        lastMonthButton.setOnClickListener {
            (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.MONTHLY)
            changeButtonStyle(lastMonthButton)
            requestSensorData(getLastMonthDate(), getCurrentDate())
            //make the start date and end date null
            selectedStartDate = null
            selectedEndDate = null
        }
        customDateButtom.setOnClickListener {
            val dateRangePickerFragment =
                DateRangePickerFragment.newInstance(object : DateRangeListener {
                    override fun onDateRangeSelected(startDate: String?, endDate: String?) {
                        (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.CUSTOM_RANGE)
                        (activity as ScoutActivity).setSelectedStartEndDate(startDate, endDate)
                        changeButtonStyle(customDateButtom)
                        requestSensorData(startDate, endDate)
                        //set the value of startDate and endDate
                        selectedStartDate = startDate
                        selectedEndDate = endDate
                    }
                }, selectedStartDate, selectedEndDate)
            dateRangePickerFragment.show(childFragmentManager, TAG_DATE_RANGE)
        }
        compareButton.setOnClickListener {
            val comparableSensorList = (activity as ScoutActivity).getComparableListOfSensor()
            if (!comparableSensorList.isNullOrEmpty()) {
                val alreadyCheckedList = listOf(
                    ToBeCompared(
                        "Derived Data 1",
                        gduDataItem?.id,
                        ReadingTypeItem(readingTypeId = "E10B3A95-5080-4C53-B148-43011AB33105", name = gduDataItem?.name, categoryCode = "gdd")
                    )
                )
                val compareFragment = CompareFragment.newInstance(comparableSensorList, alreadyCheckedList, object : CompareItemListener {
                    override fun onCompareItemClicked(toComparedList: List<ToBeCompared?>?) {
                        if (toComparedList?.isNullOrEmpty() == false) {
                            if (toComparedList.size > 1) {
                                //open scout compare screen, used to display sensor compare data
                                (activity as ScoutActivity).showContainer()
                                changeFragment(ScoutCompareFragment.newInstance(toComparedList.setRandomColors()))
                            } else {
                                //navigate to particular sensor and readingType as per user selection
                                (activity as ScoutActivity).switchSensorAndReading(toComparedList.first())
                            }
                        }
                    }
                })
                compareFragment.show(childFragmentManager, ScoutFragment.TAG_COMPARE_READING)
            }
        }
    }

    /**
     * Change button style for currently selected week/month/custom date
     */
    private fun changeButtonStyle(clickedButton: Button) {
        val viewList = mutableListOf(lastWeekButton, lastMonthButton, customDateButtom)
        viewList.remove(clickedButton)
        viewList.forEach {
            it?.apply {
                setTextColor(ContextCompat.getColor(context!!, R.color.colorForegroundDim))
                setBackgroundResource(android.R.color.transparent)
            }
        }
        clickedButton.apply {
            setTextColor(ContextCompat.getColor(context!!, R.color.foregroundColorLight))
            setBackgroundResource(R.drawable.bg_button_rounded)
        }
    }

    private fun setSensorData() {
        comparedReadingRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = toBeComparedList.let {
                ComparedItemAdapter(it!!) {
                    //remove icon clicked
                    toBeComparedList?.remove(it!!)
                    adapter?.notifyDataSetChanged()
                    removeItemFromChart(it)
                }
            }
        }
        setChartAccordingToChartDataType()
    }

    /**
     * Remove particualr item from comparable list and handle all the possible senerio
     */
    private fun removeItemFromChart(it: ToBeCompared?) {
        activity?.finish()
    }

    /**
     * Set chart selection according to selection type depending upon other sensors
     */
    private fun setChartAccordingToChartDataType() {
        when ((activity as ScoutActivity).getChartDataDate()) {
            ScoutActivity.Companion.CHART_DATA_DATE.WEEKLY -> {
                lastWeekButton.callOnClick()
            }
            ScoutActivity.Companion.CHART_DATA_DATE.MONTHLY -> {
                lastMonthButton.callOnClick()
            }
            ScoutActivity.Companion.CHART_DATA_DATE.CUSTOM_RANGE -> {
                selectedStartDate = (activity as ScoutActivity).getSelectedStartDate()
                selectedEndDate = (activity as ScoutActivity).getSelectedEndDate()
                changeButtonStyle(customDateButtom)
                requestSensorData(selectedStartDate, selectedEndDate)
            }
        }
    }

    private fun requestSensorData(selectedStartDate: String?, selectedEndDate: String?) {
        requestSingleSensorData(selectedStartDate, selectedEndDate, toBeComparedList?.first())
    }

    private fun requestSingleSensorData(
        startDate: String?,
        endDate: String?,
        toBeCompared: ToBeCompared?
    ) {
        chartLoading.show()
        val postData = GetSensorDataRequest(
            startDate,
            endDate,
            toBeCompared?.comparableReadingType?.readingTypeId,
            toBeCompared?.comparableReadingType?.categoryCode,
            gduDataItem?.id
        )
        mDisposable?.add(
            dataSource.getSensorData(postData).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    chartLoading.hide()
                    handleRequestSensorData(it, toBeCompared)
                }, {
                    chartLoading.hide()
                })
        )
    }

    private fun handleRequestSensorData(
        it: List<ReadingDataItem?>,
        readingTypeItem: ToBeCompared?
    ) {
        populateChart(it)
    }

    private fun populateChart(list: List<ReadingDataItem?>?) {
        if (list.isNullOrEmpty()) {
            chartView.clear()
            chartView.setNoDataText(getString(R.string.no_chart_data))
            chartView.setNoDataTextColor(context.getColorCompat(R.color.colorPrimary))
            unit1TextView.text = ""
            return
        }
        unit1TextView.text = "gdd"
        val chartData = getLineData(list)
        chartData?.setDrawValues(false)
        chartData?.mode = LineDataSet.Mode.CUBIC_BEZIER
        chartData?.setDrawCircles(false)
        chartData?.color = context.getColorCompat(toBeComparedList?.first()?.foreGroundTint!!)
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(
                context.getColorCompat(toBeComparedList?.first()?.foreGroundTint!!),
                context.getColorCompat(android.R.color.transparent)
            )
        )
        chartData?.fillDrawable = gradientDrawable
        chartData?.setDrawFilled(true)
        if ((activity as ScoutActivity).isLandscape)
            chartView.xAxis.labelCount = 6
        else
            chartView.xAxis.labelCount = 3
        chartView?.legend?.isEnabled = false
        chartView?.description?.isEnabled = false
        chartView.data = LineData(chartData)
        chartView.isDoubleTapToZoomEnabled = false
        chartView.xAxis.valueFormatter = XAxisValueFormatter()
        //setting x-axis data range to bottom
        val xAxis = chartView.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        //hiding the  y-axis on right
        val rightYAxis = chartView.axisRight
        rightYAxis.isEnabled = false
//        chartView?.axisLeft?.axisMinimum = -15f
        //setting custom marker when tapped
        val marker =
            YourMarkerView(context, R.layout.view_marker, toBeComparedList?.first()?.foreGroundTint!!)
        chartView.marker = marker
        chartView.invalidate()
    }

    companion object {
        const val TAG_DATE_RANGE = "open_date_range_picker"
        const val TAG_COMPARE_READING = "open_compare_readings"
        fun newInstance(gduDataItem: GDUDataItem?, scoutData: ScoutModel?): ScoutGDUFragment {
            val fragment = ScoutGDUFragment()
            fragment.gduDataItem = gduDataItem
            return fragment
        }
    }
}