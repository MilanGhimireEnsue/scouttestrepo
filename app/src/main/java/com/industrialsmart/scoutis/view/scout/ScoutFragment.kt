package com.industrialsmart.scoutis.view.scout

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.listener.CompareItemListener
import com.industrialsmart.scoutis.listener.DateRangeListener
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.extensions.*
import com.industrialsmart.scoutis.utils.extensions.getColorCompat
import com.industrialsmart.scoutis.utils.view.XAxisValueFormatter
import com.industrialsmart.scoutis.utils.view.YourMarkerView
import com.industrialsmart.scoutis.view.calendar.DateRangePickerFragment
import com.industrialsmart.scoutis.view.dashboard.home.adapter.SensorReadingAdapter
import com.industrialsmart.scoutis.view.scout.compare.CompareFragment
import io.github.hyuwah.draggableviewlib.Draggable
import io.github.hyuwah.draggableviewlib.makeDraggable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_scout.*
import kotlinx.android.synthetic.main.fragment_scout.chartLoading
import kotlinx.android.synthetic.main.fragment_scout.chartView
import kotlinx.android.synthetic.main.fragment_scout.compareButton
import kotlinx.android.synthetic.main.fragment_scout.customDateButtom
import kotlinx.android.synthetic.main.fragment_scout.draggableView
import kotlinx.android.synthetic.main.fragment_scout.lastMonthButton
import kotlinx.android.synthetic.main.fragment_scout.lastWeekButton
import kotlinx.android.synthetic.main.fragment_scout_compare.*
import kotlin.math.abs


class ScoutFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_scout

    private var sensorData: Sensor? = null
    private var selectedReadingType: SensorReading? = null

    //currently selected startDate and endDate
    private var selectedStartDate: String? = null
    private var selectedEndDate: String? = null

    //viewpager life cycler
    private var isVisibleToUser: Boolean = false
    private var isStarted = false

    private fun initViewForLifeCycle() {
        mDisposable = CompositeDisposable()
        setClickListener()
        setSensorData()
        configurationChange((activity as ScoutActivity).isLandscape)

        draggableView.makeDraggable(Draggable.STICKY.AXIS_X, false) // set sticky axis to x & animation to false
        draggableView.makeDraggable(Draggable.STICKY.AXIS_XY) // set sticky axis to xy
        draggableView.makeDraggable() // all default
    }

    fun configurationChange(isFullScreen: Boolean) {
        if (isFullScreen) {
            sensorCurrentReadingRV.hide()
            layoutSensorInfoContainer.setPadding(context!!.dpToPixels(10f), context!!.dpToPixels(10f), 0, 0)
        } else {
            sensorCurrentReadingRV.show()
            layoutSensorInfoContainer.setPadding(
                context!!.dpToPixels(10f),
                context!!.dpToPixels(10f),
                context!!.dpToPixels(10f),
                context!!.dpToPixels(10f)
            )
        }
    }

    /**
     * Change the currently selected readingType, mostly used to trigger from external events
     */
    fun changeReadingType(readingTypeItem: ReadingTypeItem?) {
        if (readingTypeItem != null) {
            selectedReadingType = sensorData?.readings?.find { it.code == readingTypeItem.code }
            sensorData?.readings?.forEach { it.isSelected = false }
            sensorData?.readings?.find { it == selectedReadingType }?.isSelected = true
            sensorCurrentReadingRV?.postDelayed({
                sensorCurrentReadingRV.adapter?.notifyDataSetChanged()
                setChartAccordingToChartDataType()
            }, 200)
        }
    }

    /**
     * Handle click for week/month/custom date
     */
    private fun setClickListener() {
        lastWeekButton.setOnClickListener {
            (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.WEEKLY)
            changeButtonStyle(lastWeekButton)
            requestSensorData(getLastWeekDate(), getCurrentDate())
            //make the start date and end date null
            selectedStartDate = null
            selectedEndDate = null
        }
        lastMonthButton.setOnClickListener {
            (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.MONTHLY)
            changeButtonStyle(lastMonthButton)
            requestSensorData(getLastMonthDate(), getCurrentDate())
            //make the start date and end date null
            selectedStartDate = null
            selectedEndDate = null
        }
        customDateButtom.setOnClickListener {
            val dateRangePickerFragment =
                DateRangePickerFragment.newInstance(object : DateRangeListener {
                    override fun onDateRangeSelected(startDate: String?, endDate: String?) {
                        (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.CUSTOM_RANGE)
                        (activity as ScoutActivity).setSelectedStartEndDate(startDate, endDate)
                        changeButtonStyle(customDateButtom)
                        requestSensorData(startDate, endDate)
                        //set the value of startDate and endDate
                        selectedStartDate = startDate
                        selectedEndDate = endDate
                    }
                }, selectedStartDate, selectedEndDate)
            dateRangePickerFragment.show(childFragmentManager, TAG_DATE_RANGE)
        }
        compareButton.setOnClickListener {
            val comparableSensorList = (activity as ScoutActivity).getComparableListOfSensor()
            if (!comparableSensorList.isNullOrEmpty()) {
                val alreadyCheckedList = listOf(
                    ToBeCompared(
                        sensorData?.title,
                        sensorData?.id,
                        (activity as ScoutActivity).getReadingTypeFromCode(selectedReadingType?.code)
                    )
                )
                val compareFragment = CompareFragment.newInstance(comparableSensorList, alreadyCheckedList, object : CompareItemListener {
                    override fun onCompareItemClicked(toComparedList: List<ToBeCompared?>?) {
                        val mToComparedList = toComparedList?.distinct()
                        if (mToComparedList?.isNullOrEmpty() == false) {
                            if (mToComparedList.size > 1) {
                                //open scout compare screen, used to display sensor compare data
                                (activity as ScoutActivity).showContainer()
                                changeFragment(ScoutCompareFragment.newInstance(mToComparedList.setRandomColors()))
                            } else {
                                //navigate to particular sensor and readingType as per user selection
                                (activity as ScoutActivity).switchSensorAndReading(mToComparedList.first())
                            }
                        }
                    }
                })
                compareFragment.show(childFragmentManager, TAG_COMPARE_READING)
            }
        }
    }

    /**
     * Change button style for currently selected week/month/custom date
     */
    private fun changeButtonStyle(clickedButton: Button) {
        val viewList = mutableListOf(lastWeekButton, lastMonthButton, customDateButtom)
        viewList.remove(clickedButton)
        viewList.forEach {
            it?.apply {
                setTextColor(ContextCompat.getColor(context!!, R.color.colorForegroundDim))
                setBackgroundResource(android.R.color.transparent)
            }
        }
        clickedButton.apply {
            setTextColor(ContextCompat.getColor(context!!, R.color.foregroundColorLight))
            setBackgroundResource(R.drawable.bg_button_rounded)
        }
    }

    private fun setSensorData() {
        sensorLabel.text = sensorData?.title
        sensorIdentifier.text = sensorData?.code
        dateTime.text = sensorData?.dateTime?.getFormattedDate()
        //setting current sensor as selected sensor
        sensorData?.readings?.forEach { it.isSelected = false }
        if (selectedReadingType == null) {
            selectedReadingType = sensorData?.readings?.first()
        }
        sensorData?.readings?.find { it == selectedReadingType }?.isSelected = true
        sensorCurrentReadingRV.apply {
            layoutManager = GridLayoutManager(context, 3, RecyclerView.VERTICAL, false)
            adapter = SensorReadingAdapter(sensorData?.readings!!) {
                if (it != selectedReadingType) {
                    //only if the it type is different than selected type
                    selectedReadingType = it
                    sensorData?.readings?.forEach { it.isSelected = false }
                    sensorData?.readings?.find { it == selectedReadingType }?.isSelected = true
                    adapter?.notifyDataSetChanged()
                    setChartAccordingToChartDataType()
                }
            }
        }
        setChartAccordingToChartDataType()
    }

    /**
     * Set chart selection according to selection type depending upon other sensors
     */
    private fun setChartAccordingToChartDataType() {
        when ((activity as ScoutActivity).getChartDataDate()) {
            ScoutActivity.Companion.CHART_DATA_DATE.WEEKLY -> {
                lastWeekButton.callOnClick()
            }
            ScoutActivity.Companion.CHART_DATA_DATE.MONTHLY -> {
                lastMonthButton.callOnClick()
            }
            ScoutActivity.Companion.CHART_DATA_DATE.CUSTOM_RANGE -> {
                selectedStartDate = (activity as ScoutActivity).getSelectedStartDate()
                selectedEndDate = (activity as ScoutActivity).getSelectedEndDate()
                changeButtonStyle(customDateButtom)
                requestSensorData(selectedStartDate, selectedEndDate)
            }
        }
    }

    private fun requestSensorData(startDate: String?, endDate: String?) {
        chartLoading.show()
        val postData = GetSensorDataRequest(
            startDate,
            endDate,
            (activity as ScoutActivity).getReadingTypeFromCode(selectedReadingType?.code)?.readingTypeId,
            (activity as ScoutActivity).getReadingTypeFromCode(selectedReadingType?.code)?.categoryCode,
            sensorData?.id
        )
        mDisposable?.add(
            dataSource.getSensorData(postData).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    chartLoading.hide()
                    populateChart(it)
                }, {
                    chartLoading.hide()
                })
        )
    }


    private fun populateChart(list: List<ReadingDataItem?>?) {
        if (list.isNullOrEmpty()) {
            draggableView?.removeAllViews()
            chartView.clear()
            chartView.setNoDataText(getString(R.string.no_chart_data))
            chartView.setNoDataTextColor(context.getColorCompat(R.color.colorPrimary))
            unitTextView.text = ""
            return
        }
        unitTextView.text = selectedReadingType?.unit?.addDegreeIfTempUnit()
        val chartData = getLineData(list)
        chartData?.setDrawValues(false)
        chartData?.mode = LineDataSet.Mode.HORIZONTAL_BEZIER
        chartData?.setDrawCircles(false)
        chartData?.color = context.getColorCompat(selectedReadingType?.foreGroundTint!!)
        val gradientDrawable = GradientDrawable(
            GradientDrawable.Orientation.TOP_BOTTOM,
            intArrayOf(
                context.getColorCompat(selectedReadingType?.foreGroundTint!!),
                context.getColorCompat(android.R.color.transparent)
            )
        )
        chartData?.fillDrawable =
            ContextCompat.getDrawable(activity as ScoutActivity, R.color.transparent)
        chartData?.setDrawFilled(false)

        if ((activity as ScoutActivity).isLandscape)
            chartView.xAxis.labelCount = 6
        else
            chartView.xAxis.labelCount = 3
        chartView?.legend?.isEnabled = false
        chartView?.description?.isEnabled = false
        chartView.data = LineData(chartData)
        chartView.isDoubleTapToZoomEnabled = false
        chartView.xAxis.valueFormatter = XAxisValueFormatter()
        //setting x-axis data range to bottom
        val xAxis = chartView.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        //hiding the  y-axis on right
        val rightYAxis = chartView.axisRight
        rightYAxis.isEnabled = false
        chartView.animateY(2000, Easing.EaseOutSine)
//        chartView?.axisLeft?.axisMinimum = -15f
        //setting custom marker when tapped
//        val marker =
//            YourMarkerView(context, R.layout.view_marker, selectedReadingType?.foreGroundTint!!)
//        chartView.marker = marker

        chartView.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onNothingSelected() {
            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {
                val highlight: ArrayList<Highlight?> = ArrayList()
                for (j in 0 until chartView.data.dataSets.size) {
                    var highLightMatched = false
                    val iDataSet = chartView.data.dataSets[j]
                    for (i in (iDataSet as LineDataSet).values.indices) {
                        if ((iDataSet as LineDataSet).values[i].x == e?.x) {
                            highlight.add(Highlight(e.x, e.y, j))
                            highLightMatched = true
                        }
                    }
                    if (!highLightMatched) {
                        val entryList = (iDataSet as LineDataSet).values
                        val xList = entryList?.map { it?.x }
                        e?.x?.let {
                            val closestValue = xList.closestValue(it)
                            highlight.add(Highlight(closestValue!!, e.y, j))
                        }
                    }
                }
                setHighLightedValues(highlight)
//                chartView.highlightValues(highlight.toTypedArray())
//                chartView.setDrawMarkers(true)
            }
        })

        chartView.invalidate()
    }

    private fun setHighLightedValues(highlight: java.util.ArrayList<Highlight?>) {
        draggableView?.removeAllViews()
        highlight.forEach {
            val view =
                LayoutInflater.from(context)
                    .inflate(R.layout.view_marker_compare, compareRootLayout, false)
            val tvContent = view.findViewById<TextView>(R.id.tvContent)
            val entryData = chartView.data.getEntryForHighlight(it)
            tvContent.text = getFormattedDateForXAxis(entryData.x) + " : " + entryData.y
//            tvContent.compoundDrawables[0].setTint(ContextCompat.getColor(context!!, entryData?.data as Int))
            draggableView.addView(view)
        }
    }

    fun List<Float?>?.closestValue(value: Float) = this?.minBy { abs(value - it!!) }

    override fun onStart() {
        super.onStart()
        isStarted = true
        if (isVisibleToUser && isStarted) {
            //api call here
            initViewForLifeCycle()
        } else
            mDisposable?.dispose()
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        isVisibleToUser = visible
        if (visible && isStarted) {
            //api call here
            initViewForLifeCycle()
        } else
            mDisposable?.dispose()
    }

    override fun onStop() {
        super.onStop()
        isStarted = false
    }

    companion object {
        const val TAG_DATE_RANGE = "open_date_range_picker"
        const val TAG_COMPARE_READING = "open_compare_readings"
        fun newInstance(sensorData: Sensor?): ScoutFragment {
            val fragment = ScoutFragment()
            fragment.sensorData = sensorData
            return fragment
        }
    }
}