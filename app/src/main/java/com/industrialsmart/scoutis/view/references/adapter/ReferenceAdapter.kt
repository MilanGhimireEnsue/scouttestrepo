package com.industrialsmart.scoutis.view.references.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ReferenceItem
import kotlinx.android.synthetic.main.item_reference.view.*

class ReferenceAdapter(private val referenceList: List<ReferenceItem?>, val itemClick: (ReferenceItem?) -> Unit) :
    RecyclerView.Adapter<ReferenceAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_reference, parent, false)
        return MyViewHolder(view, itemClick)
    }

    override fun getItemCount(): Int {
        return referenceList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(referenceList[position])
    }

    class MyViewHolder(view: View, private val itemClick: (ReferenceItem?) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bind(item: ReferenceItem?) {
            itemView.referenceTitle.text = item?.name
            itemView.referenceContent.text = item?.content
            itemView.referenceDescription.text = item?.description
            itemView.setOnClickListener {
                itemClick.invoke(item)
            }
        }
    }
}