package com.industrialsmart.scoutis.view.appinfo

import com.industrialsmart.scoutis.BuildConfig
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.SysInfoResponse
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.view.dialog.InfoDialogFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_app_info.*
import org.jetbrains.anko.startActivity

class AppInfoFragment : BaseFragment() {
    override val layoutId: Int
        get() = R.layout.fragment_app_info

    override fun initView() {
        super.initView()
        (activity as ToolbarNavActivity).setToolbarTitle("App Info")
        appVersion.text = BuildConfig.VERSION_NAME
        requestSysInfo()
        privacyPolicy.setOnClickListener {
            context?.startActivity<ToolbarNavActivity>(
                ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.WEB_VIEW,
                ToolbarNavActivity.EXTRA_PARAM_1 to "https://www.earthscout.com/privacy-policy/",
                ToolbarNavActivity.EXTRA_PARAM_2 to getString(R.string.privacy_policy)
            )
        }
        termsOfService.setOnClickListener {
            context?.startActivity<ToolbarNavActivity>(
                ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.WEB_VIEW,
                ToolbarNavActivity.EXTRA_PARAM_1 to "https://www.earthscout.com/terms-of-service/",
                ToolbarNavActivity.EXTRA_PARAM_2 to getString(R.string.terms_of_service)
            )
        }
        sensorAccuracyStatement.setOnClickListener {
            InfoDialogFragment.newInstance(getString(R.string.sensor_accuracy_statement), getString(R.string.sensor_accuracy_content))
                .show(childFragmentManager, "INFO_DIALOG")
        }
    }

    private fun requestSysInfo() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getSysInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    handleSysInfoResponse(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleSysInfoResponse(it: SysInfoResponse?) {
        apiVersion.text = it?.version
        databaseVersion.text = it?.versionDbSchemaLastMigration
    }

    companion object {
        fun newInstance(): AppInfoFragment {
            val fragment = AppInfoFragment()
            return fragment
        }
    }
}