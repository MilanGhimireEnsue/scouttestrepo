package com.industrialsmart.scoutis.view.dashboard.notification

import android.content.res.ColorStateList
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseBottomSheetFragment
import com.industrialsmart.scoutis.models.NotificationItem
import com.industrialsmart.scoutis.utils.ScreenUtils
import com.industrialsmart.scoutis.utils.extensions.getFormattedDateTimeFromServerDate
import kotlinx.android.synthetic.main.fragment_notification_detail.*

class NotificationDetailFragment : BaseBottomSheetFragment() {

    private var notificationItem: NotificationItem? = null

    override val layoutId = R.layout.fragment_notification_detail

    override fun initView() {
        super.initView()
        view?.postDelayed(
            {
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<View>(bottomSheet)
                val params = bottomSheet.layoutParams
                params.height = ((ScreenUtils(context!!).height) / 100) * 60 //take about 70 percentage of screen height
                bottomSheet.layoutParams = params
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }, 200
        )
        notificationTitle.text = "${notificationItem?.periodEventDataProviderName} (${notificationItem?.periodEventDataProviderScoutName})"
        notificationSubtitle.text = notificationItem?.periodSubtitle
        notificationDateAndTime.text = notificationItem?.periodEventWhen?.getFormattedDateTimeFromServerDate()
        notificationDetail.text = notificationItem?.text
        setIconColorAccordingToDate()
        closeButton.setOnClickListener {
            dismiss()
        }
    }

    private fun setIconColorAccordingToDate() {
        when (notificationItem?.periodEventSeverity) {
            0 -> {
                icon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.color_severity_0))
            }
            1 -> {
                icon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.color_severity_1))
            }
            2 -> {
                icon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.color_severity_2))
            }
            3 -> {
                icon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.color_severity_3))
            }
            4 -> {
                icon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.color_severity_4))
            }
        }
    }

    companion object {
        fun newInstance(notificationItem: NotificationItem?): NotificationDetailFragment {
            val fragment = NotificationDetailFragment()
            fragment.notificationItem = notificationItem
            return fragment
        }
    }
}