package com.industrialsmart.scoutis.view.scout.compare.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ToBeCompared
import com.industrialsmart.scoutis.utils.extensions.addDegreeIfTempUnit
import kotlinx.android.synthetic.main.item_sensor_reading_compare.view.*

class SensorReadingCompareAdapter(
    private val comparableReadingType: List<ToBeCompared?>,
    val itemCheckChange: (ToBeCompared, Boolean) -> Unit
) :
    RecyclerView.Adapter<SensorReadingCompareAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sensor_reading_compare, parent, false)
        return MyViewHolder(view, itemCheckChange)
    }

    override fun getItemCount(): Int {
        return comparableReadingType.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(comparableReadingType[position])
    }

    class MyViewHolder(
        view: View,
        val itemCheckChange: (ToBeCompared, Boolean) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(toBeCompared: ToBeCompared?) {
            itemView.readingTypeName.text = toBeCompared?.comparableReadingType?.name
            itemView.readingUnit.text = toBeCompared?.comparableReadingType?.categoryCode?.addDegreeIfTempUnit()
            itemView.readingTypeName.setOnCheckedChangeListener { buttonView, isChecked ->
                toBeCompared?.let { itemCheckChange.invoke(it, isChecked) }
            }
            itemView.readingTypeName.isChecked = toBeCompared?.isChecked!!
        }
    }
}