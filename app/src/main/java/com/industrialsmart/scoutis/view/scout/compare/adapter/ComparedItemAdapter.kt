package com.industrialsmart.scoutis.view.scout.compare.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ToBeCompared
import com.industrialsmart.scoutis.utils.extensions.addDegreeIfTempUnit
import kotlinx.android.synthetic.main.item_sensor_compare_reading.view.*

class ComparedItemAdapter(private val toComparedList: List<ToBeCompared?>, val removeClicked: (ToBeCompared?) -> Unit) : RecyclerView.Adapter<ComparedItemAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sensor_compare_reading, parent, false)
        return MyViewHolder(view, parent.context, removeClicked)
    }

    override fun getItemCount(): Int {
        return toComparedList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(toComparedList[position])
    }

    class MyViewHolder(
        view: View,
        val context: Context,
        val removeClicked: (ToBeCompared?) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(readingTypeItem: ToBeCompared?) {
            itemView.sensorName.text = readingTypeItem?.sensorName
            itemView.readingTypeName.text =
                readingTypeItem?.comparableReadingType?.name + " (${readingTypeItem?.comparableReadingType?.categoryCode.addDegreeIfTempUnit()})"
            itemView.detailContainer.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(context, readingTypeItem?.foreGroundTint!!))
            itemView.removeIcon.setOnClickListener {
                removeClicked.invoke(readingTypeItem)
            }
        }
    }
}