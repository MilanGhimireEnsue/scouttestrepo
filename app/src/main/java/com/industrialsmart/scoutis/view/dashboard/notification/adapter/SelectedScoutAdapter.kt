package com.industrialsmart.scoutis.view.dashboard.notification.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ScoutItemResponse
import kotlinx.android.synthetic.main.item_selected_scout.view.*

class SelectedScoutAdapter(var scoutList: List<ScoutItemResponse?>, private val sensorClicked: (ScoutItemResponse?) -> Unit) :
    RecyclerView.Adapter<SelectedScoutAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_selected_scout, parent, false)
        return MyViewHolder(view, sensorClicked)
    }

    override fun getItemCount(): Int {
        return scoutList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(scoutList[position])
    }

    class MyViewHolder(view: View, val sensorClicked: (ScoutItemResponse?) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bind(item: ScoutItemResponse?) {
            itemView.scoutName.text = item?.name
            itemView.removeIcon.setOnClickListener {
                sensorClicked.invoke(item)
            }
        }
    }

    fun setNewData(pScoutList: List<ScoutItemResponse?>) {
        scoutList = pScoutList
        notifyDataSetChanged()
    }
}