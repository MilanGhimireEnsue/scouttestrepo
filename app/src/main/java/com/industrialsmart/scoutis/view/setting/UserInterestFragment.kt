package com.industrialsmart.scoutis.view.setting


import android.view.LayoutInflater
import com.google.android.material.checkbox.MaterialCheckBox
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.IdCodeItem
import com.industrialsmart.scoutis.models.UserDataResponse
import com.industrialsmart.scoutis.models.UserInfo
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.utils.extensions.updateUserSetting
import com.industrialsmart.scoutis.utils.successToast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_interest.*


class UserInterestFragment : BaseFragment() {

    override val layoutId = R.layout.fragment_user_interest

    var selectedItem: MutableList<IdCodeItem>? = null
    var allList: List<IdCodeItem?>? = null
    private lateinit var viewType: ViewType


    override fun initView() {
        super.initView()
        when (viewType) {
            ViewType.CROP_OF_INTEREST -> (activity as ToolbarNavActivity).setToolbarTitle(
                getString(R.string.crops_of_interest),
                rightText = getString(R.string.save)
            )
            ViewType.REGION_OF_INTEREST -> (activity as ToolbarNavActivity).setToolbarTitle(
                getString(R.string.regions_of_interest),
                rightText = getString(R.string.save)
            )
        }
        allList?.forEach {
            val view = LayoutInflater.from(context).inflate(R.layout.item_checkbox, rootView, false)
            val checkButton = view.findViewById<MaterialCheckBox>(R.id.checkBoxItem)
            checkButton.text = it?.name
            if (selectedItem?.contains(it) == true)
                checkButton.isChecked = true
            checkButton.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked)
                    it?.let { it1 -> selectedItem?.add(it1) }
                else
                    selectedItem?.remove(it)
            }
            checkBoxContainer.addView(view)
        }
    }

    companion object {
        fun newInstance(selectedItem: List<IdCodeItem>?, allList: List<IdCodeItem?>, viewType: ViewType): UserInterestFragment {
            val fragment = UserInterestFragment()
            fragment.selectedItem = selectedItem?.toMutableList()
            fragment.allList = allList
            fragment.viewType = viewType
            return fragment
        }
    }

    override fun onRightButtonClicked() {
        super.onRightButtonClicked()
        when (viewType) {
            ViewType.CROP_OF_INTEREST -> {
                val postData = UserInfo.userData
                postData?.userSettingsInfo?.subjects = selectedItem?.toList()
                requestSaveUserInfo(postData)
            }
            ViewType.REGION_OF_INTEREST -> {
                val postData = UserInfo.userData
                postData?.userSettingsInfo?.regions = selectedItem?.toList()
                requestSaveUserInfo(postData)
            }
        }
    }

    private fun requestSaveUserInfo(postData: UserDataResponse?) {
        showProgressDialog()
        mDisposable?.add(
            dataSource.postUserData(postData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it != null) {
                        UserInfo.userData = it
                        context?.successToast(getString(R.string.saved_success_msg))
                    }
                    requireActivity().onBackPressed()
                }, {
                    dismissProgressDialog()
                })
        )
    }
}