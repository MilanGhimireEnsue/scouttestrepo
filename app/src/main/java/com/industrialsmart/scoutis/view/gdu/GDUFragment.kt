package com.industrialsmart.scoutis.view.gdu

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.listener.EditDeleteListener
import com.industrialsmart.scoutis.models.GDUDataItem
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.utils.successToast
import com.industrialsmart.scoutis.view.scout.ScoutActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_gdu.*
import kotlinx.android.synthetic.main.fragment_gdu.pullToRefresh
import kotlinx.android.synthetic.main.layout_empty.*
import org.jetbrains.anko.startActivity

class GDUFragment : BaseFragment() {
    override val layoutId: Int
        get() = R.layout.fragment_gdu

    private var gduResponseList: MutableList<GDUDataItem?>? = null

    override fun initView() {
        super.initView()
        (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.growing_degree_units), R.drawable.ic_add_white)
        pullToRefresh.setOnRefreshListener {
            pullToRefresh.isRefreshing = false
            requestGduList()
        }
    }

    override fun onRightIconClicked() {
        super.onRightIconClicked()
        changeFragment(AddGDUFragment.newInstance())
    }

    override fun onResume() {
        super.onResume()
        requestGduList()
    }

    private fun requestGduList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getGduList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (!it.isNullOrEmpty()) {
                        gduRV.show()
                        emptyLayout.hide()
                        handleGduResponse(it)
                    }else{
                        gduRV.hide()
                        emptyLayout.show()
                    }
                }, {
                    dismissProgressDialog()
                    it.printStackTrace()
                })
        )
    }

    /**
     * Populate the GDU list to recycler view
     */
    private fun handleGduResponse(it: List<GDUDataItem?>) {
        gduResponseList = it.toMutableList()
        gduRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = GduAdapter(it, object : EditDeleteListener {
                override fun onClicked(dataItem: Any?) {
                    getScoutList(dataItem as GDUDataItem?)
                }

                override fun onEdit(dataItem: Any?) {
                    val mGduDataItem = dataItem as GDUDataItem?
                    changeFragment(AddGDUFragment.newInstance(mGduDataItem))
                }

                override fun onDelete(dataItem: Any?) {
                    val mGduDataItem = dataItem as GDUDataItem?
                    showDialog(
                        message = "Are you sure you want to permanently delete this GDU?",
                        title = "Delete ${mGduDataItem?.name}",
                        positiveText = "Delete",
                        negativeText = "Cancel"
                    ) {
                        requestGDUDelete(mGduDataItem)
                    }
                }

            })
        }
        gduRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val pos = (gduRV?.layoutManager as LinearLayoutManager?)?.findFirstVisibleItemPosition()
                pullToRefresh.isEnabled = pos == 0
            }
        })
    }

    private fun getScoutList(gduDataItem: GDUDataItem?) {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getScoutsList().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    dismissProgressDialog()
                    handleScoutList(it, gduDataItem)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleScoutList(scoutList: List<ScoutModel?>, gduDataItem: GDUDataItem?) {
        val relatedScoutModel = scoutList.find { it?.id == gduDataItem?.rawScoutData?.id }
        if (relatedScoutModel != null)
            context?.startActivity<ScoutActivity>(
                ScoutActivity.EXTRA_SCOUT_DATA to relatedScoutModel,
                ScoutActivity.EXTRA_GDU_DATA to gduDataItem,
                ScoutActivity.EXTRA_SCOUT_CHART_TYPE to ScoutActivity.Companion.SCOUT_CHART_TYPE.GDU_CHART
            )
    }

    /**
     * Request GDU delete
     */
    private fun requestGDUDelete(gduDataItem: GDUDataItem?) {
        showProgressDialog()
        mDisposable?.add(
            dataSource.requestGDUDelete(gduDataItem?.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it == true) {
                        context?.successToast("GDU deleted successfully.")
                        gduResponseList?.remove(gduDataItem)
                        gduResponseList?.toList()?.let { it1 -> handleGduResponse(it1) }
                    }
                }, {
                    dismissProgressDialog()
                })
        )
    }

    companion object {
        fun newInstance(): GDUFragment {
            return GDUFragment()
        }
    }
}