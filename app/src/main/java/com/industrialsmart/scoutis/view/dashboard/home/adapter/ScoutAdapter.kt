package com.industrialsmart.scoutis.view.dashboard.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.listener.ScoutClickListener
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.models.UserSetting
import com.industrialsmart.scoutis.models.VisibilityMode
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import kotlinx.android.synthetic.main.tpl_dashboard_item_scout.view.*

class ScoutAdapter(
    private var items: List<ScoutModel?>, private val itemClick: ScoutClickListener
) : RecyclerView.Adapter<ScoutAdapter.MyViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()

    init {
        if (UserSetting.visibilityMode == VisibilityMode.ACTIVE_ONLY) {
            items = items.filter { it?.isActive == true }.toMutableList()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.tpl_dashboard_item_scout, parent, false)
        return MyViewHolder(view, itemClick, parent?.context, viewPool)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(items[position])
    }

    class MyViewHolder(
        view: View,
        private val itemClick: ScoutClickListener,
        private val mContext: Context,
        private val viewPool: RecyclerView.RecycledViewPool
    ) : RecyclerView.ViewHolder(view) {
        fun bind(scoutItem: ScoutModel?) {
            if (scoutItem?.isActive == false) {
                itemView.scoutDimOverlay.show()
                itemView.sensorDimOverlay.show()
            } else {
                itemView.scoutDimOverlay.hide()
                itemView.sensorDimOverlay.hide()
            }
            itemView.scoutTitle.text = scoutItem?.title
            itemView.scoutIdentifier.text = scoutItem?.code
            itemView.scoutDateTime.text = scoutItem?.dateTime
            itemView.scoutSettingIcon.setOnClickListener {
                itemClick.onScoutSettingClicked(scoutItem)
            }
            itemView.scoutInfoContainer.setOnClickListener {
                //for click action, scout should be active and at least one sensor is scout should be active
                if (scoutItem?.isActive == true && ((scoutItem.sensors?.filter { it.isActive == true })?.size!! > 0))
                    itemClick.onScoutClicked(scoutItem)
            }
            itemView.scoutNotification.setOnClickListener {
                if (scoutItem?.isActive == true)
                    itemClick.onScoutNotificationClicked(scoutItem)
            }
            itemView.addScoutNotification.setOnClickListener {
                if (scoutItem?.isActive == true)
                    itemClick.onAddScoutNotificationClicked(scoutItem)
            }
            if (scoutItem?.notificationConfirmationCount != null && scoutItem?.notificationConfirmationCount!! > 0) {
                itemView.unreadNotificationImageView.show()
            } else {
                itemView.unreadNotificationImageView.hide()
            }
            if (!scoutItem?.sensors.isNullOrEmpty())
                itemView.sensorRV.apply {
                    layoutManager = LinearLayoutManager(mContext, RecyclerView.VERTICAL, false)
                    adapter = SensorAdapter(scoutItem?.sensors!!, scoutItem.isActive, scoutItem, itemClick)
                    setRecycledViewPool(viewPool)
                }
        }
    }
}