package com.industrialsmart.scoutis.view.dashboard.calendar

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.models.OccasionItem
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.getFormattedMonthYearForEvent
import com.industrialsmart.scoutis.utils.extensions.getLocalDateFromServerFormat
import com.industrialsmart.scoutis.utils.extensions.inBetweenDateRange
import com.industrialsmart.scoutis.utils.extensions.isDateBetween
import com.industrialsmart.scoutis.utils.warningToast
import com.industrialsmart.scoutis.view.dashboard.DashBoardActivity
import com.industrialsmart.scoutis.view.dashboard.calendar.adapter.CalendarMonthListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_calendar_list.*
import kotlinx.android.synthetic.main.fragment_calendar_list.pullToRefresh
import org.threeten.bp.LocalDate

class CalendarListFragment : BaseFragment() {
    private var occasionMap: MutableMap<LocalDate?, List<OccasionItem?>>? = null

    override val layoutId = R.layout.fragment_calendar_list

    override fun initView() {
        super.initView()
        getOccasionList()?.let { filterOccasionListAccordingToDay(it) }
        pullToRefresh.setOnRefreshListener {
            pullToRefresh.isRefreshing = false
            requestOccasionList()
        }
    }

    fun navIconClicked() {
        requireActivity().onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        (requireParentFragment() as CalendarParentFragment).setToolbarIconForOtherView()
    }

    private fun requestOccasionList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getOccasionList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    setOccasionList(it)
                    filterOccasionListAccordingToDay(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    /**
     * To filter the response in format of <Date, List<OccasionItem?>?>
     */
    private fun filterOccasionListAccordingToDay(occasionList: List<OccasionItem?>) {
        val occasionListMap: MutableMap<LocalDate?, List<OccasionItem?>> = HashMap()
        occasionList?.forEach {
            val date = it?.startDateTime.getLocalDateFromServerFormat()
            it?.eventDay = date
            val eventListForDate: MutableList<OccasionItem?> = ArrayList()
            occasionList?.forEach { occasionItem ->
                if (date?.isDateBetween(occasionItem?.startDateTime, occasionItem?.endDateTime) == true) {
                    eventListForDate.add(occasionItem)
                }
            }
            if (occasionListMap.containsKey(date)) {
                val modifiedList: MutableList<OccasionItem?>? = occasionListMap[date]?.toMutableList()
                modifiedList?.addAll(eventListForDate)
                occasionListMap[date] = modifiedList?.distinct()!!.toList()
            } else {
                occasionListMap[date] = eventListForDate.distinct()
            }
        }
        occasionMap = occasionListMap
        filterData()
    }

    private fun filterData() {
        occasionMap = occasionMap?.toSortedMap(compareBy { it })
        val datesList = occasionMap?.keys
        val filteredMap: MutableMap<String?, List<OccasionItem?>> = LinkedHashMap() //linked hashmap for maintaining order
        datesList?.forEach {
            val dateKey = it?.getFormattedMonthYearForEvent()
            if (filteredMap.containsKey(dateKey)) {
                val modifiedList: MutableList<OccasionItem?>? = filteredMap[dateKey]?.toMutableList()
                modifiedList?.addAll(occasionMap?.get(it)!!.distinct())
                filteredMap[dateKey] = modifiedList?.distinct()!!.toList()
            } else {
                filteredMap[dateKey] = occasionMap?.get(it)!!.distinct()
            }
        }
        setUpRecyclerView(filteredMap)
    }

    private fun setUpRecyclerView(filteredMap: MutableMap<String?, List<OccasionItem?>>) {
        eventRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, true)
            adapter = CalendarMonthListAdapter(filteredMap) {
                EventDetailFragment.newInstance(it) {
                    requestOccasionList()
                }.show(childFragmentManager, "DETAIL_TAG")
            }
        }
        eventRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val pos = (eventRV?.layoutManager as LinearLayoutManager?)?.findLastCompletelyVisibleItemPosition()
                pullToRefresh.isEnabled = pos == (filteredMap.size - 1)
            }
        })
        if (!filteredMap.isNullOrEmpty())
            eventRV.scrollToPosition(filteredMap.size - 1)
    }

    private fun removeEventFromList(it: OccasionItem?) {
        d("okhttp remove event")
    }

    private fun setOccasionList(occasionList: List<OccasionItem?>) {
        (activity as DashBoardActivity).setOccasionList(occasionList)
    }

    private fun getOccasionList(): List<OccasionItem?>? {
        return (activity as DashBoardActivity).getOccasionList()
    }

    companion object {
        fun newInstance(): CalendarListFragment {
            val fragment = CalendarListFragment()
            return fragment
        }
    }
}