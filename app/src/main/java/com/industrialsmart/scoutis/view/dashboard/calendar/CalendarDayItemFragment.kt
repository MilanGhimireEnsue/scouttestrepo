package com.industrialsmart.scoutis.view.dashboard.calendar

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.models.OccasionItem
import com.industrialsmart.scoutis.utils.extensions.getMonthDayString
import com.industrialsmart.scoutis.utils.extensions.getWeekDayString
import com.industrialsmart.scoutis.view.dashboard.calendar.adapter.CalendarEventDayAdapter
import kotlinx.android.synthetic.main.fragment_calendar_day_item.*
import org.threeten.bp.LocalDate

class CalendarDayItemFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_calendar_day_item

    private var occasionList: List<OccasionItem?>? = null
    private var date: LocalDate? = null

    override fun initView() {
        super.initView()
        weekDayTextView.text = date?.getWeekDayString()
        dayTextView.text = date?.getMonthDayString()
        eventRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = occasionList?.let {
                CalendarEventDayAdapter(it) {
                    EventDetailFragment.newInstance(it) {
                        (requireParentFragment() as CalendarDayFragment).getFreshData()
                    }.show(childFragmentManager, "DETAIL_TAG")
                }
            }
        }
    }

    companion object {
        fun newInstance(date: LocalDate?, occasionList: List<OccasionItem?>?): CalendarDayItemFragment {
            val fragment = CalendarDayItemFragment()
            fragment.date = date
            fragment.occasionList = occasionList
            return fragment
        }
    }
}