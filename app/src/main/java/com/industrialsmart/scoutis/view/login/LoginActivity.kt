package com.industrialsmart.scoutis.view.login

import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.util.Base64
import android.view.View
import com.industrialsmart.scoutis.MyApplication
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseActivity
import com.industrialsmart.scoutis.models.UserInfo
import com.industrialsmart.scoutis.service.LoginService
import com.industrialsmart.scoutis.service.getConfigRawFile
import com.industrialsmart.scoutis.service.getScopes
import com.industrialsmart.scoutis.utils.Constants
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.successToast
import com.industrialsmart.scoutis.view.dashboard.DashBoardActivity
import com.microsoft.identity.client.*
import com.microsoft.identity.client.IPublicClientApplication.IMultipleAccountApplicationCreatedListener
import com.microsoft.identity.client.exception.MsalException
import org.jetbrains.anko.startActivity
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class LoginActivity : BaseActivity() {
    override val layoutId: Int get() = R.layout.activity_login

    private lateinit var loginService: LoginService

    var mMultipleAccountApp: IMultipleAccountPublicClientApplication? = null
    var mFirstAccount: IAccount? = null

    override fun initView() {
        super.initView()
        d("okhttp login ${UserInfo.loginStatus}")
        if (UserInfo.loginStatus) {
            startActivity<DashBoardActivity>()
            finish()
            return
        }
        loginService = LoginService(this)
//        getHashkey()
        PublicClientApplication.createMultipleAccountPublicClientApplication(context,
            getConfigRawFile(),
            object : IMultipleAccountApplicationCreatedListener {
                override fun onCreated(application: IMultipleAccountPublicClientApplication) {
                    mMultipleAccountApp = application
                    startB2CAuth()
                    d("b2c Successfully created")
                }

                override fun onError(exception: MsalException) {
                    //Log Exception Here
                    showDialog(exception.message.toString(), positiveText = getString(R.string.ok), negativeText = "") {

                    }
                    d("b2c Failed creation ${exception.message}")
                    d("b2c Failed creation ${exception.stackTrace}")
                }
            })
    }


    fun onLoginClick(view: View?) {
        startB2CAuth()
    }

    private fun startB2CAuth() {
        mMultipleAccountApp?.acquireToken(this, getScopes(), object : AuthenticationCallback {
            override fun onSuccess(authenticationResult: IAuthenticationResult?) {
                d("b2c Successfully authenticated")
                d("b2c ID Token : ${authenticationResult?.accessToken}")
                d("b2c ID Token : ${authenticationResult?.account}")
                d("b2c ID Token : ${authenticationResult}")
                (application as MyApplication).setLoginAccount(authenticationResult?.account, mMultipleAccountApp)
                UserInfo.loginIAccout = authenticationResult?.account?.id.toString()
                UserInfo.accessToken = authenticationResult?.accessToken.toString()
                UserInfo.loginStatus = true
                d("user token ${UserInfo.accessToken}")
                startActivity<DashBoardActivity>()
                finish()
            }

            override fun onCancel() {
                d("b2c cancelled")
            }

            override fun onError(exception: MsalException?) {
                d("b2c onError")
                d("b2c onError : ${exception?.errorCode}")
                d("b2c onError : ${exception?.message}")
                d("b2c onError : ${exception?.stackTrace}")
            }

        })
    }

    fun getHashkey() {
        try {
            val info: PackageInfo? = context?.getPackageManager()?.getPackageInfo(
                context?.applicationContext?.getPackageName(),
                PackageManager.GET_SIGNATURES
            )
            for (signature in info!!.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                successToast(Base64.encodeToString(md.digest(), Base64.NO_WRAP))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            successToast(e.message.toString())
        } catch (e: NoSuchAlgorithmException) {
            successToast(e.message.toString())
        }
    }
}