package com.industrialsmart.scoutis.view.dashboard.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.listener.ScoutClickListener
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.models.Sensor
import com.industrialsmart.scoutis.models.UserSetting
import com.industrialsmart.scoutis.models.VisibilityMode
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import kotlinx.android.synthetic.main.tpl_dashboard_sensor.view.*

class SensorAdapter(
    private var sensorList: List<Sensor?>,
    private val scoutActive: Boolean?,
    private val scoutItem: ScoutModel,
    private val itemClick: ScoutClickListener
) :
    RecyclerView.Adapter<SensorAdapter.MyViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()

    init {
        if (UserSetting.visibilityMode == VisibilityMode.ACTIVE_ONLY) {
            sensorList = sensorList.filter { it?.isActive == true }.toMutableList()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.tpl_dashboard_sensor, parent, false)
        return MyViewHolder(view, parent.context, viewPool, scoutActive, scoutItem, itemClick)
    }

    override fun getItemCount(): Int {
        return sensorList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(sensorList[position])
    }

    class MyViewHolder(
        view: View,
        private val mContext: Context,
        private val viewPool: RecyclerView.RecycledViewPool,
        private val scoutActive: Boolean?,
        private val scoutItem: ScoutModel,
        private val itemClick: ScoutClickListener
    ) : RecyclerView.ViewHolder(view) {
        fun bind(sensorItem: Sensor?) {
            //checking scoutActive flag, just to ensure if the item is already shared by visibilityLayout
            if (sensorItem?.isActive == false && (scoutActive == true))
                itemView.visibilityOverlay.show()
            else
                itemView.visibilityOverlay.hide()
            itemView.sensorLabel.text = sensorItem?.title
            itemView.sensorIdentifier.text = sensorItem?.code
            if (!sensorItem?.readings.isNullOrEmpty())
                itemView.sensorReadingRV.apply {
                    layoutManager = GridLayoutManager(mContext, 3, RecyclerView.VERTICAL, false)
                    adapter = SensorReadingAdapter(sensorItem?.readings!!) {
                        itemClick.onSensorClicked(scoutItem, sensorItem)
                    }
                    setRecycledViewPool(viewPool)
                }
            itemView.setOnClickListener {
                itemClick.onSensorClicked(scoutItem, sensorItem)
            }
        }
    }
}