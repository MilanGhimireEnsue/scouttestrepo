package com.industrialsmart.scoutis.view

import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment

class BlankFragment: BaseFragment() {
    override val layoutId: Int
        get() = R.layout.fragment_blank

    override fun initView() {
        super.initView()
    }

    companion object{
        fun newInstance(): BlankFragment{
            return BlankFragment()
        }
    }
}