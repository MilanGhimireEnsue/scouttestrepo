package com.industrialsmart.scoutis.view.dashboard.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import kotlinx.android.synthetic.main.custom_tab_layout.view.*

class ViewPagerAdapter(fragmentManager: FragmentManager, context: Context) :
    FragmentStatePagerAdapter(fragmentManager) {

    private val fragments = ArrayList<Fragment>()
    private val mContext = context

    fun addFragment(fragment: Fragment) {
        fragments.add(fragment)
    }

    fun addAllFragment(fragmentList: List<Fragment>) {
        fragments.addAll(fragmentList)
    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    fun getTabView(position: Int): View {
        //inflating the custom tab
        var view = LayoutInflater.from(mContext).inflate(R.layout.custom_tab_layout, null)
        var mTabNameTextView = view.tabNameTextView
        mTabNameTextView.text = getTabBottomTitle(position)
        var img = view.tabImageView
        img.setImageResource(getInactiveTabBottomImg(position)!!)
        if (position == 3)
            view.badgeCountTextView.show()
        else
            view.badgeCountTextView.hide()
        return view
    }

    companion object {
        fun getTabBottomTitle(position: Int): String? {
            when (position) {
                0 -> return "Home"
                1 -> return "Maps"
                2 -> return "Calendar"
                3 -> return "Notification"
            }
            return null
        }

        fun getInactiveTabBottomImg(position: Int): Int? {
            when (position) {
                0 -> return R.drawable.ic_home_inactive
                1 -> return R.drawable.ic_map_inactive
                2 -> return R.drawable.ic_calendar_inactive
                3 -> return R.drawable.ic_notification_inactive
            }
            return null
        }

        fun getActiveTabBottomImg(position: Int): Int? {
            when (position) {
                0 -> return R.drawable.ic_home_active
                1 -> return R.drawable.ic_map_active
                2 -> return R.drawable.ic_calendar_active
                3 -> return R.drawable.ic_notification_active
            }
            return null
        }
    }


}