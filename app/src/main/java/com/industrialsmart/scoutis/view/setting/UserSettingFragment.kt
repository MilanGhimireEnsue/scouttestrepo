package com.industrialsmart.scoutis.view.setting

import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.logoutApplication
import com.industrialsmart.scoutis.utils.extensions.regionToIdCodeList
import com.industrialsmart.scoutis.utils.extensions.subjectToIdCodeList
import com.industrialsmart.scoutis.utils.extensions.updateUserSetting
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_setting.*
import org.jetbrains.anko.toast

class UserSettingFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_user_setting

    override fun initView() {
        super.initView()
        (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.user_settings), R.drawable.ic_logout)
    }

    override fun onResume() {
        super.onResume()
        getUserData()
        visibilitySwitch.setOnClickedButtonListener {
            //update visibility mode
            if (it == 0)
                UserSetting.visibilityMode = VisibilityMode.ALL
            else if (it == 1)
                UserSetting.visibilityMode = VisibilityMode.ACTIVE_ONLY
        }
        tempUnitSwitch.setOnClickedButtonListener {
            //update temp unit
            val postData = UserInfo.userData
            if (it == 0)
                postData?.userSettingsInfo?.units?.find { it.name == "Temperature" }?.value = TemperatureUnit.CELSIUS.unitCode
            else if (it == 1)
                postData?.userSettingsInfo?.units?.find { it.name == "Temperature" }?.value = TemperatureUnit.FAHRENHEIT.unitCode
            requestUserSettingUpdate(postData)
        }
        soilUnitSwitch.setOnClickedButtonListener {
            //update soil unit
            val postData = UserInfo.userData
            if (it == 0)
                postData?.userSettingsInfo?.units?.find { it.name == "SoilEC" }?.value = SoilECUnit.USCM.unitCode
            else if (it == 1)
                postData?.userSettingsInfo?.units?.find { it.name == "SoilEC" }?.value = SoilECUnit.DSM.unitCode
            requestUserSettingUpdate(postData)
        }
        //set current position for visibility mode depending upon previous setting
        if (UserSetting.visibilityMode == VisibilityMode.ACTIVE_ONLY)
            visibilitySwitch.setPosition(1, 0)
        cropsOfInterest.setOnClickListener {
            requestCropsList()
        }
        regionOfInterest.setOnClickListener {
            requestRegionsList()
        }
    }

    private fun requestCropsList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getSubjectList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it?.isNullOrEmpty() == false)
                        openCropsOfInterest(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun requestRegionsList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getRegionList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it?.isNullOrEmpty() == false)
                        openRegionsOfInterest(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun openRegionsOfInterest(regionsList: List<AppRegionItem?>) {
        changeFragment(
            UserInterestFragment.newInstance(
                UserInfo.userData?.userSettingsInfo?.regions,
                regionsList.regionToIdCodeList(),
                ViewType.REGION_OF_INTEREST
            )
        )

    }

    private fun openCropsOfInterest(cropsList: List<AppSubjectItem?>) {
        changeFragment(
            UserInterestFragment.newInstance(
                UserInfo.userData?.userSettingsInfo?.subjects,
                cropsList.subjectToIdCodeList(),
                ViewType.CROP_OF_INTEREST
            )
        )

    }

    override fun onRightIconClicked() {
        super.onRightIconClicked()
        context?.logoutApplication()
    }

    private fun requestUserSettingUpdate(postData: UserDataResponse?) {
        showProgressDialog()
        mDisposable?.add(
            dataSource.postUserData(postData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it != null)
                        UserInfo.userData = it
                    updateUserSetting()
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun getUserData() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    handleUserDataResponse(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleUserDataResponse(userDataResponse: UserDataResponse?) {
        UserInfo.userData = userDataResponse
        updateUserSetting()
        userNameEditText.text = userDataResponse?.authUsername
        cropsOfInterest.text =
            if (userDataResponse?.userSettingsInfo?.subjects?.isNullOrEmpty() == true) getString(R.string.none) else userDataResponse?.userSettingsInfo?.subjects?.map { it.name }
                ?.joinToString(", ")
        regionOfInterest.text =
            if (userDataResponse?.userSettingsInfo?.regions?.isNullOrEmpty() == true) getString(R.string.none) else userDataResponse?.userSettingsInfo?.regions?.map { it.name }
                ?.joinToString(", ")
        val tempUnitResponse = userDataResponse?.userSettingsInfo?.units?.find { it.name == "Temperature" }?.value
        val soilECUnitResponse = userDataResponse?.userSettingsInfo?.units?.find { it.name == "SoilEC" }?.value
        if (tempUnitResponse == TemperatureUnit.FAHRENHEIT.unitCode) {
            tempUnitSwitch.setPosition(1, 0)
        }
        if (soilECUnitResponse == SoilECUnit.DSM.unitCode) {
            soilUnitSwitch.setPosition(1, 0)
        }
    }

    companion object {
        fun newInstance(): UserSettingFragment {
            return UserSettingFragment()
        }
    }
}