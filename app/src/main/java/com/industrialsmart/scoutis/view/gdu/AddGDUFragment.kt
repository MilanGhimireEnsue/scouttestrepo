package com.industrialsmart.scoutis.view.gdu

import android.app.DatePickerDialog
import android.widget.DatePicker
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.*
import com.industrialsmart.scoutis.utils.extensions.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_add_gdu.*
import java.util.*


class AddGDUFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_add_gdu

    private var subjectListResponse: List<AppSubjectItem?>? = null

    private var existingDataItem: GDUDataItem? = null

    private var selectedRelatedScout: ScoutItemResponse? = null
    private var selectedAppSubject: AppSubjectItem? = null
    private var selectedCategory: GDUCategoryItem? = null

    private var mDatePickerDialog: DatePickerDialog? = null

    override fun initView() {
        super.initView()
        //style to be changed during add/edit
        if (existingDataItem == null) {
            (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.new_gdu))
            addButton.text = "ADD"
        } else {
            (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.edit_gdu))
            addButton.text = "DONE"
            //disable type spinner
            typeSpinner.isEnabled = false
            typeLabel.alpha = 0.5f
            //disable start date field
            startDateEditText.isEnabled = false
            startDateEditText.alpha = 0.5f
            startDateLabel.alpha = 0.5f
            //disable related scout
            relatedScoutSpinner.isEnabled = false
            relatedScoutLabel.alpha = 0.5f
            //disable category
            categorySpinner.isEnabled = false
            categoryLabel.alpha = 0.5f
        }
        nameEditText.setOnTouchListener { v, event ->
            nameEditText.onTouchEvent(event)
            nameEditText.text?.length?.let { nameEditText.setSelection(it) }
            true
        }
        descriptionEditText.setOnTouchListener { v, event ->
            descriptionEditText.onTouchEvent(event)
            descriptionEditText.text?.length?.let { descriptionEditText.setSelection(it) }
            true
        }
        requestScoutList()
        requestSubjectList()
        requestGDUCategory()
        addButton.setOnClickListener {
            validateData()
        }
        startDateEditText.setOnClickListener {
            showDatePickerDialog(Constants.DateType.START_DATE)
        }
        endDateEditText.setOnClickListener {
            showDatePickerDialog(Constants.DateType.END_DATE)
        }
        if (existingDataItem != null)
            setUpEditData()
    }

    private fun setUpEditData() {
        nameEditText.textValue = existingDataItem?.name.toString()
        startDateEditText.textValue = existingDataItem?.subjectWhenPlanting.getFormattedDateFromServerDate()
        if (existingDataItem?.subjectWhenHarvesting?.isEmpty() == false)
            endDateEditText.textValue = existingDataItem?.subjectWhenHarvesting.getFormattedDateFromServerDate()
        if (existingDataItem?.description?.isEmpty() == false)
            descriptionEditText.textValue = existingDataItem?.description.toString()

    }

    private fun requestScoutList() {
        mDisposable?.add(
            dataSource.getRawScoutList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    handleScoutList(it)
                }, {

                })
        )
    }

    /**
     * Setup and populate related scout spinner
     */
    private fun handleScoutList(scoutList: List<ScoutItemResponse?>?) {
        val textList = scoutList?.map { it?.name!! }
        relatedScoutSpinner.setHintSpinnerAdapter(textList, "Required")
        relatedScoutSpinner.onItemSelected { view, position, l ->
            selectedRelatedScout = position?.let { pos -> scoutList?.get(pos - 1) }
        }
        if (existingDataItem?.rawScoutData != null) {
            val pos = scoutList?.indexOf(existingDataItem?.rawScoutData)
            if (pos != null) {
                relatedScoutSpinner.setSelection(pos + 1)
            }
        }
    }

    private fun requestSubjectList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getSubjectList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    this.subjectListResponse = it
                    handleSubjectList(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    /**
     * Setup and populate subject/type spinner
     */
    private fun handleSubjectList(subjectList: List<AppSubjectItem?>?) {
        val textList = subjectList?.map { it?.name!! }
        typeSpinner.setHintSpinnerAdapter(textList, "Required")
        typeSpinner.onItemSelected { view, position, l ->
            selectedAppSubject = position?.let { pos -> subjectList?.get(pos - 1) }
        }
        if (existingDataItem?.subjectId != null) {
            val selectedSubject = subjectList?.find { it?.id == existingDataItem?.subjectId }
            val pos = subjectList?.indexOf(selectedSubject)
            if (pos != null) {
                typeSpinner.setSelection(pos + 1)
            }
        }
    }

    private fun requestGDUCategory() {
        val gduCategoryList = listOf<GDUCategoryItem>(GDUCategoryItem("crop", "Crop"), GDUCategoryItem("pest", "Pest/Insect"))
        val gduCategoryString = gduCategoryList.map { it?.name!! }
        categorySpinner.setHintSpinnerAdapter(gduCategoryString, "Required")
        categorySpinner.onItemSelected { view, position, l ->
            selectedCategory = position?.let { gduCategoryList.get(it - 1) }
            val filteredSubjectList = subjectListResponse?.filter { it?.subjectTypeId?.equals(selectedCategory?.id, true)!! }
            if (!filteredSubjectList.isNullOrEmpty())
                handleSubjectList(filteredSubjectList)
        }
        if (existingDataItem?.subjectTypeId != null) {
            val selectedGDUCategoryItem = gduCategoryList.find { it.id == existingDataItem?.subjectTypeId }
            val pos: Int? = gduCategoryList.indexOf(selectedGDUCategoryItem)
            if (pos != null) {
                categorySpinner?.setSelection(pos + 1)
            }
        }
    }


    private fun showDatePickerDialog(dateType: Constants.DateType) {
        when (dateType) {
            Constants.DateType.START_DATE -> {
                mDatePickerDialog = DatePickerDialog(
                    context!!,
                    { datePicker: DatePicker, year: Int, month: Int, day: Int ->
                        startDateEditText.textValue = getDateString(year, month, day)
                    },
                    getYearFromDateString(startDateEditText.textValue),
                    getMonthFromDateString(startDateEditText.textValue),
                    getDayFromDateString(startDateEditText.textValue)
                )
//                mDatePickerDialog?.datePicker?.minDate = getCurrentUTCDate()
                mDatePickerDialog?.show()
            }
            Constants.DateType.END_DATE -> {
                mDatePickerDialog = DatePickerDialog(
                    context!!,
                    { datePicker: DatePicker, year: Int, month: Int, day: Int ->
                        endDateEditText.textValue = getDateString(year, month, day)
                    },
                    getYearFromDateString(endDateEditText.textValue),
                    getMonthFromDateString(endDateEditText.textValue),
                    getDayFromDateString(endDateEditText.textValue)
                )
//                mDatePickerDialog?.datePicker?.minDate = getCurrentUTCDate()
                mDatePickerDialog?.show()
            }
        }
    }

    /**
     * Validate the post data
     */
    private fun validateData() {
        if (nameEditText.textValue.isEmpty()) {
            context?.warningToast("Please enter GDU name.")
            return
        }
        if (startDateEditText.textValue.isEmpty()) {
            context?.warningToast("Please select start date.")
            return
        }
        if (selectedRelatedScout == null) {
            context?.warningToast("Please select the related scout.")
            return
        }
        if (selectedCategory == null) {
            context?.warningToast("Please select the GDU category.")
            return
        }
        if (selectedAppSubject == null) {
            89
            context?.warningToast("Please select the GDU type.")
            return
        }
        requestAddGDU()
    }

    private fun requestAddGDU() {
        val postData = GDUResponse(
            holdingId = selectedRelatedScout?.holdingId,
            subjectId = selectedAppSubject?.id,
            subjectTypeId = selectedCategory?.id,
            subjectWhenPlanting = startDateEditText.textValue + "T12:00:00Z",
            isHoldingDefaultSeason = true,
            code = UUID.randomUUID().toString(),
            name = nameEditText.textValue
        )
        if (endDateEditText.textValue.isNotEmpty())
            postData.subjectWhenHarvesting = endDateEditText.textValue + "T12:00:00Z"
        if (descriptionEditText.textValue.isNotEmpty())
            postData.description = descriptionEditText.textValue
        if (existingDataItem != null) {
            postData.id = existingDataItem?.id
            postData.code = existingDataItem?.code
        }
        showProgressDialog()
        mDisposable?.add(
            dataSource.requestAddGDU(postData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it == null)
                        context?.errorToast("Error occurred. Please try again.")
                    else {
                        if (existingDataItem == null)
                            context?.successToast("GDU added successfully.")
                        else
                            context?.successToast("GDU edited successfully.")
                        requireActivity().onBackPressed()
                    }
                }, {
                    dismissProgressDialog()
                })
        )
    }

    companion object {

        fun newInstance(): AddGDUFragment {
            return AddGDUFragment()
        }

        fun newInstance(gduDataItem: GDUDataItem?): AddGDUFragment {
            val fragment = AddGDUFragment()
            fragment.existingDataItem = gduDataItem
            return fragment
        }
    }
}