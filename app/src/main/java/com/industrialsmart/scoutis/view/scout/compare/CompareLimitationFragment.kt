package com.industrialsmart.scoutis.view.scout.compare

import android.text.Html
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseBottomSheetFragment
import com.industrialsmart.scoutis.models.ToBeCompared
import com.industrialsmart.scoutis.utils.extensions.addDegreeIfTempUnit
import com.industrialsmart.scoutis.utils.warningToast
import kotlinx.android.synthetic.main.fragment_compare_limitation.*

class CompareLimitationFragment : BaseBottomSheetFragment() {
    private lateinit var applyCallback: (List<ToBeCompared?>) -> Unit
    private var readingTypeItem: ToBeCompared? = null
    private var distinctCategoryCode: MutableList<ToBeCompared?>? = null
    override val layoutId = R.layout.fragment_compare_limitation

    override fun initView() {
        super.initView()
        setUpDataToView()
    }

    private fun setUpDataToView() {
        limitationInfoTextView.text = Html.fromHtml(
            getString(
                R.string.compare_limitation_info,
                "${readingTypeItem?.comparableReadingType?.name} (${readingTypeItem?.comparableReadingType?.categoryCode?.addDegreeIfTempUnit()})"
            )
        )
        readingTypeOneLabel.text = "Reading Type A (${distinctCategoryCode?.get(0)?.comparableReadingType?.categoryCode?.addDegreeIfTempUnit()})"
        readingTypeTwoLabel.text = "Reading Type B (${distinctCategoryCode?.get(1)?.comparableReadingType?.categoryCode?.addDegreeIfTempUnit()})"

        applyButton.setOnClickListener {
            if (readingTypeOneSwitch.isChecked && readingTypeTwoSwitch.isChecked) { //if both are selected, show warning
                dismiss()
                return@setOnClickListener
            }
            val uncheckedItemList: MutableList<ToBeCompared?> = ArrayList()
            if (!readingTypeOneSwitch.isChecked)
                uncheckedItemList.add(distinctCategoryCode?.get(0))
            if (!readingTypeTwoSwitch.isChecked)
                uncheckedItemList.add(distinctCategoryCode?.get(1))
            dismiss()
            applyCallback.invoke(uncheckedItemList)
        }
        cancelButton.setOnClickListener {
            dismiss()
        }
    }

    companion object {
        fun newInstance(
            distinctCategoryCode: MutableList<ToBeCompared?>,
            readingTypeItem: ToBeCompared,
            applyCallback: (List<ToBeCompared?>) -> Unit
        ): CompareLimitationFragment {
            val fragment = CompareLimitationFragment()
            fragment.distinctCategoryCode = distinctCategoryCode
            fragment.readingTypeItem = readingTypeItem
            fragment.applyCallback = applyCallback
            return fragment
        }
    }
}