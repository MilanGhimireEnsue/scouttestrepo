package com.industrialsmart.scoutis.view.dashboard.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.SensorReading
import com.industrialsmart.scoutis.utils.extensions.addDegreeIfTempUnit
import kotlinx.android.synthetic.main.tpl_dashboard_sensor_reading.view.*

class SensorReadingAdapter(val readings: List<SensorReading?>, val itemClick: (SensorReading?) -> Unit) :
    RecyclerView.Adapter<SensorReadingAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.tpl_dashboard_sensor_reading, parent, false)
        return MyViewHolder(view, parent.context, itemClick)
    }

    override fun getItemCount(): Int {
        return readings.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(readings[position])
    }

    class MyViewHolder(
        view: View,
        val context: Context,
        val itemClick: (SensorReading?) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(
            readingItem: SensorReading?
        ) {
            itemView.sensorReadingValue.text = readingItem?.value
            itemView.sensorReadingLabel.text = readingItem?.name
            itemView.sensorReadingUnit.text = readingItem?.unit?.addDegreeIfTempUnit()
            if (readingItem?.isSelected == true) {
                //setting the color of selected item as reading item
                itemView.sensorReadingValue.setTextColor(ContextCompat.getColor(context, readingItem.foreGroundTint))
                itemView.sensorReadingValue.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_circle, 0, 0, 0)
                itemView.sensorReadingValue.compoundDrawables[0].setTint(ContextCompat.getColor(context, readingItem.foreGroundTint))
                itemView.sensorReadingUnit.setTextColor(ContextCompat.getColor(context, readingItem.foreGroundTint))
                itemView.sensorReadingLabel.setTextColor(ContextCompat.getColor(context, readingItem.foreGroundTint))
            } else {
                itemView.sensorReadingValue.setTextColor(ContextCompat.getColor(context, R.color.colorForegroundDarkest))
                itemView.sensorReadingValue.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                itemView.sensorReadingUnit.setTextColor(ContextCompat.getColor(context, R.color.colorForegroundDarkest))
                itemView.sensorReadingLabel.setTextColor(ContextCompat.getColor(context, R.color.colorForegroundDim))
            }
            itemView.setOnClickListener {
                itemClick.invoke(readingItem)
            }
        }

    }
}