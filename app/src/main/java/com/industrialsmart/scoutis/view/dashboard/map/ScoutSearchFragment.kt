package com.industrialsmart.scoutis.view.dashboard.map

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.utils.ScreenUtils
import com.industrialsmart.scoutis.utils.extensions.afterTextChanged
import com.industrialsmart.scoutis.view.dashboard.map.adapter.ScoutSearchAdapter
import kotlinx.android.synthetic.main.fragment_scout_search.*

class ScoutSearchFragment : BottomSheetDialogFragment() {

    private lateinit var itemClicked: (ScoutModel?) -> Unit
    private var scoutList: List<ScoutModel?>? = null
    private var filteredList: MutableList<ScoutModel?>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // get the views and attach the listener
        // get the views and attach the listener
        return inflater.inflate(R.layout.fragment_scout_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.postDelayed(
            {
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<View>(bottomSheet)
                val params = bottomSheet.layoutParams
                params.height = ((ScreenUtils(context!!).height) / 100) * 80 //take about 90 percentage of screen height
                bottomSheet.layoutParams = params
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }, 200
        )
        filteredList = scoutList?.toMutableList()
        scoutListRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = filteredList?.let {
                ScoutSearchAdapter(it) {
                    handleScoutItemClicked(it)
                }
            }
        }
        scoutSearch.afterTextChanged { it ->
            filteredList = if (it.isEmpty())
                scoutList?.toMutableList()
            else
                scoutList?.filter { item -> item?.title?.contains(it, true)!! || item.code.contains(it, true) }?.toMutableList()
            scoutListRV.apply {
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = filteredList?.let {
                    ScoutSearchAdapter(it) {
                        handleScoutItemClicked(it)
                    }
                }
            }
        }

    }

    private fun handleScoutItemClicked(it: ScoutModel?) {
        itemClicked.invoke(it)
        dismiss()
    }

    companion object {
        fun newInstance(scoutList: List<ScoutModel?>, itemClicked: (ScoutModel?) -> Unit): ScoutSearchFragment {
            val fragment = ScoutSearchFragment()
            fragment.scoutList = scoutList
            fragment.itemClicked = itemClicked
            return fragment
        }
    }
}