package com.industrialsmart.scoutis.view.dashboard.notification

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.NotificationItem
import com.industrialsmart.scoutis.models.ScoutItemResponse
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.getCurrentDate
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.view.dashboard.DashBoardActivity
import com.industrialsmart.scoutis.view.dashboard.notification.adapter.NotificationAdapter
import com.industrialsmart.scoutis.view.dashboard.notification.adapter.SelectedScoutAdapter
import com.industrialsmart.scoutis.view.dashboard.notification.filter.ScoutFilterFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.fragment_notification.pullToRefresh
import org.jetbrains.anko.startActivity

class NotificationFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_notification

    //for lifecycle
    private var isVisibleToUser: Boolean = false
    private var isStarted = false

    private var notificationListResponse: List<NotificationItem?>? = null //for saving all the notification list from response
    private var unReadNotification: List<NotificationItem?>? = null //for saving unRead notification from current showing list
    private var currentShowingAllNotification: List<NotificationItem?>? = null //for saving current showing list
    private var selectedScoutList: MutableList<ScoutItemResponse?> = ArrayList() //for saving currently selected scout filters
    private var scoutItemFromDashboard: ScoutModel? = null

    override fun initView() {
        super.initView()
        if (activity is ToolbarNavActivity) //triggering setup view when opening form scout notification filter
            setUpView()
    }

    private fun initViewForLifeCycle() {
        mDisposable = CompositeDisposable()
        setUpView()
    }

    private fun setUpView() {
        requestNotificationList()
        messageTypeSwitch.setOnClickedButtonListener {
            //update read/unread  mode
            if (it == 0)
                notificationListResponse?.let { handleNotificationResponse(currentShowingAllNotification!!) }
            else if (it == 1)
                unReadNotification?.let { handleNotificationResponse(unReadNotification!!) }
        }
        filterButton.setOnClickListener {
            if (scoutItemFromDashboard == null)
                ScoutFilterFragment.newInstance(selectedScoutList) {
                    applyFilter(it)
                }.show(childFragmentManager, "SCOUT_FILTER_TAG")
            else
                activity?.onBackPressed()
        }
        notificationSettingButton.setOnClickListener {
            context?.startActivity<ToolbarNavActivity>(ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.NOTIFICATION_SETTING)
        }
        markAllAsRead.setOnClickListener {
            requestMarkAllAsRead()
        }
        setUpViewAccordingToScout()
        pullToRefresh.setOnRefreshListener {
            pullToRefresh.isRefreshing = false
            requestNotificationList()
        }
    }

    private fun setUpViewAccordingToScout() {
        //for navigating notification from dashboard
        if (scoutItemFromDashboard != null) {
            filterButton.setImageResource(R.drawable.ic_back)
            selectedScoutName.show()
            selectedScoutName.text = scoutItemFromDashboard?.title
        }
    }

    override fun onResume() {
        super.onResume()
        if (activity is ToolbarNavActivity)
            (activity as ToolbarNavActivity).hideToolbar()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (activity is ToolbarNavActivity)
            (activity as ToolbarNavActivity).showToolbar()
    }

    private fun applyFilter(scoutList: List<ScoutItemResponse?>) {
        selectedScoutList = scoutList.toMutableList()
        if (selectedScoutList.isEmpty()) {
            selectedScoutRV.hide()
            notificationListResponse?.let {
                setReadUnReadCount(notificationListResponse!!)
                handleNotificationResponse(notificationListResponse!!)
            }
        } else {
            selectedScoutRV.show()
            selectedScoutRV.apply {
                layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
                adapter = SelectedScoutAdapter(selectedScoutList) {
                    selectedScoutList.remove(it)
                    if (selectedScoutList.isEmpty()) {
                        applyFilter(selectedScoutList)
                    } else {
                        (adapter as SelectedScoutAdapter).setNewData(selectedScoutList)
                        filterNotificationList(selectedScoutList)
                    }
                }
            }
            filterNotificationList(scoutList)
        }
    }

    private fun filterNotificationList(scoutList: List<ScoutItemResponse?>) {
        val scoutIdList = scoutList.map { it?.id }
        notificationListResponse?.filter { scoutIdList.contains(it?.periodEventDataProviderScoutId) }?.let {
            setReadUnReadCount(it)
            handleNotificationResponse(it)
        }
    }

    private fun requestNotificationList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getNotificationList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    notificationListResponse = it
                    if (scoutItemFromDashboard != null) {
                        //for navigating notification from dashboard
                        val notificationListForScoutItem = it.filter { it?.periodEventDataProviderScoutId == scoutItemFromDashboard?.id }
                        setReadUnReadCount(notificationListForScoutItem)
                        handleNotificationResponse(notificationListForScoutItem)
                    } else {
                        notificationListResponse?.let {
                            setReadUnReadCount(notificationListResponse!!)
                            handleNotificationResponse(notificationListResponse!!)
                        }
                    }
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleNotificationResponse(mNotificationListResponse: List<NotificationItem?>) {
        notificationRV?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = NotificationAdapter(mNotificationListResponse) {
                requestNotificationReadStatus(it)
            }
        }
        notificationRV.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val pos = (notificationRV?.layoutManager as LinearLayoutManager?)?.findFirstVisibleItemPosition()
                pullToRefresh.isEnabled = pos == 0
            }
        })
    }

    private fun requestNotificationReadStatus(it: NotificationItem?) {
        if (it?.periodEventIsPeriodNotificationConfirmed == true) {
            NotificationDetailFragment.newInstance(it).show(childFragmentManager, "NOTIFICATION_DETAIL_TAG")
            return
        }
        showProgressDialog()
        it?.apply {
            confirmedWhen = getCurrentDate()
            periodEventIsPeriodNotificationConfirmed = true
        }
        mDisposable?.add(
            dataSource.postNotificationRead(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    requestNotificationList()
                    NotificationDetailFragment.newInstance(it).show(childFragmentManager, "NOTIFICATION_DETAIL_TAG")
                }, {
                    dismissProgressDialog()
                })
        )
    }


    /**
     * Request for marking currently showing notification as read
     */
    private fun requestMarkAllAsRead() {
        val idList = unReadNotification?.map { it?.id }
        val idListString = idList?.joinToString(",")
        showProgressDialog()
        mDisposable?.add(
            dataSource.postBulkNotificationRead(idListString)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    requestNotificationList()
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun setReadUnReadCount(pNotificationListResponse: List<NotificationItem?>) {
        currentShowingAllNotification = pNotificationListResponse
        unReadNotification = pNotificationListResponse.filter { it?.periodEventIsPeriodNotificationConfirmed == false }
        allButton.setText("All(${pNotificationListResponse.size})")
        unreadButton.setText("Unread(${unReadNotification?.size})")
        if (activity is DashBoardActivity)
            (activity as DashBoardActivity).setNotificationBadgeCount(unReadNotification?.size)
    }

    override fun onStart() {
        super.onStart()
        isStarted = true
        if (isVisibleToUser && isStarted) {
            //api call here
            initViewForLifeCycle()
        } else
            disposeRequestHandler()
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        isVisibleToUser = visible
        if (visible && isStarted) {
            //api call here
            initViewForLifeCycle()
        } else
            disposeRequestHandler()
    }

    private fun disposeRequestHandler() {
        if (activity is DashBoardActivity)
            mDisposable?.dispose()
    }

    override fun onStop() {
        super.onStop()
        isStarted = false
    }

    companion object {
        fun newInstance(selectedScout: ScoutModel? = null): NotificationFragment {
            val fragment = NotificationFragment()
            fragment.scoutItemFromDashboard = selectedScout
            return fragment
        }
    }
}