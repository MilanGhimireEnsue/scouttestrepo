package com.industrialsmart.scoutis.view.dashboard.calendar.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.OccasionItem
import com.industrialsmart.scoutis.utils.extensions.*
import kotlinx.android.synthetic.main.item_event_single_day.view.*

class CalendarEventDayAdapter(private val occasionList: List<OccasionItem?>, private val itemClicked: (OccasionItem?) -> Unit) :
    RecyclerView.Adapter<CalendarEventDayAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_event_single_day, parent, false)
        return MyViewHolder(view, itemClicked)
    }

    override fun getItemCount(): Int {
        return occasionList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(occasionList[position])
    }

    class MyViewHolder(view: View, private val itemClicked: (OccasionItem?) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bind(item: OccasionItem?) {
            itemView.eventTitle.text = item?.name
            if (item?.description?.isNotEmpty() == true)
                itemView.eventDetail.text = item.description
            else
                itemView.eventDetail.hide()
            itemView.eventDateDetail.text = item?.startDateTime?.getFormattedDateTimeFromServerDate()
            itemView.eventItem.setOnClickListener {
                itemClicked.invoke(item)
            }
        }

    }
}