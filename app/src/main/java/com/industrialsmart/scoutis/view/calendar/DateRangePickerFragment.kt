package com.industrialsmart.scoutis.view.calendar


import android.content.res.Configuration
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.children
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.listener.DateRangeListener
import com.industrialsmart.scoutis.utils.extensions.*
import com.industrialsmart.scoutis.utils.warningToast
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import kotlinx.android.synthetic.main.calendar_day_legend.*
import kotlinx.android.synthetic.main.example_4_calendar_day.view.*
import kotlinx.android.synthetic.main.example_4_calendar_header.view.*
import kotlinx.android.synthetic.main.fragment_date_range_picker.*
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.TextStyle
import org.threeten.bp.temporal.ChronoUnit
import java.util.*


class DateRangePickerFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // get the views and attach the listener
        // get the views and attach the listener
        return inflater.inflate(R.layout.fragment_date_range_picker, container, false)
    }

    //last selected startDate and endDate for setting the preselected value
    private var selectedEndDate: String? = null
    private var selectedStartDate: String? = null

    private var dateRangeListener: DateRangeListener? = null

    private val today = LocalDate.now()

    private var startDate: LocalDate? = null
    private var endDate: LocalDate? = null

    private val headerDateFormatter = DateTimeFormatter.ofPattern("MMMM d, yyyy")
    private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    private val startBackground: GradientDrawable by lazy {
        requireContext().getDrawableCompat(R.drawable.example_4_continuous_selected_bg_start) as GradientDrawable
    }

    private val endBackground: GradientDrawable by lazy {
        requireContext().getDrawableCompat(R.drawable.example_4_continuous_selected_bg_end) as GradientDrawable
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //for bottom sheet to take entire width so that date slider inside should not be impacted
        handleConfigChange(resources.configuration)
        if (selectedStartDate != null && selectedEndDate != null) {
            startDate = LocalDate.parse(selectedStartDate, formatter)
            endDate = LocalDate.parse(selectedEndDate, formatter)
        }
        view.viewTreeObserver.addOnGlobalLayoutListener {
            val dialog = dialog as BottomSheetDialog?
            val bottomSheet =
                dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
            val behavior: BottomSheetBehavior<*> =
                BottomSheetBehavior.from<View>(bottomSheet)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        // We set the radius of the continuous selection background drawable dynamically
        // since the view size is `match parent` hence we cannot determine the appropriate
        // radius value which would equal half of the view's size beforehand.
        exFourCalendar.post {
            val radius = ((exFourCalendar.width / 7) / 2).toFloat()
            startBackground.setCornerRadius(topLeft = radius, bottomLeft = radius)
            endBackground.setCornerRadius(topRight = radius, bottomRight = radius)
        }
        // Set the First day of week depending on Locale
        val daysOfWeek = daysOfWeekFromLocale()
        legendLayout.children.forEachIndexed { index, view ->
            (view as TextView).apply {
                text = daysOfWeek[index].getDisplayName(TextStyle.SHORT, Locale.ENGLISH)
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
                setTextColorRes(R.color.colorForeground)
            }
        }
        val currentMonth = YearMonth.now()
        exFourCalendar.setup(currentMonth.minusMonths(120), currentMonth, daysOfWeek.first())
        if (selectedStartDate != null && selectedEndDate != null)
            exFourCalendar.scrollToDate(LocalDate.parse(selectedStartDate, formatter))
        else
            exFourCalendar.scrollToMonth(currentMonth)

        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val textView = view.exFourDayText
            val roundBgView = view.exFourRoundBgView

            init {
                val params = roundBgView.layoutParams
                params.height = (exFourCalendar.height / 6) / 2 + context!!.dpToPixels(8f)
                params.width = (exFourCalendar.height / 6) / 2 + context!!.dpToPixels(8f)
                roundBgView.layoutParams = params
                view.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH && (day.date == today || day.date.isBefore(
                            today
                        ))
                    ) {
                        val date = day.date
                        if (startDate != null) {
                            if (date < startDate || endDate != null) {
                                startDate = date
                                endDate = null
                            } else if (date != startDate) {
                                endDate = date
                            }
                        } else {
                            startDate = date
                        }
                        exFourCalendar.notifyCalendarChanged()
                        bindSummaryViews()
                    }
                }
            }
        }
        exFourCalendar.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.textView
                val roundBgView = container.roundBgView

                textView.text = null
                textView.background = null
                roundBgView.invisible()

                if (day.owner == DayOwner.THIS_MONTH) {
                    textView.text = day.day.toString()

                    if (day.date.isAfter(today)) {
                        textView.setTextColorRes(R.color.colorForegroundLighter)
                    } else {
                        when {
                            startDate == day.date && endDate == null -> {
                                textView.setTextColorRes(R.color.foregroundColorLight)
                                roundBgView.show()
                                roundBgView.setBackgroundResource(R.drawable.example_4_single_selected_bg)
//                                textView.setBackgroundResource(R.drawable.example_4_single_selected_bg)
                            }
                            day.date == startDate -> {
                                textView.setTextColorRes(R.color.foregroundColorLight)
                                //textView.background = startBackground
                                roundBgView.show()
                                roundBgView.setBackgroundResource(R.drawable.example_4_single_selected_bg)
//                                textView.setBackgroundResource(R.drawable.example_4_single_selected_bg)
                            }
                            startDate != null && endDate != null && (day.date > startDate && day.date < endDate) -> {
                                textView.setTextColorRes(R.color.foregroundColorLight)
                                //textView.setBackgroundResource(R.drawable.example_4_continuous_selected_bg_middle)
                                roundBgView.show()
                                roundBgView.setBackgroundResource(R.drawable.example_4_single_selected_bg)
//                                textView.setBackgroundResource(R.drawable.example_4_single_selected_bg)
                            }
                            day.date == endDate -> {
                                textView.setTextColorRes(R.color.foregroundColorLight)
                                //textView.background = endBackground
                                roundBgView.show()
                                roundBgView.setBackgroundResource(R.drawable.example_4_single_selected_bg)
//                                textView.setBackgroundResource(R.drawable.example_4_single_selected_bg)
                            }
                            day.date == today -> {
                                textView.setTextColorRes(R.color.colorPrimary)
                                roundBgView.show()
                                roundBgView.setBackgroundResource(R.drawable.example_4_today_bg)
//                                textView.setBackgroundResource(R.drawable.example_4_today_bg)
                            }
                            else -> {
                                textView.setTextColorRes(R.color.colorForeground)
                            }
                        }
                    }
                } else {

                    // This part is to make the coloured selection background continuous
                    // on the blank in and out dates across various months and also on dates(months)
                    // between the start and end dates if the selection spans across multiple months.

                    val startDate = startDate
                    val endDate = endDate
                    if (startDate != null && endDate != null) {
                        // Mimic selection of inDates that are less than the startDate.
                        // Example: When 26 Feb 2019 is startDate and 5 Mar 2019 is endDate,
                        // this makes the inDates in Mar 2019 for 24 & 25 Feb 2019 look selected.
                        if ((day.owner == DayOwner.PREVIOUS_MONTH
                                    && startDate.monthValue == day.date.monthValue
                                    && endDate.monthValue != day.date.monthValue) ||
                            // Mimic selection of outDates that are greater than the endDate.
                            // Example: When 25 Apr 2019 is startDate and 2 May 2019 is endDate,
                            // this makes the outDates in Apr 2019 for 3 & 4 May 2019 look selected.
                            (day.owner == DayOwner.NEXT_MONTH
                                    && startDate.monthValue != day.date.monthValue
                                    && endDate.monthValue == day.date.monthValue) ||

                            // Mimic selection of in and out dates of intermediate
                            // months if the selection spans across multiple months.
                            (startDate < day.date && endDate > day.date
                                    && startDate.monthValue != day.date.monthValue
                                    && endDate.monthValue != day.date.monthValue)
                        ) {
                        }
                    }
                }
            }
        }

        class MonthViewContainer(view: View) : ViewContainer(view) {
            val textView = view.exFourHeaderText
        }
        exFourCalendar.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                val monthTitle =
                    "${month.yearMonth.month.name.toLowerCase().capitalize()} ${month.year}"
                container.textView.text = monthTitle
            }
        }

        doneButton.setOnClickListener click@{
            val startDate = startDate
            val endDate = endDate
            dateRangeListener?.onDateRangeSelected(
                "${formatter.format(startDate)}T12:00:00.00",
                "${formatter.format(endDate)}T12:00:00.00"
            )
            this?.dismiss()
        }

        cancelButton.setOnClickListener {
            this?.dismiss()
        }

        bindSummaryViews()
    }

    /**
     * Update the dayWidth and dayHeight of calendar view to adjust in both landscape and potrait mode
     */
    private fun updateDayCellViewDimens() {
        exFourCalendar.invalidate()
        exFourCalendar.postDelayed({
            exFourCalendar.dayWidth = exFourCalendar.width / 6
            exFourCalendar.dayHeight = exFourCalendar.height / 6
        }, 200)
    }

    private fun bindSummaryViews() {
        if (startDate != null) {
            exFourStartDateText.text = headerDateFormatter.format(startDate)
            exFourStartDateText.setTextColorRes(R.color.foregroundColorLight)
            exFourStartDateText.setBackgroundResource(R.drawable.bg_button_tab_filled_left)
        } else {
            exFourStartDateText.text = getString(R.string.start_date)
            exFourStartDateText.setTextColorRes(R.color.colorPrimary)
            exFourStartDateText.setBackgroundResource(R.drawable.bg_button_tab_unfilled_left)
        }
        if (endDate != null) {
            exFourEndDateText.text = headerDateFormatter.format(endDate)
            exFourEndDateText.setTextColorRes(R.color.foregroundColorLight)
            exFourEndDateText.setBackgroundResource(R.drawable.bg_button_tab_filled_right)
        } else {
            exFourEndDateText.text = getString(R.string.end_date)
            exFourEndDateText.setTextColorRes(R.color.colorPrimary)
            exFourEndDateText.setBackgroundResource(R.drawable.bg_button_tab_unfilled_right)
        }
        // Enable save button if a range is selected or no date is selected at all, Airbnb style.
        if (startDate != null && endDate != null) {
            if (ChronoUnit.DAYS.between(startDate, endDate) > 60) {
                context?.warningToast(getString(R.string.date_range_exceed_warning))
                startDate = null
                endDate = null
                exFourCalendar.notifyCalendarChanged()
                bindSummaryViews()
                return
            }
            doneButton.isEnabled = true
            doneButton.setTextColor(context!!.getColorCompat(R.color.colorPrimary))
            exFourHeaderDivider.setBackgroundResource(R.color.foregroundColorLight)
            daysCountTextView.show()
            daysCountTextView.text = "${ChronoUnit.DAYS.between(startDate, endDate)} days"
        } else {
            doneButton.isEnabled = false
            doneButton.setTextColor(context!!.getColorCompat(R.color.colorForeground))
            exFourHeaderDivider.setBackgroundResource(R.color.colorPrimary)
            daysCountTextView.invisible()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        handleConfigChange(newConfig)
    }

    private fun handleConfigChange(newConfig: Configuration) {
        updateDayCellViewDimens()
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            val buttonParams = buttonContainer.layoutParams
            buttonParams.height = context!!.dpToPixels(30f)
            buttonContainer.layoutParams = buttonParams

            val dateTextViewParams = dateTextViewContainer.layoutParams as AppBarLayout.LayoutParams
            dateTextViewParams.setMargins(context!!.dpToPixels(10f), 0, context!!.dpToPixels(10f), 0)
            dateTextViewContainer.layoutParams = dateTextViewParams
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            val params = buttonContainer.layoutParams
            params.height = RelativeLayout.LayoutParams.WRAP_CONTENT
            buttonContainer.layoutParams = params

            val dateTextViewParams = dateTextViewContainer.layoutParams as AppBarLayout.LayoutParams
            dateTextViewParams.setMargins(context!!.dpToPixels(10f), context!!.dpToPixels(10f), context!!.dpToPixels(10f), context!!.dpToPixels(10f))
            dateTextViewContainer.layoutParams = dateTextViewParams

        }
    }

    companion object {
        fun newInstance(
            dateRangeListener: DateRangeListener,
            selectedStartDate: String?,
            selectedEndDate: String?
        ): DateRangePickerFragment {
            val fragment = DateRangePickerFragment()
            fragment.dateRangeListener = dateRangeListener
            fragment.selectedStartDate = selectedStartDate?.removeSuffix("T12:00:00.00")
            fragment.selectedEndDate = selectedEndDate?.removeSuffix("T12:00:00.00")
            return fragment
        }
    }
}
