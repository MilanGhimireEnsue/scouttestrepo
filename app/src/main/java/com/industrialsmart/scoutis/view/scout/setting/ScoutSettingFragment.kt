package com.industrialsmart.scoutis.view.scout.setting

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.extensions.updateUserSetting
import com.industrialsmart.scoutis.view.scout.setting.adapter.SensorSettingAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_scout_setting.*

class ScoutSettingFragment : BaseFragment() {
    override val layoutId: Int get() = R.layout.fragment_scout_setting
    private var scoutModel: ScoutModel? = null

    override fun initView() {
        super.initView()
        (activity as ToolbarNavActivity).setToolbarTitle(getString(R.string.scout_settings))
        getUserData()
    }

    private fun getUserData() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it != null)
                        handleUserDataResponse(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleUserDataResponse(userDataResponse: UserDataResponse?) {
        UserInfo.userData = userDataResponse
        updateUserSetting()
        populateData()
    }

    private fun populateData() {
        scoutTitle.text = scoutModel?.title
        scoutIdentifier.text = scoutModel?.code
        val scoutSetting = UserInfo.userData?.userSettingsInfo?.scoutSettings?.find { it.id == scoutModel?.id }
        scoutVisibility.setOnCheckedChangeListener { buttonView, isChecked ->
            val postData = UserInfo.userData
            if (isChecked) {
                (sensorRV.adapter as SensorSettingAdapter?)?.sensorVisibilityEnabled = true
                sensorRV.adapter?.notifyDataSetChanged()
                val item = postData?.userSettingsInfo?.scoutSettings?.find { it.id == scoutModel?.id }
                if (item == null) {
                    postData?.userSettingsInfo?.scoutSettings?.add(ScoutSensorSetting(scoutModel?.id, Value(VISIBLE)))
                } else
                    postData.userSettingsInfo?.scoutSettings?.find { it.id == scoutModel?.id }?.value?.importance = VISIBLE
            } else {
                (sensorRV.adapter as SensorSettingAdapter?)?.sensorVisibilityEnabled = false
                sensorRV.adapter?.notifyDataSetChanged()
                val item = postData?.userSettingsInfo?.scoutSettings?.find { it.id == scoutModel?.id }
                if (item == null) {
                    postData?.userSettingsInfo?.scoutSettings?.add(ScoutSensorSetting(scoutModel?.id, Value(HIDDEN)))
                } else
                    postData.userSettingsInfo?.scoutSettings?.find { it.id == scoutModel?.id }?.value?.importance = HIDDEN
            }
            requestSaveScoutSensorVisibility(postData)
        }
        titleContainer.setOnClickListener {
            if (scoutVisibility.isChecked)
                RenameFragment.newInstance(scoutTitle.text.toString()) {
                    //new name entered
                    if (!scoutTitle.text.toString().equals(it, true) && !it.isNullOrEmpty()) {
                        requestScoutNameChange(it)
                    }

                }.show(childFragmentManager, "SCOUT_TAG")
        }
        if (!scoutModel?.sensors.isNullOrEmpty())
            populateSensors()
        scoutVisibility.isChecked = scoutSetting?.value?.importance != HIDDEN //isChecked should be called after the sensor RV is populated
    }

    private fun populateSensors() {
        sensorRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = SensorSettingAdapter(scoutModel?.sensors!!, { sensor: Sensor?, isChecked: Boolean -> // sensor visibility toggled
                val postData = UserInfo.userData
                val item = postData?.userSettingsInfo?.sensorSettings?.find { it.id == sensor?.id }
                if (isChecked) {
                    if (item == null)
                        postData?.userSettingsInfo?.sensorSettings?.add(ScoutSensorSetting(sensor?.id, Value(VISIBLE)))
                    else
                        postData.userSettingsInfo?.sensorSettings?.find { it.id == sensor?.id }?.value?.importance = VISIBLE
                } else {
                    if (item == null)
                        postData?.userSettingsInfo?.sensorSettings?.add(ScoutSensorSetting(sensor?.id, Value(HIDDEN)))
                    else
                        postData.userSettingsInfo?.sensorSettings?.find { it.id == sensor?.id }?.value?.importance = HIDDEN
                }
                requestSaveScoutSensorVisibility(postData)
            }, { sensor -> //sensor title clicked, open rename dialog
                RenameFragment.newInstance(sensor?.title.toString()) {
                    //new name entered
                    if (!sensor?.title.toString().equals(it, true) && !it.isNullOrEmpty()) {
                        requestSensorNameChange(it, sensor)
                    }
                }.show(childFragmentManager, "SENSOR_TAG")
            })
        }
    }

    /**
     * Request for changing the visibility status of sensor and scout
     */
    private fun requestSaveScoutSensorVisibility(postData: UserDataResponse?) {
        showProgressDialog()
        mDisposable?.add(
            dataSource.postUserData(postData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it != null)
                        UserInfo.userData = it
                    updateUserSetting()
                }, {
                    dismissProgressDialog()
                })
        )
    }

    /**
     * Request for changing the scout name
     */
    private fun requestScoutNameChange(scoutName: String) {
        showProgressDialog()
        val postData = ScoutModel()
        postData.apply {
            id = scoutModel?.id.toString()
            title = scoutName
        }
        mDisposable?.add(
            dataSource.postScoutInfo(listOf(postData))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    scoutTitle.text = scoutName//update the title in the screen in case of success
                }, {
                    dismissProgressDialog()
                })
        )
    }

    /**
     * Request for changing the sensor name
     */
    private fun requestSensorNameChange(it: String, sensor: Sensor?) {
        showProgressDialog()
        val postData = sensor
        postData?.apply {
            title = it
            id = sensor?.id.toString()
        }
        mDisposable?.add(
            dataSource.postSensorInfo(listOf(postData))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    handleSensorRenameSuccess(it, sensor)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleSensorRenameSuccess(newName: String, sensor: Sensor?) {
        val pos = scoutModel?.sensors?.indexOf(sensor)
        if (pos != null) {
            scoutModel?.sensors?.get(pos)?.title = newName
            populateSensors()
        }
    }

    companion object {
        const val VISIBLE = "None"
        const val HIDDEN = "Hide"
        fun newInstance(scoutModel: ScoutModel?): ScoutSettingFragment {
            val fragment = ScoutSettingFragment()
            fragment.scoutModel = scoutModel
            return fragment
        }
    }
}