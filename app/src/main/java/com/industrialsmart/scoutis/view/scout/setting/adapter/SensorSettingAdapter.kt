package com.industrialsmart.scoutis.view.scout.setting.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.Sensor
import com.industrialsmart.scoutis.models.UserInfo
import com.industrialsmart.scoutis.view.scout.setting.ScoutSettingFragment
import kotlinx.android.synthetic.main.item_sensor_setting.view.*

class SensorSettingAdapter(val sensorList: List<Sensor?>, val checkChange: (Sensor?, Boolean) -> Unit, val sensorClicked: (Sensor?) -> Unit) :
    RecyclerView.Adapter<SensorSettingAdapter.MyViewHolder>() {

    var sensorVisibilityEnabled = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sensor_setting, parent, false)
        return MyViewHolder(view, checkChange, sensorClicked)
    }

    override fun getItemCount(): Int {
        return sensorList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(sensorList[position], sensorVisibilityEnabled)
    }

    class MyViewHolder(
        view: View,
        val checkChange: (Sensor?, Boolean) -> Unit,
        val sensorClicked: (Sensor?) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(sensor: Sensor?, sensorVisibilityEnabled: Boolean) {
            itemView.sensorTitle.text = sensor?.title
            itemView.sensorIdentifier.text = sensor?.code
            val sensorSetting = UserInfo.userData?.userSettingsInfo?.sensorSettings?.find { it.id == sensor?.id }
            itemView.sensorVisibility.isChecked = sensorSetting?.value?.importance != ScoutSettingFragment.HIDDEN
            itemView.sensorVisibility.isEnabled = sensorVisibilityEnabled
            if (sensorVisibilityEnabled)
                itemView.textContent.alpha = 1.0f
            else
                itemView.textContent.alpha = 0.4f
            itemView.sensorVisibility.setOnCheckedChangeListener { buttonView, isChecked ->
                checkChange.invoke(sensor, isChecked)
            }
            itemView.textContent.setOnClickListener {
                if (itemView.sensorVisibility.isChecked)
                    sensorClicked.invoke(sensor)
            }
        }
    }
}