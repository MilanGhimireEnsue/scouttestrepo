package com.industrialsmart.scoutis.view.dashboard.calendar

import android.view.View
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseBottomSheetFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.OccasionItem
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.utils.ScreenUtils
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.getFormattedDateFromServerDateForEvent
import com.industrialsmart.scoutis.utils.extensions.getFormattedTimeFromServerDateForEvent
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.successToast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_event_detail.*
import org.jetbrains.anko.startActivity
import org.threeten.bp.LocalDate

class EventDetailFragment : BaseBottomSheetFragment() {

    private lateinit var deleteSuccessCallback: (OccasionItem?) -> Unit
    private var occasionItem: OccasionItem? = null
    private val today = LocalDate.now()

    override val layoutId = R.layout.fragment_event_detail

    override fun initView() {
        super.initView()
        view?.postDelayed(
            {
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<View>(bottomSheet)
                val params = bottomSheet.layoutParams
                params.height = ((ScreenUtils(context!!).height) / 100) * 70 //take about 70 percentage of screen height
                bottomSheet.layoutParams = params
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }, 200
        )
        eventNameTextView.text = occasionItem?.name
        eventDateTextView.text = occasionItem?.eventDay?.getFormattedDateFromServerDateForEvent(occasionItem?.isAllDay)
        dateTimeTextView.text = occasionItem?.eventDay?.getFormattedTimeFromServerDateForEvent()
        if (occasionItem?.description?.isNotEmpty() == true)
            descriptionTextView.text = occasionItem?.description
        else
            eventDescContainer.hide()
        if (occasionItem?.where?.isNotEmpty() == true)
            locationTextView.text = occasionItem?.where
        else
            eventLocationContainer.hide()
        setIconColorAccordingToDate()
        closeButton.setOnClickListener {
            dismiss()
        }
        editButton.setOnClickListener {
            context?.startActivity<ToolbarNavActivity>(
                ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.ADD_EVENT,
                ToolbarNavActivity.EXTRA_PARAM_1 to occasionItem
            )
        }
        deleteButton.setOnClickListener {
            when {
                occasionItem?.eventDay!! < today -> {
                    showInfoDialog(message = "Past event cannot be deleted") {
                    }
                }
                else -> {
                    showDeleteConfirmationDialog()
                }
            }
        }
    }

    private fun showDeleteConfirmationDialog() {
        showDialog(
            message = "Are you sure you want to permanently delete this event?",
            title = "Delete ${occasionItem?.name}",
            positiveText = "Delete",
            negativeText = "Cancel"
        ) {
            requestEventDelete()
        }
    }

    private fun requestEventDelete() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.requestDeleteOccasion(occasionItem?.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    if (it == true) {
                        context?.successToast("Event deleted successfully.")
                        deleteSuccessCallback.invoke(occasionItem)
                        dismiss()
                    } else {
                        showDialog(message = getString(R.string.unable_to_delete_event), positiveText = getString(R.string.ok), negativeText = "") {
                        }
                    }
                }, {
                    dismissProgressDialog()
                })
        )

    }

    private fun setIconColorAccordingToDate() {
        if (occasionItem?.eventDay == null)
            return
        when {
            occasionItem?.eventDay!! < today -> {
                icon.setImageResource(R.drawable.layout_event_past_bg)
                //deleteButton.isEnabled = false
                //deleteButton.setTextColor(ContextCompat.getColor(context!!, R.color.colorForeground))
            }
            occasionItem?.eventDay!! == today -> {
                icon.setImageResource(R.drawable.layout_event_today_bg)
            }
            occasionItem?.eventDay!! > today -> {
                icon.setImageResource(R.drawable.layout_event_future_bg)
            }
        }
    }

    companion object {
        fun newInstance(occasionItem: OccasionItem?, itemClicked: (OccasionItem?) -> Unit): EventDetailFragment {
            val fragment = EventDetailFragment()
            fragment.occasionItem = occasionItem
            fragment.deleteSuccessCallback = itemClicked
            return fragment
        }
    }
}