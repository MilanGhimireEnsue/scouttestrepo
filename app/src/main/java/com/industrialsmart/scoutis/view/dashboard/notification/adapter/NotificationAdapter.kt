package com.industrialsmart.scoutis.view.dashboard.notification.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.NotificationItem
import com.industrialsmart.scoutis.utils.extensions.getFormattedDateTimeFromServerDate
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationAdapter(private val notificationList: List<NotificationItem?>, val itemClick: (NotificationItem?) -> Unit) : RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false)
        return MyViewHolder(view, parent.context, itemClick)
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(notificationList[position])
    }

    class MyViewHolder(
        view: View,
        private val mContext: Context,
        private val itemClick: (NotificationItem?) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(notificationItem: NotificationItem?) {
            if(notificationItem?.periodEventIsPeriodNotificationConfirmed == true)
                itemView.dimOverlay.show()
            else
                itemView.dimOverlay.hide()
            itemView.notificationSubtitle.text = notificationItem?.periodSubtitle
            itemView.notificationTitle.text =
                "${notificationItem?.periodEventDataProviderName} (${notificationItem?.periodEventDataProviderScoutName})"
            itemView.notificationDetail.text = notificationItem?.text
            itemView.notificationDate.text = notificationItem?.periodEventWhen?.getFormattedDateTimeFromServerDate()
            itemView.setOnClickListener {
                itemClick.invoke(notificationItem)
            }
            itemView.circleIcon.imageTintList
            when (notificationItem?.periodEventSeverity) {
                0 -> {
                    itemView.circleIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_0))
                }
                1 -> {
                    itemView.circleIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_1))
                }
                2 -> {
                    itemView.circleIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_2))
                }
                3 -> {
                    itemView.circleIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_3))
                }
                4 -> {
                    itemView.circleIcon.imageTintList = ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.color_severity_4))
                }
            }
        }
    }

}