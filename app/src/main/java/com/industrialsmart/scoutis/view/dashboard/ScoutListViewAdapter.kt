package com.industrialsmart.scoutis.view.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.TextView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.models.Sensor
import kotlin.math.abs

class ScoutListViewAdapter(
    private val mContext: Context,
    private var resourceId: Int,
    private val items: List<ScoutModel?>
) : ArrayAdapter<ScoutModel>(mContext, resourceId, items) {
    private val inflater = LayoutInflater.from(mContext)

    private class ScoutViewHolder {
        var sensorContainer: ViewGroup? = null
        lateinit var title: TextView
        lateinit var code: TextView
        lateinit var dateTime: TextView

        var sensorViewList: ArrayList<SensorViewHolder> = ArrayList()
    }

    private class SensorViewHolder {
        var sensorReadingView: ViewGroup? = null
        var readingContainer: ViewGroup? = null
        lateinit var title: TextView
        lateinit var code: TextView

        var sensorReadingList: ArrayList<SensorReadingViewHolder> = ArrayList()
    }

    private class SensorReadingViewHolder {
        lateinit var title: TextView
        lateinit var value: TextView
    }

    private fun createNecessarySubChild(
        view: View,
        scoutModel: ScoutModel?,
        scoutViewHolder: ScoutViewHolder
    ) {
        if (scoutModel?.sensors == null) {
            scoutModel?.sensors = ArrayList()
        }
        val sensorContainer: LinearLayout
        if (scoutViewHolder.sensorContainer != null) {
            sensorContainer = scoutViewHolder.sensorContainer as LinearLayout
        } else {
            sensorContainer = LinearLayout(mContext)
            sensorContainer.tag = "sensorContainer"
            sensorContainer.layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            sensorContainer.orientation = LinearLayout.VERTICAL
            scoutViewHolder.sensorContainer = sensorContainer
            (view as ViewGroup).addView(sensorContainer)
        }
        var viewsRequired =
            (scoutModel?.sensors as List<Sensor>).size - scoutViewHolder.sensorViewList.size
        if (viewsRequired > 0) {
            for (sensor in 1..abs(viewsRequired)) {
                val sensorViewList =
                    inflater.inflate(R.layout.tpl_dashboard_sensor, null) as ViewGroup
                sensorContainer.addView(sensorViewList)
                val sensorViewHolder = SensorViewHolder()
                sensorViewHolder.sensorReadingView = sensorViewList
                val v = LinearLayout(mContext)
                v.orientation = LinearLayout.HORIZONTAL
                v.layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                sensorViewHolder.readingContainer = v

                sensorViewHolder.title = sensorViewList.findViewById(R.id.sensorLabel)
                sensorViewHolder.code = sensorViewList.findViewById(R.id.sensorIdentifier)
                sensorContainer.addView(v)
                scoutViewHolder.sensorViewList.add(sensorViewHolder)

            }
        } else if (viewsRequired < 0) {
            // remove views
            // TODO (Fix the issue here. Views are not propery updated)
            val container = scoutViewHolder.sensorContainer as ViewGroup
            for (i in container.childCount downTo abs(viewsRequired)) {
                container.removeViewAt(i - 1)
            }
        }

        for ((index, sensor) in (scoutModel?.sensors as List<Sensor>).withIndex()) {

            if (sensor.readings == null) {
                sensor.readings = ArrayList()
            }
            val sensorReadingViewList = scoutViewHolder.sensorViewList[index].sensorReadingList
            viewsRequired = sensor.readings!!.size - sensorReadingViewList.size

            if (viewsRequired > 0) {
                for (reading in 1..abs(viewsRequired)) {
                    val v = inflater.inflate(R.layout.tpl_dashboard_sensor_reading, null)
                    scoutViewHolder.sensorViewList[index].readingContainer?.addView(v)
                    val sensorReadingViewHolder = SensorReadingViewHolder()
                    sensorReadingViewHolder.title = v.findViewById(R.id.sensorReadingLabel)
                    sensorReadingViewHolder.value = v.findViewById(R.id.sensorReadingValue)
                    sensorReadingViewList.add(sensorReadingViewHolder)
                }
            } else if (viewsRequired < 0) {
                val tmp = scoutViewHolder.sensorViewList[index].readingContainer
                if (tmp != null) {
                    for (i in tmp.childCount downTo abs(viewsRequired)) {
                        tmp.removeViewAt(i - 1)
                    }
                }

            }
        }

    }

    override fun getView(position: Int, reusableView: View?, parent: ViewGroup): View {
        val scoutViewHolder: ScoutViewHolder
        var view: View? = reusableView
        val scoutModel = items[position]
        if (view == null) {
            view = inflater.inflate(resourceId, parent, false) as View

            scoutViewHolder = ScoutViewHolder()

            scoutViewHolder.code = view.findViewById(R.id.scoutIdentifier)
            scoutViewHolder.title = view.findViewById(R.id.scoutTitle)
            scoutViewHolder.dateTime = view.findViewById(R.id.scoutDateTime)
            view.tag = scoutViewHolder
        } else {
            scoutViewHolder = view.tag as ScoutViewHolder
        }

        createNecessarySubChild(view, scoutModel, scoutViewHolder)

        scoutViewHolder.title.text = items[position]?.title
        scoutViewHolder.code.text = items[position]?.code
        scoutViewHolder.dateTime.text = items[position]?.dateTime

        // fill the sensors
        if (scoutModel?.sensors != null) {
            for ((i, sensor) in (scoutModel?.sensors as List<Sensor>).withIndex()) {
                scoutViewHolder.sensorViewList[i].title.text = sensor.title
                scoutViewHolder.sensorViewList[i].code.text = sensor.code
                val readings = scoutModel?.sensors!![i].readings
                if (readings != null) {
                    for ((j, reading) in readings.withIndex()) {
                        scoutViewHolder.sensorViewList[i].sensorReadingList[j].title.text =
                            reading.name
                        scoutViewHolder.sensorViewList[i].sensorReadingList[j].value.text =
                            reading.value
                    }
                }

            }
        }

        return view

    }

}