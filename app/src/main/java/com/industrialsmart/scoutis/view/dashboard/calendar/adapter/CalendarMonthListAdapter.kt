package com.industrialsmart.scoutis.view.dashboard.calendar.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.OccasionItem
import kotlinx.android.synthetic.main.item_calendar_list.view.*

class CalendarMonthListAdapter(private val filteredMap: MutableMap<String?, List<OccasionItem?>>, private val itemClicked: (OccasionItem?) -> Unit) :
    RecyclerView.Adapter<CalendarMonthListAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_calendar_list, parent, false)
        return MyViewHolder(view, itemClicked)
    }

    override fun getItemCount(): Int {
        return filteredMap.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val keys = filteredMap.keys.toList()
        holder.bind(keys[position], filteredMap[keys[position]])
    }

    class MyViewHolder(view: View, private val itemClicked: (OccasionItem?) -> Unit) : RecyclerView.ViewHolder(view) {
        fun bind(key: String?, value: List<OccasionItem?>?) {
            itemView.monthYearLabel.text = key
            itemView.eventRV.apply {
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = value?.let { CalendarEventListAdapter(it.asReversed(), itemClicked) }
            }
        }
    }
}