package com.industrialsmart.scoutis.view.scout.setting

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.DialogFragment
import com.industrialsmart.scoutis.R
import kotlinx.android.synthetic.main.fragment_rename.*


class RenameFragment : DialogFragment() {

    private lateinit var callback: (String?) -> Unit
    private var mExistingName: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_rename, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        existingName.setText(mExistingName)
        existingName.requestFocus()
        showKeyboard()
        cancelButton.setOnClickListener {
            dismiss()
        }
        doneButton.setOnClickListener {
            dismiss()
            callback.invoke(existingName.text.toString().trim())
        }
    }

    private fun showKeyboard() {
        val inputMethodManager =
            context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    private fun closeKeyboard() {
        val inputMethodManager =
            context!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        closeKeyboard()
    }


    companion object {
        fun newInstance(existingName: String?, callback: (String?) -> Unit): RenameFragment {
            val fragment = RenameFragment()
            fragment.mExistingName = existingName
            fragment.callback = callback
            return fragment
        }
    }
}