package com.industrialsmart.scoutis.view.dashboard.notification.filter.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.ScoutItemResponse
import kotlinx.android.synthetic.main.item_scout.view.*

class ScoutFilterAdapter(
    private val scoutList: List<ScoutItemResponse?>,
    private val preSelectedScout: List<ScoutItemResponse?>,
    private val itemCheckChange: (ScoutItemResponse?, Boolean) -> Unit
) :
    RecyclerView.Adapter<ScoutFilterAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_scout, parent, false)
        return MyViewHolder(view, itemCheckChange, preSelectedScout)
    }

    override fun getItemCount(): Int {
        return scoutList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(scoutList[position])
    }

    class MyViewHolder(
        view: View,
        private val itemCheckChange: (ScoutItemResponse?, Boolean) -> Unit,
        private val preSelectedScout: List<ScoutItemResponse?>
    ) : RecyclerView.ViewHolder(view) {
        fun bind(item: ScoutItemResponse?) {
            itemView.scoutCheckBox.text = item?.name
            itemView.scoutCode.text = item?.assetName
            itemView.scoutCheckBox.setOnCheckedChangeListener { buttonView, isChecked ->
                item?.let { itemCheckChange.invoke(it, isChecked) }
            }
            itemView.setOnClickListener {
                itemView.scoutCheckBox.isChecked = !itemView.scoutCheckBox.isChecked
            }
            itemView.scoutCheckBox.isChecked = preSelectedScout.contains(item)
        }
    }
}