package com.industrialsmart.scoutis.view.dashboard.calendar

import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.models.OccasionItem
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.getLocalDateFromServerFormat
import com.industrialsmart.scoutis.utils.extensions.inBetweenDateRange
import com.industrialsmart.scoutis.utils.extensions.isDateBetween
import com.industrialsmart.scoutis.view.dashboard.DashBoardActivity
import com.industrialsmart.scoutis.view.dashboard.adapter.ViewPagerAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_calendar_day.*
import org.threeten.bp.LocalDate

class CalendarDayFragment : BaseFragment() {
    private var selectedDate: LocalDate? = null
    private var occasionMap: MutableMap<LocalDate?, List<OccasionItem?>>? = null

    override val layoutId = R.layout.fragment_calendar_day

    override fun initView() {
        super.initView()
        getOccasionList()?.let { filterOccasionListAccordingToDay(it) }
    }

    fun navIconClicked() {
        requireActivity().onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        (requireParentFragment() as CalendarParentFragment).setToolbarIconForOtherView()
        (requireParentFragment() as CalendarParentFragment).setCurrentMonthInToolbar(selectedDate)
    }

    private fun filterOccasionListAccordingToDay(occasionList: List<OccasionItem?>) {
        val modifiedOccasionList: MutableList<OccasionItem?> = ArrayList()
        occasionList.forEach {
            modifiedOccasionList.add(it)
            val inBetweenDateRange = inBetweenDateRange(it?.startDateTime, it?.endDateTime)
            inBetweenDateRange.forEach { eventDate ->
                val modifiedItem = OccasionItem(
                    it?.audienceScope,
                    it?.code,
                    it?.colour,
                    it?.content,
                    it?.customerId,
                    it?.description,
                    it?.duration,
                    it?.id,
                    it?.isAllDay,
                    it?.isPrivate,
                    it?.name,
                    it?.occasionTypeId,
                    it?.ownerId,
                    it?.priority,
                    it?.regionId,
                    it?.startDateTime,
                    it?.where,
                    it?.endDateTime,
                    eventDate
                )
                modifiedOccasionList.add(modifiedItem)
            }
        }
        val occasionListMap: MutableMap<LocalDate?, List<OccasionItem?>> = HashMap()
        modifiedOccasionList?.forEach {
            var date: LocalDate? = null
            if (it?.eventDay == null) {
                date = it?.startDateTime.getLocalDateFromServerFormat()
                it?.eventDay = date
            } else {
                date = it.eventDay
            }
            val eventListForDate: MutableList<OccasionItem?> = ArrayList()
            occasionList?.forEach { occasionItem ->
                if (date?.isDateBetween(occasionItem?.startDateTime, occasionItem?.endDateTime) == true) {
                    eventListForDate.add(occasionItem)
                }
            }
            if (occasionListMap.containsKey(date)) {
                val modifiedList: MutableList<OccasionItem?>? = occasionListMap[date]?.toMutableList()
                modifiedList?.addAll(eventListForDate)
                occasionListMap[date] = modifiedList?.distinct()!!.toList()
            } else {
                occasionListMap[date] = eventListForDate.distinct()
            }
        }
        occasionMap = occasionListMap
        initViewPager()
    }

    private fun initViewPager() {
        val pagerAdapter = ViewPagerAdapter(childFragmentManager, context!!)
        occasionMap = occasionMap?.toSortedMap(compareBy { it })
        val keys = occasionMap?.keys
        keys?.forEach {
            pagerAdapter.addFragment(CalendarDayItemFragment.newInstance(it, occasionMap?.get(it)))
        }
        calendarDayEventViewPager.adapter = pagerAdapter
        if (currentPosition != 0 && !keys.isNullOrEmpty()) {
            calendarDayEventViewPager.currentItem = currentPosition
        } else {
            val toBeSelectedIndex = keys?.indexOf(selectedDate)
            if (toBeSelectedIndex != null || toBeSelectedIndex == -1) {
                calendarDayEventViewPager.currentItem = toBeSelectedIndex
                currentPosition = toBeSelectedIndex
            }
        }
        calendarDayEventViewPager.addOnPageChangeListener(onPageChangeListener)
    }

    var currentPosition = 0
    private val onPageChangeListener = object : ViewPager.OnPageChangeListener {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            currentPosition = position
            val localDate = occasionMap?.keys?.toList()?.get(position)
            (requireParentFragment() as CalendarParentFragment).setCurrentMonthInToolbar(localDate)
        }

        override fun onPageScrollStateChanged(state: Int) {
        }
    }


    private fun setOccasionList(occasionList: List<OccasionItem?>) {
        (activity as DashBoardActivity).setOccasionList(occasionList)
    }

    private fun getOccasionList(): List<OccasionItem?>? {
        return (activity as DashBoardActivity).getOccasionList()
    }

    fun getFreshData() {
        requestOccasionList()
    }

    private fun requestOccasionList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getOccasionList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    setOccasionList(it)
                    filterOccasionListAccordingToDay(it)
                }, {
                    dismissProgressDialog()
                })
        )
    }

    companion object {
        fun newInstance(selectedDate: LocalDate?): CalendarDayFragment {
            val fragment = CalendarDayFragment()
            fragment.selectedDate = selectedDate
            return fragment
        }
    }
}