package com.industrialsmart.scoutis.view.dashboard.calendar

import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.OccasionItem
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.utils.extensions.getFormattedTimeFromServerDateForEvent
import com.industrialsmart.scoutis.utils.extensions.getLocalDateFromServerFormat
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.utils.warningToast
import com.industrialsmart.scoutis.view.dashboard.DashBoardActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_calendar_parent.*
import org.jetbrains.anko.startActivity
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth

class CalendarParentFragment : BaseFragment() {
    override val layoutId: Int
        get() = R.layout.fragment_calendar_parent

    //for lifecycle
    private var isVisibleToUser: Boolean = false
    private var isStarted = false

    private fun initViewForLifeCycle() {
        addEventButton.setOnClickListener {
            context?.startActivity<ToolbarNavActivity>(ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.ADD_EVENT)
        }
        navIcon.setOnClickListener {
            val fragment = childFragmentManager.findFragmentById(R.id.container)
            if (fragment is CalendarFragment)
                fragment.navIconClicked()
            else if (fragment is CalendarListFragment)
                fragment.navIconClicked()
            else if (fragment is CalendarDayFragment)
                fragment.navIconClicked()
        }
        todayDate.setOnClickListener {
            val fragment = childFragmentManager.findFragmentById(R.id.container)
            if (fragment is CalendarFragment) {
                fragment.todayButtonClicked()
            }
        }
        mDisposable = CompositeDisposable()
        requestOccasionList()
    }

    private fun requestOccasionList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getOccasionList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    dismissProgressDialog()
                    setOccasionList(it)
                    changeChildFragment(CalendarFragment.newInstance())
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun setOccasionList(occasionList: List<OccasionItem?>) {
        occasionList.forEach {
            val date = it?.startDateTime.getLocalDateFromServerFormat()
            it?.eventDay = date
        }
        (activity as DashBoardActivity).setOccasionList(occasionList)
    }

    fun setToolbarIconForCalendar() {
        navIcon.setImageResource(R.drawable.ic_list_ul)
        todayDate.show()
        todayDate.text = "TODAY"
        todayDate.isClickable = true
    }

    fun setCurrentMonthInToolbar(selectedDate: LocalDate?) {
        todayDate.show()
        todayDate.text = selectedDate?.getFormattedTimeFromServerDateForEvent()
        todayDate.isClickable = false
    }

    fun setToolbarIconForOtherView() {
        navIcon.setImageResource(R.drawable.ic_back)
        todayDate.hide()
    }

    fun openCalendarListFragment() {
        changeChildFragment(CalendarListFragment.newInstance())
    }

    fun openCalendarDayFragment(date: LocalDate) {
        changeChildFragment(CalendarDayFragment.newInstance(date))
    }

    override fun onStart() {
        super.onStart()
        isStarted = true
        if (isVisibleToUser && isStarted) {
            //api call here
            initViewForLifeCycle()
        } else
            mDisposable?.dispose()
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        isVisibleToUser = visible
        if (visible && isStarted) {
            //api call here
            initViewForLifeCycle()
        } else
            mDisposable?.dispose()
    }

    override fun onStop() {
        super.onStop()
        isStarted = false
    }

    companion object {
        fun newInstance(): CalendarParentFragment {
            val fragment = CalendarParentFragment()
            return fragment
        }
    }
}