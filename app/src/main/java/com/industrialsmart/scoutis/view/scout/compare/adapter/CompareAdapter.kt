package com.industrialsmart.scoutis.view.scout.compare.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.models.CompareSensorData
import com.industrialsmart.scoutis.models.ToBeCompared
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.invisible
import com.industrialsmart.scoutis.utils.extensions.show
import kotlinx.android.synthetic.main.item_compare.view.*

class CompareAdapter(private var compareSensorDataList: List<CompareSensorData?>, val itemCheckChange: (ToBeCompared, Boolean) -> Unit) :
    RecyclerView.Adapter<CompareAdapter.MyViewHolder>() {

    lateinit var mRecyclerView: RecyclerView

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_compare, parent, false)
        return MyViewHolder(view, parent.context, viewPool, itemCheckChange)
    }

    override fun getItemCount(): Int {
        return compareSensorDataList.size
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.mRecyclerView = recyclerView
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(compareSensorDataList[position], position) { position, hiddenStatus ->
            compareSensorDataList[position]?.isHidden = hiddenStatus
            mRecyclerView.post {
                notifyDataSetChanged()
            }
        }
    }

    class MyViewHolder(
        view: View,
        private val mContext: Context,
        private val viewPool: RecyclerView.RecycledViewPool,
        private val itemCheckChange: (ToBeCompared, Boolean) -> Unit
    ) : RecyclerView.ViewHolder(view) {
        fun bind(compareSensorData: CompareSensorData?, position: Int, isHiddenCallBack: (Int, Boolean) -> Unit) {
            itemView.compareName.text =
                "${compareSensorData?.compareType} (${compareSensorData?.comparableSensorList?.size})"
            if (compareSensorData?.comparableSensorList?.isNotEmpty() == true)
                itemView.sensorListRV.apply {
                    layoutManager = LinearLayoutManager(mContext, RecyclerView.VERTICAL, false)
                    adapter = SensorCompareAdapter(compareSensorData.comparableSensorList, itemCheckChange)
                    setRecycledViewPool(viewPool)
                    isNestedScrollingEnabled = false
                }
            if (compareSensorData?.isHidden == true) {
                itemView.sensorListRV.hide()
                itemView.compareName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_right, 0, 0, 0)
            } else {
                itemView.sensorListRV.show()
                itemView.compareName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_arrow_down, 0, 0, 0)
            }
            itemView.compareName.setOnClickListener {
                if (compareSensorData?.isHidden == true)
                    isHiddenCallBack.invoke(position, false)
                else
                    isHiddenCallBack.invoke(position, true)
            }
        }
    }
}