package com.industrialsmart.scoutis.view.gdu

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.listener.EditDeleteListener
import com.industrialsmart.scoutis.models.GDUDataItem
import kotlinx.android.synthetic.main.item_gdu.view.*

class GduAdapter(private val gduList: List<GDUDataItem?>, private val gduClickListener: EditDeleteListener?) :
    RecyclerSwipeAdapter<GduAdapter.MyViewHolder>() {

    lateinit var mRecyclerView: RecyclerView

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_gdu, parent, false)
        return MyViewHolder(view, gduClickListener, mItemManger)
    }

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipeLayout
    }

    override fun getItemCount(): Int {
        return gduList.size
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        this.mRecyclerView = recyclerView
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(gduList[position])
        mItemManger.bindView(holder.itemView, position)
    }

    class MyViewHolder(
        view: View,
        private val gduClickListener: EditDeleteListener?,
        val mItemManger: SwipeItemRecyclerMangerImpl
    ) : RecyclerView.ViewHolder(view) {
        fun bind(gduItem: GDUDataItem?) {
            itemView.gduTitle.text = gduItem?.name
            itemView.gduCode.text = gduItem?.rawScoutData?.name
            itemView.gduId.text = gduItem?.rawScoutData?.assetName
            itemView.editButton.setOnClickListener {
                itemView.swipeLayout.close()
                gduClickListener?.onEdit(gduItem)
            }
            itemView.dragItem.setOnClickListener {
                gduClickListener?.onClicked(gduItem)
            }
            itemView.deleteButton.setOnClickListener {
                itemView.swipeLayout.close()
                gduClickListener?.onDelete(gduItem)
            }
            itemView.swipeLayout.addSwipeListener(object : SimpleSwipeListener() {
                override fun onStartOpen(layout: SwipeLayout) {
                    mItemManger.closeAllExcept(layout)
                }
            })
        }
    }
}