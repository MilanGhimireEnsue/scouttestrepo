package com.industrialsmart.scoutis.view.dashboard.notification.filter

import android.view.View
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseBottomSheetFragment
import com.industrialsmart.scoutis.models.ScoutItemResponse
import com.industrialsmart.scoutis.utils.ScreenUtils
import com.industrialsmart.scoutis.view.dashboard.notification.filter.adapter.ScoutFilterAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_scout_filter.*

class ScoutFilterFragment : BaseBottomSheetFragment() {

    override val layoutId = R.layout.fragment_scout_filter

    private lateinit var itemClicked: (List<ScoutItemResponse?>) -> Unit
    private var preCheckedScoutList: List<ScoutItemResponse?>? = null

    private val checkedScoutList: MutableList<ScoutItemResponse?> = ArrayList()

    override fun initView() {
        super.initView()
        view?.postDelayed(
            {
                val dialog = dialog as BottomSheetDialog?
                val bottomSheet =
                    dialog!!.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                val behavior: BottomSheetBehavior<*> =
                    BottomSheetBehavior.from<View>(bottomSheet)
                val params = bottomSheet.layoutParams
                params.height = ((ScreenUtils(context!!).height) / 100) * 70 //take about 70 percentage of screen height
                bottomSheet.layoutParams = params
                behavior.state = BottomSheetBehavior.STATE_EXPANDED
            }, 200
        )
        requestScoutList()
        doneButton.setOnClickListener {
            itemClicked.invoke(checkedScoutList)
            dismiss()
        }
        cancelButton.setOnClickListener {
            dismiss()
        }
    }

    private fun requestScoutList() {
        mDisposable?.add(
            dataSource.getRawScoutList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    it?.let { handleScoutList(it) }
                }, {

                })
        )
    }

    private fun handleScoutList(list: List<ScoutItemResponse?>) {
        scoutRV?.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = ScoutFilterAdapter(list, preCheckedScoutList!!) { scoutItem, isChecked ->
                if (isChecked)
                    checkedScoutList.add(scoutItem)
                else
                    checkedScoutList.remove(scoutItem)
            }
        }
    }

    companion object {
        fun newInstance(preSelectedScoutList: List<ScoutItemResponse?>, itemClicked: (List<ScoutItemResponse?>) -> Unit): ScoutFilterFragment {
            val fragment = ScoutFilterFragment()
            fragment.itemClicked = itemClicked
            fragment.preCheckedScoutList = preSelectedScoutList
            return fragment
        }
    }
}




