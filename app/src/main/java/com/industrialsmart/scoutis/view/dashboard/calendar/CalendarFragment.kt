package com.industrialsmart.scoutis.view.dashboard.calendar

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.children
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.OccasionItem
import com.industrialsmart.scoutis.models.ViewType
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.*
import com.industrialsmart.scoutis.utils.extensions.setTextColorRes
import com.industrialsmart.scoutis.utils.extensions.isDateBetween
import com.industrialsmart.scoutis.utils.warningToast
import com.industrialsmart.scoutis.view.dashboard.DashBoardActivity
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.calendar_day_event.view.*
import kotlinx.android.synthetic.main.calendar_day_legend.*
import kotlinx.android.synthetic.main.example_4_calendar_header.view.*
import kotlinx.android.synthetic.main.fragment_calendar.*
import org.jetbrains.anko.startActivity
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import org.threeten.bp.format.TextStyle
import java.util.*
import kotlin.collections.ArrayList

class CalendarFragment : CalendarBaseFragment() {
    override val layoutId = R.layout.fragment_calendar

    private val today = LocalDate.now()

    override fun initView() {
        super.initView()
        updateDayCellViewDimens()
        handleOccasionResponse()
    }

    override fun onResume() {
        super.onResume()
        (requireParentFragment() as CalendarParentFragment).setToolbarIconForCalendar()
    }

    fun navIconClicked() {
        if (getOccasionList() == null)
            context?.warningToast("Please wait...")
        else
            (requireParentFragment() as CalendarParentFragment).openCalendarListFragment()
    }

    fun todayButtonClicked() {
        val currentMonth = YearMonth.now()
        exFourCalendar.scrollToMonth(currentMonth)
    }


    private fun requestOccasionList() {
        showProgressDialog()
        mDisposable?.add(
            dataSource.getOccasionList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    setOccasionList(it)
                    dismissProgressDialog()
                    handleOccasionResponse()
                }, {
                    dismissProgressDialog()
                })
        )
    }

    private fun handleOccasionResponse() {
        populateCalendar(getOccasionList())
    }

    private fun populateCalendar(occasionList: List<OccasionItem?>?) {
        // Set the First day of week depending on Locale
        val daysOfWeek = daysOfWeekFromLocale()
        legendLayout.children.forEachIndexed { index, view ->
            (view as TextView).apply {
                text = daysOfWeek[index].getDisplayName(TextStyle.SHORT, Locale.ENGLISH)
                setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
                setTextColorRes(R.color.colorPrimary)
            }
        }
        val currentMonth = YearMonth.now()
        exFourCalendar.setup(currentMonth.minusMonths(5 * 12), currentMonth.plusMonths(5 * 12), daysOfWeek.first())
        exFourCalendar.scrollToMonth(currentMonth)
        class DayViewContainer(view: View) : ViewContainer(view) {
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val textView = view.exFourDayText
            val roundBgView = view.exFourRoundBgView
            val eventContainer = view.eventView

            init {
                view.setOnClickListener {
                    val dayEventList: MutableList<OccasionItem?> = ArrayList()
                    dayEventList.clear()
                    //make the list of events for particular day
                    occasionList?.forEach {
                        if (day.date.isDateBetween(it?.startDateTime, it?.endDateTime))
                            dayEventList.add(it)
                    }
                    if (dayEventList.size > 0) {
                        openDayView(day.date)
                    }
                }
                view.setOnLongClickListener {
                    context?.startActivity<ToolbarNavActivity>(
                        ToolbarNavActivity.VIEW_TYPE_INTENT to ViewType.ADD_EVENT,
                        ToolbarNavActivity.EXTRA_DATE_DATA to day.date
                    )
                    return@setOnLongClickListener true
                }
            }
        }

        exFourCalendar.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.textView
                val roundBgView = container.roundBgView
                val eventContainer = container.eventContainer

                textView.text = null
                textView.background = null
                roundBgView.invisible()
                eventContainer.removeAllViews()

                if (day.owner == DayOwner.THIS_MONTH) {
                    textView.text = day.day.toString()
                    val dayEventList: MutableList<OccasionItem?> = ArrayList()
                    dayEventList.clear()
                    //make the list of events for particular day
                    occasionList?.forEach {
                        if (day.date.isDateBetween(it?.startDateTime, it?.endDateTime))
                            dayEventList.add(it)
                    }
                    //change color of the today's date
                    if (day.date == today) {
                        textView.setTextColorRes(R.color.foregroundColorLight)
                        textView.background = ContextCompat.getDrawable(context!!, R.drawable.icon_circle)
                    } else {
                        textView.setTextColorRes(R.color.colorForeground)
                    }
                    //populate the event for particular day
                    var eventShowCount = 2
                    eventContainer.removeAllViews()
                    dayEventList.forEachIndexed lit@{ index, it ->//using lit@ for breaking the for loop
                        if (index < eventShowCount) {
                            val view = LayoutInflater.from(context).inflate(R.layout.item_event, rootView, false)
                            val eventTextView = view.findViewById<TextView>(R.id.eventName)
                            eventTextView.text = it?.name
                            when {
                                day.date < today -> {
                                    eventTextView.setBackgroundResource(R.drawable.layout_event_past_bg)
                                    eventTextView.setTextColorRes(R.color.colorForeground)
                                }
                                day.date == today -> {
                                    eventTextView.setBackgroundResource(R.drawable.layout_event_today_bg)
                                    eventTextView.setTextColorRes(R.color.foregroundColorLight)
                                }
                                day.date > today -> {
                                    eventTextView.setBackgroundResource(R.drawable.layout_event_future_bg)
                                    eventTextView.setTextColorRes(R.color.foregroundColorLight)
                                }
                            }
                            eventTextView.setOnClickListener { view ->
                                EventDetailFragment.newInstance(it) {
                                    requestOccasionList()
                                }.show(childFragmentManager, "DETAIL_TAG")
                            }
                            eventContainer.addView(view)
                            if (index == 0) {
                                if (dayEventList.size > eventShowCount)
                                    eventShowCount-- //decreasing the item to be shown as we need to show {+n more}
                                /*//dynamically calculating the number of event to be shown initially
                                if(day.date == today){
                                    d("okhttp dimen 1 ${eventContainer.getMeasurments().second} : ${view.getMeasurments().second}")
                                    d("okhttp dimen 2 ${eventShowCount}")
                                }
                                eventShowCount = (eventContainer.getMeasurments().second / view.getMeasurments().second)
                                if (dayEventList.size > eventShowCount)
                                    eventShowCount-- //decreasing the item to be shown as we need to show {+n more}
                                if(day.date == today){
                                    d("okhttp dimen 3 ${eventShowCount}")
                                }*/
                            }
                        } else {
                            return@lit
                        }
                    }
                    //if the day have event's that cannot be shown, just indicating them with
                    if (eventShowCount < dayEventList.size) {
                        val view = LayoutInflater.from(context).inflate(R.layout.item_event, rootView, false)
                        val eventTextView = view.findViewById<TextView>(R.id.eventName)
                        eventTextView.setBackgroundResource(R.drawable.layout_more_event_bg)
                        eventTextView.text = "+${dayEventList.size - eventShowCount} more"
                        eventContainer.addView(view)
                    }
                }
            }
        }

        class MonthViewContainer(view: View) : ViewContainer(view) {
            val textView = view.exFourHeaderText
        }
        exFourCalendar.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                val monthTitle =
                    "${month.yearMonth.month.name.toLowerCase().capitalize()} ${month.year}"
                container.textView.text = monthTitle
            }
        }
    }

    fun openDayView(date: LocalDate) {
        (requireParentFragment() as CalendarParentFragment).openCalendarDayFragment(date)
    }

    private fun updateDayCellViewDimens() {
        exFourCalendar.invisible()
        calendarViewProgress.show()
        exFourCalendar.invalidate()
        exFourCalendar.post {
            exFourCalendar.dayHeight = exFourCalendar.height / 6
            calendarViewProgress.hide()
            exFourCalendar.show()
        }
    }

    private fun setOccasionList(occasionList: List<OccasionItem?>) {
        occasionList.forEach {
            val date = it?.startDateTime.getLocalDateFromServerFormat()
            it?.eventDay = date
        }
        (activity as DashBoardActivity).setOccasionList(occasionList)
    }

    private fun getOccasionList(): List<OccasionItem?>? {
        return (activity as DashBoardActivity).getOccasionList()
    }

    companion object {
        fun newInstance(): CalendarFragment {
            val fragment = CalendarFragment()
            return fragment
        }
    }
}