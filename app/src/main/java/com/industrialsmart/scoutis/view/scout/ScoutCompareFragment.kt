package com.industrialsmart.scoutis.view.scout

import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.listener.CompareItemListener
import com.industrialsmart.scoutis.listener.DateRangeListener
import com.industrialsmart.scoutis.models.GetSensorDataRequest
import com.industrialsmart.scoutis.models.ReadingDataItem
import com.industrialsmart.scoutis.models.ToBeCompared
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.*
import com.industrialsmart.scoutis.utils.view.XAxisValueFormatter
import com.industrialsmart.scoutis.view.calendar.DateRangePickerFragment
import com.industrialsmart.scoutis.view.scout.compare.CompareFragment
import com.industrialsmart.scoutis.view.scout.compare.adapter.ComparedItemAdapter
import io.github.hyuwah.draggableviewlib.Draggable
import io.github.hyuwah.draggableviewlib.makeDraggable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_scout_compare.*
import kotlin.math.abs


class ScoutCompareFragment : BaseFragment() {
    override val layoutId = R.layout.fragment_scout_compare

    private var toComparedList: MutableList<ToBeCompared?>? = null

    private var fetchedList = 0 //to store the count of fetched item

    //currently selected startDate and endDate
    private var selectedStartDate: String? = null
    private var selectedEndDate: String? = null

    //to persist all the readings for comparable sensors
    private var readingResponseMap: MutableMap<ToBeCompared?, List<ReadingDataItem?>> = HashMap()

    override fun initView() {
        super.initView()
        setClickListener()
        setSensorData()
        draggableView.makeDraggable(Draggable.STICKY.AXIS_X, false) // set sticky axis to x & animation to false
        draggableView.makeDraggable(Draggable.STICKY.AXIS_XY) // set sticky axis to xy
        draggableView.makeDraggable() // all default
    }

    /**
     * Handle click for week/month/custom date
     */
    private fun setClickListener() {
        lastWeekButton.setOnClickListener {
            (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.WEEKLY)
            changeButtonStyle(lastWeekButton)
            requestSensorData(getLastWeekDate(), getCurrentDate())
            //make the start date and end date null
            selectedStartDate = null
            selectedEndDate = null
        }
        lastMonthButton.setOnClickListener {
            (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.MONTHLY)
            changeButtonStyle(lastMonthButton)
            requestSensorData(getLastMonthDate(), getCurrentDate())
            //make the start date and end date null
            selectedStartDate = null
            selectedEndDate = null
        }
        customDateButtom.setOnClickListener {
            val dateRangePickerFragment =
                DateRangePickerFragment.newInstance(object : DateRangeListener {
                    override fun onDateRangeSelected(startDate: String?, endDate: String?) {
                        (activity as ScoutActivity).setChartDataDate(ScoutActivity.Companion.CHART_DATA_DATE.CUSTOM_RANGE)
                        (activity as ScoutActivity).setSelectedStartEndDate(startDate, endDate)
                        changeButtonStyle(customDateButtom)
                        requestSensorData(startDate, endDate)
                        //set the value of startDate and endDate
                        selectedStartDate = startDate
                        selectedEndDate = endDate
                    }
                }, selectedStartDate, selectedEndDate)
            dateRangePickerFragment.show(childFragmentManager, TAG_DATE_RANGE)
        }
        compareButton.setOnClickListener {
            val comparableSensorList = (activity as ScoutActivity).getComparableListOfSensor()
            if (!comparableSensorList.isNullOrEmpty()) {
                val compareFragment = toComparedList?.let { it1 ->
                    CompareFragment.newInstance(comparableSensorList, it1, object : CompareItemListener {
                        override fun onCompareItemClicked(toComparedList: List<ToBeCompared?>?) {
                            if (toComparedList?.isNullOrEmpty() == false) {
                                if (toComparedList.size > 1) {
                                    //stay in scout compare screen
                                    this@ScoutCompareFragment.toComparedList = toComparedList.setRandomColors().toMutableList()
                                    setSensorData()
                                } else {
                                    //navigate to particular sensor and readingType as per user selection
                                    fragmentManager?.popBackStack()
                                    (activity as ScoutActivity).switchSensorAndReading(toComparedList.first())
                                }
                            } else
                                fragmentManager?.popBackStack()
                        }
                    })
                }
                compareFragment?.show(childFragmentManager, TAG_COMPARE_READING)
            }
        }
    }

    /**
     * Change button style for currently selected week/month/custom date
     */
    private fun changeButtonStyle(clickedButton: Button) {
        val viewList = mutableListOf(lastWeekButton, lastMonthButton, customDateButtom)
        viewList.remove(clickedButton)
        viewList.forEach {
            it?.apply {
                setTextColor(ContextCompat.getColor(context!!, R.color.colorForegroundDim))
                setBackgroundResource(android.R.color.transparent)
            }
        }
        clickedButton.apply {
            setTextColor(ContextCompat.getColor(context!!, R.color.foregroundColorLight))
            setBackgroundResource(R.drawable.bg_button_rounded)
        }
    }

    private fun setSensorData() {
        comparedReadingRV.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = toComparedList?.let {
                ComparedItemAdapter(it) {
                    //remove icon clicked
                    toComparedList?.remove(it)
                    adapter?.notifyDataSetChanged()
                    removeItemFromChart(it)
                }
            }
        }
        setChartAccordingToChartDataType()
    }

    /**
     * Remove particular item from comparable list and handle all the possible scenario
     */
    private fun removeItemFromChart(it: ToBeCompared?) {
        readingResponseMap.remove(it)
        //if only 1 entity is remaining after removal, then exit from compare and  redirect particular sensor/reading page
        if (toComparedList?.size == 1) {
            fragmentManager?.popBackStack()
            (activity as ScoutActivity).switchSensorAndReading(toComparedList?.first())
        } else
            populateChart(readingResponseMap)
    }

    /**
     * Set chart selection according to selection type depending upon other sensors
     */
    private fun setChartAccordingToChartDataType() {
        when ((activity as ScoutActivity).getChartDataDate()) {
            ScoutActivity.Companion.CHART_DATA_DATE.WEEKLY -> {
                lastWeekButton.callOnClick()
            }
            ScoutActivity.Companion.CHART_DATA_DATE.MONTHLY -> {
                lastMonthButton.callOnClick()
            }
            ScoutActivity.Companion.CHART_DATA_DATE.CUSTOM_RANGE -> {
                selectedStartDate = (activity as ScoutActivity).getSelectedStartDate()
                selectedEndDate = (activity as ScoutActivity).getSelectedEndDate()
                changeButtonStyle(customDateButtom)
                requestSensorData(selectedStartDate, selectedEndDate)
            }
        }
    }

    private fun requestSensorData(selectedStartDate: String?, selectedEndDate: String?) {
        fetchedList = 0
        readingResponseMap.clear()
        toComparedList?.forEachIndexed { index, toBeCompared ->
            requestSingleSensorData(selectedStartDate, selectedEndDate, toBeCompared)
        }
    }

    private fun requestSingleSensorData(
        startDate: String?,
        endDate: String?,
        toBeCompared: ToBeCompared?
    ) {
        chartLoading.show()
        val postData = GetSensorDataRequest(
            startDate,
            endDate,
            toBeCompared?.comparableReadingType?.readingTypeId,
            toBeCompared?.comparableReadingType?.categoryCode,
            toBeCompared?.sensorId
        )
        mDisposable?.add(
            dataSource.getSensorData(postData).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    fetchedList++
                    handleRequestSensorData(it, toBeCompared)
                }, {
                    fetchedList++
                })
        )
    }

    private fun handleRequestSensorData(
        it: List<ReadingDataItem?>,
        readingTypeItem: ToBeCompared?
    ) {
        if (!it.isNullOrEmpty()) {
            readingResponseMap[readingTypeItem] = it
        }
        if (fetchedList >= toComparedList?.size!!) {
            chartLoading.hide()
            if (readingResponseMap.isNotEmpty())
                populateChart(readingResponseMap)
        }
    }

    private fun populateChart(
        map: MutableMap<ToBeCompared?, List<ReadingDataItem?>>
    ) {
        draggableView?.removeAllViews()
        chartView.clear()
        if (map.isNullOrEmpty()) {
            chartView.setNoDataText(getString(R.string.no_chart_data))
            chartView.setNoDataTextColor(context.getColorCompat(R.color.colorPrimary))
            unit1TextView.text = ""
            unit2TextView.text = ""
            return
        }
        val distinctCategoryCode = map.keys.distinctBy { it?.comparableReadingType?.categoryCode }
        unit1TextView.text = distinctCategoryCode?.first()?.comparableReadingType?.categoryCode?.addDegreeIfTempUnit()
        if (distinctCategoryCode.size == 1)
            unit2TextView.text = distinctCategoryCode?.first()?.comparableReadingType?.categoryCode?.addDegreeIfTempUnit()
        else
            unit2TextView.text = distinctCategoryCode[1]?.comparableReadingType?.categoryCode?.addDegreeIfTempUnit()
        val dataSets: MutableList<LineDataSet?> = ArrayList()
        map.forEach {
            val chartData = getLineData(it.value, it.key?.foreGroundTint)
            chartData?.setDrawValues(false)
            chartData?.mode = LineDataSet.Mode.CUBIC_BEZIER
            chartData?.setDrawCircles(false)
            chartData?.color = context.getColorCompat(it.key?.foreGroundTint!!)
            //to set different axis dependency for different categoryCode (C,%)
            if (it.key?.comparableReadingType?.categoryCode == distinctCategoryCode.first()?.comparableReadingType?.categoryCode)
                chartData?.axisDependency = YAxis.AxisDependency.LEFT
            else
                chartData?.axisDependency = YAxis.AxisDependency.RIGHT
            dataSets.add(chartData)
        }
        chartView.setDrawMarkers(false)
        chartView.data = LineData(dataSets.toList())
        chartView?.legend?.isEnabled = false
        chartView?.description?.isEnabled = false
        chartView.xAxis.labelCount = 4
        chartView.isDoubleTapToZoomEnabled = false
        chartView.xAxis.valueFormatter = XAxisValueFormatter()
//        chartView.maxHighlightDistance = 1000f
        //setting x-axis data range to bottom
        val xAxis = chartView.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        //hiding the  y-axis on right
        if (distinctCategoryCode.size < 2) {
            //only single type of category code is present, so hide the right Yaxis
            val rightYAxis = chartView.axisRight
            rightYAxis.isEnabled = false
        }
        chartView.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onNothingSelected() {
            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {
                val highlight: ArrayList<Highlight?> = ArrayList()
                for (j in 0 until chartView.data.dataSets.size) {
                    var highLightMatched = false
                    val iDataSet = chartView.data.dataSets[j]
                    for (i in (iDataSet as LineDataSet).values.indices) {
                        if ((iDataSet as LineDataSet).values[i].x == e?.x) {
                            highlight.add(Highlight(e.x, e.y, j))
                            highLightMatched = true
                        }
                    }
                    if (!highLightMatched) {
                        val entryList = (iDataSet as LineDataSet).values
                        val xList = entryList?.map { it?.x }
                        e?.x?.let {
                            val closestValue = xList.closestValue(it)
                            highlight.add(Highlight(closestValue!!, e.y, j))
                        }
                    }
                }
                setHighLightedValues(highlight)
//                chartView.highlightValues(highlight.toTypedArray())
//                chartView.setDrawMarkers(true)
            }
        })

        //setting custom marker when tapped
//        val compareDataList = map.keys.toList()
//        val marker =
//            CompareMarkerView(context, R.layout.view_marker, compareDataList)
//        chartView.marker = marker
        chartView.invalidate()
    }

    private fun setHighLightedValues(highlight: java.util.ArrayList<Highlight?>) {
        draggableView?.removeAllViews()
        highlight.forEach {
            val view =
                LayoutInflater.from(context)
                    .inflate(R.layout.view_marker_compare, compareRootLayout, false)
            val tvContent = view.findViewById<TextView>(R.id.tvContent)
            val entryData = chartView.data.getEntryForHighlight(it)
            tvContent.text = getFormattedDateForXAxis(entryData.x) + " : " + entryData.y
            tvContent.compoundDrawables[0].setTint(ContextCompat.getColor(context!!, entryData?.data as Int))
            draggableView.addView(view)
        }
    }

    fun List<Float?>?.closestValue(value: Float) = this?.minBy { abs(value - it!!) }


    companion object {
        const val TAG_DATE_RANGE = "open_date_range_picker"
        const val TAG_COMPARE_READING = "open_compare_readings"
        fun newInstance(toComparedList: List<ToBeCompared?>?): ScoutCompareFragment {
            val fragment = ScoutCompareFragment()
            fragment.toComparedList = toComparedList?.toMutableList()
            return fragment
        }
    }
}