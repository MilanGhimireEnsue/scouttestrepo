package com.industrialsmart.scoutis.view.dashboard.map


import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.base.BaseFragment
import com.industrialsmart.scoutis.base.ToolbarNavActivity
import com.industrialsmart.scoutis.models.ScoutMarkerItem
import com.industrialsmart.scoutis.models.ScoutModel
import com.industrialsmart.scoutis.utils.extensions.hide
import com.industrialsmart.scoutis.utils.extensions.show
import com.industrialsmart.scoutis.utils.view.CustomMarkerRenderer
import com.industrialsmart.scoutis.view.dashboard.DashBoardActivity
import com.industrialsmart.scoutis.view.scout.ScoutActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_i_s_maps.*
import org.jetbrains.anko.startActivity


class ISMapsFragment : BaseFragment(), ClusterManager.OnClusterItemClickListener<ScoutMarkerItem?>,
    ClusterManager.OnClusterClickListener<ScoutMarkerItem?> {

    override val layoutId = R.layout.fragment_i_s_maps

    private var mGoogleMap: GoogleMap? = null
    private var scoutList: List<ScoutModel?>? = null
    private var selectedScout: ScoutModel? = null
    private var mClusterManager: ClusterManager<ScoutMarkerItem?>? = null
    private var selectedScoutItemFromScoutScreen: ScoutModel? = null //if location is clicked from ScoutActivity

    //for lifecycle
    private var isVisibleToUser: Boolean = false
    private var isStarted = false

    override fun initView() {
        super.initView()
        mDisposable = CompositeDisposable()
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
        closeIcon.setOnClickListener {
            selectedScout = null
            scoutDetailContainer.hide()
        }
        scoutDetailContainer.setOnClickListener {
            if (selectedScout == null)
                return@setOnClickListener
            if (selectedScoutItemFromScoutScreen == null)
                context?.startActivity<ScoutActivity>(
                    ScoutActivity.EXTRA_SCOUT_DATA to selectedScout,
                    ScoutActivity.EXTRA_SCOUT_CHART_TYPE to ScoutActivity.Companion.SCOUT_CHART_TYPE.SCOUT_CHART,
                    ScoutActivity.EXTRA_NAVIGATION_FROM_MAP to true
                )
        }
        searchScoutButton.setOnClickListener {
            val scoutSearchFragment =
                scoutList?.let { it1 ->
                    ScoutSearchFragment.newInstance(it1) {
                        handleMarkerClick(it)
                    }
                }
            scoutSearchFragment?.show(childFragmentManager, "TAG_SCOUT_SEARCH")
        }
        backButton.setOnClickListener {
            activity?.onBackPressed()
        }
    }

    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        mGoogleMap = googleMap
        mGoogleMap?.uiSettings?.isZoomControlsEnabled = true
        mClusterManager = ClusterManager(context, mGoogleMap)
        mGoogleMap?.setOnCameraIdleListener(mClusterManager)
        mGoogleMap?.setOnMarkerClickListener(mClusterManager)
        mClusterManager!!.renderer = CustomMarkerRenderer(context, mGoogleMap!!, mClusterManager!!)
        mClusterManager!!.setOnClusterItemClickListener(this)
        mClusterManager!!.setOnClusterClickListener(this)
        requestScoutList()
        mapTypeSwitch.setOnClickedButtonListener {
            //update map  mode
            if (it == 0)
                mGoogleMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
            else if (it == 1)
                mGoogleMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE
        }
    }

    private fun requestScoutList() {
        if (activity is DashBoardActivity) {
            scoutList = (activity as DashBoardActivity).getScoutList()
            handleScoutList(scoutList)
        } else {
            //if navigating from scout page
            backButton.show()
            scoutList = listOf(selectedScoutItemFromScoutScreen)
            handleScoutList(scoutList)
//            showProgressDialog()
//            mDisposable?.add(
//                dataSource.getScoutsList().observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io())
//                    .subscribe({
//                        dismissProgressDialog()
//                        scoutList = it
//                        handleScoutList(scoutList)
//                    }, {
//                        dismissProgressDialog()
//                    })
//            )
        }
    }


    private fun handleScoutList(scoutListResponse: List<ScoutModel?>?) {
        var isMapCentered = false
        scoutListResponse?.forEachIndexed { index, it ->
            if (it?.coordinateLatitude != null && it.coordinateLongitude != null) {
                mClusterManager?.addItem(ScoutMarkerItem(it))
                if (!isMapCentered && (it.coordinateLatitude != 0.0 && it.coordinateLongitude != 0.0)) {
                    mGoogleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.coordinateLatitude!!, it.coordinateLongitude!!), 19F))
                    isMapCentered = true
                }
            }
        }
        if (selectedScoutItemFromScoutScreen != null)
            handleMarkerClick(selectedScoutItemFromScoutScreen)
    }

    override fun onClusterItemClick(item: ScoutMarkerItem?): Boolean {
        handleMarkerClick(item?.scoutModel)
        return true
    }

    override fun onClusterClick(cluster: Cluster<ScoutMarkerItem?>?): Boolean {
        mGoogleMap?.animateCamera(
            mGoogleMap?.cameraPosition?.zoom?.plus(2.0f)?.let {
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        cluster?.items?.firstOrNull()?.scoutModel?.coordinateLatitude!!,
                        cluster.items?.firstOrNull()?.scoutModel?.coordinateLongitude!!
                    ), it
                )
            }
        )
        return true
    }

    private fun handleMarkerClick(scoutItem: ScoutModel?) {
        selectedScout = scoutItem
        mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(scoutItem?.coordinateLatitude!!, scoutItem.coordinateLongitude!!), 19F))
        showMarkerInfo(scoutItem)
    }

    private fun showMarkerInfo(scoutItem: ScoutModel?) {
        scoutDetailContainer.show()
        scoutTitle.text = scoutItem?.title
        scoutIdentifier.text = scoutItem?.code
        scoutDateTime.text = scoutItem?.dateTime
    }

    //if location is clicked from ScoutActivity
    fun openScoutInfo(scoutItem: ScoutModel?) {
        selectedScoutItemFromScoutScreen = scoutItem
        if (mGoogleMap != null) {
            handleMarkerClick(selectedScoutItemFromScoutScreen)
        }
    }

    override fun onResume() {
        super.onResume()
        if (activity is ToolbarNavActivity)
            (activity as ToolbarNavActivity).hideToolbar()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (activity is ToolbarNavActivity)
            (activity as ToolbarNavActivity).showToolbar()
    }

    companion object {
        fun newInstance(scoutItem: ScoutModel? = null): ISMapsFragment {
            val fragment = ISMapsFragment()
            fragment.selectedScoutItemFromScoutScreen = scoutItem
            return fragment
        }
    }
}