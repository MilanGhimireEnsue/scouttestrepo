package com.industrialsmart.scoutis

import android.content.Context
import com.industrialsmart.scoutis.service.ApiService
import com.industrialsmart.scoutis.datasource.LocalDataSource
import com.industrialsmart.scoutis.datasource.RemoteDataSource
import com.industrialsmart.scoutis.interfaces.DataSource
import com.industrialsmart.scoutis.utils.Constants

class DataRepo(context: Context) {
    lateinit var dataSource: DataSource

    init {
        if (Constants.isLocalDataSource)
            dataSource = LocalDataSource(context)
        else
            dataSource = RemoteDataSource(ApiService.create(context))
    }
}