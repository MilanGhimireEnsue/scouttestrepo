package com.industrialsmart.scoutis.service

import android.app.Dialog
import android.content.Context
import android.net.Uri
import android.webkit.WebView
import android.webkit.WebViewClient
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.listener.AzureLoginResponseListener
import com.industrialsmart.scoutis.utils.Constants
import com.industrialsmart.scoutis.utils.d
import java.net.URI
import java.net.URLEncoder

class LoginService(var context: Context) {

    private var loginUrlFormat =
        "https://login.microsoftonline.com/14b0c004-f559-4cab-a48d-1be2fddb4dea/oauth2/authorize?" +
                "response_type=id_token" +
                "&client_id={client_id}" +
                "&redirect_uri={redirect_uri}" +
                "&scope=openid" +
                "&nonce={nonce}" +
                "&prompt=login"

    /**
     * Get formatted url
     */
    private fun getUrl(): String {
        var ret = this.loginUrlFormat.replace("{tenant}", Constants.AZURE_TENANT, true)
        ret =
            ret.replace("{client_id}", URLEncoder.encode(Constants.AZURE_CLIENT_ID, "UTF-8"), true)
        ret = ret.replace(
            "{redirect_uri}",
            URLEncoder.encode(Constants.AZURE_REDIRECT_URI, "UTF-8"),
            true
        )
        ret = ret.replace("{nonce}", URLEncoder.encode(Constants.AZURE_NONCE_ID, "UTF-8"))
        return ret
    }

    /**
     * Open microsoft azure login modal box
     */
    fun login(listener: AzureLoginResponseListener? = null) {
        val dialog = Dialog(context, android.R.style.Theme_DeviceDefault_Light_NoActionBar)
        dialog.setContentView(R.layout.modal_external_login)
        val webView = dialog.findViewById<WebView>(R.id.externalWebView)
        dialog.window?.attributes?.windowAnimations = R.style.AppBaseTheme_WindowsAnimation
        val client = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url?.startsWith(Constants.AZURE_REDIRECT_URI) == true) {
                    if (url.contains("id_token")) {
                        val splits = url.split("=")
                        listener?.onSuccess(splits[1].removeSuffix("&session_state"))
                    } else {
                        listener?.onError("Error")
                    }
                    dialog.dismiss()
                    return true
                }
                return false
            }

            override fun onLoadResource(view: WebView?, url: String?) {
                d(url)
            }
        }
        webView.webViewClient = client
        @SuppressWarnings
        webView.settings.javaScriptEnabled = true
        d(getUrl())
        webView.loadUrl(getUrl())
        dialog.show()
    }
}