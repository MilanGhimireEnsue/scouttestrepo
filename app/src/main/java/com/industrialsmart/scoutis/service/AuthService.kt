package com.industrialsmart.scoutis.service

import android.content.Context
import com.industrialsmart.scoutis.BuildConfig
import com.industrialsmart.scoutis.MyApplication
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.utils.Constants

fun Context.getFreshToken(): String? {
    try {
        val context = this
        val mMultipleAccountApp = (context.applicationContext as MyApplication).getMultipleAccount()!!
        val scopes = arrayOf(Constants.SCOPES_DEV)
        val result = mMultipleAccountApp.acquireTokenSilent(
            scopes,
            (context.applicationContext as MyApplication).getLoginAccount()!!,
            mMultipleAccountApp.configuration.defaultAuthority.authorityURL.toString()
        )
        if (result == null)
            return null
        return result.accessToken
    } catch (ex: Exception) {
        return null
    }
}

fun getConfigRawFile(): Int {
    if (BuildConfig.BUILD_TYPE.equals("release"))
        return R.raw.msal_config_prod
    else
        return R.raw.msal_config
}

fun getScopes(): Array<String>{
    if (BuildConfig.BUILD_TYPE.equals("release"))
        return arrayOf(Constants.SCOPES_PROD)
    else
        return arrayOf(Constants.SCOPES_DEV)
}