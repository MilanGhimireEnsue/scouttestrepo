package com.industrialsmart.scoutis.service

import android.content.Context
import com.google.gson.GsonBuilder
import com.industrialsmart.scoutis.BuildConfig
import com.industrialsmart.scoutis.models.*
import com.industrialsmart.scoutis.utils.Constants
import com.industrialsmart.scoutis.utils.extensions.logoutApplication
import io.reactivex.Completable
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ApiService {

    @GET("api/app/scout")
    fun getScoutsList(): Single<List<ScoutItemResponse?>>

    @GET("api/app/sensor")
    fun getSensorsList(): Single<List<SensorItemResponse?>>

    @GET("api/app/readingtype")
    fun getReadingTypeList(): Single<List<ReadingTypeItem?>>

    @GET("api/app/sensordata")
    fun getSensorData(
        @Query("whenFrom") whenFrom: String?,
        @Query("whenTo") whenTo: String?,
        @Query("readingTypeId") readingTypeId: String?,
        @Query("sensorId") sensorId: String?
    ): Single<List<ReadingDataItem?>>

    @GET("api/app/user")
    fun getUserData(): Single<UserDataResponse?>

    @POST("api/app/user")
    fun postUserData(@Body userDataResponse: UserDataResponse?): Single<UserDataResponse?>

    @POST("api/app/scout")
    fun postScoutInfo(@Body scoutModel: List<ScoutModel?>): Completable

    @POST("api/app/sensor")
    fun postSensorInfo(@Body sensorList: List<Sensor?>): Completable

    @GET("api/app/subject")
    fun getSubjectList(): Single<List<AppSubjectItem?>>

    @GET("api/app/season")
    fun getGduList(): Single<List<GDUResponse?>>

    @POST("api/app/season")
    fun requestAddGDU(@Body gduItem: GDUResponse?): Single<GDUResponse?>

    @DELETE("api/app/season")
    fun requestGDUDelete(@Query("id") id: String?): Single<Boolean?>

    @GET("api/app/region")
    fun getRegionList(): Single<List<AppRegionItem?>>

    @GET("api/app/occasion")
    fun getOccasionList(): Single<List<OccasionItem?>>

    @POST("api/app/occasion")
    fun requestAddOccasion(@Body occasionItem: OccasionItem?): Single<OccasionItem?>?

    @DELETE("api/app/occasion")
    fun requestDeleteOccasion(@Query("id") id: String?): Single<Boolean?>

    @GET("api/app/periodnotification")
    fun getNotificationList(): Single<List<NotificationItem?>>

    @GET("api/app/period")
    fun getNotificationSettingList(): Single<List<NotificationSettingItem?>>

    @POST("api/app/periodnotification")
    fun postNotificationRead(@Body notificationItem: NotificationItem?): Completable

    @POST("api/app/periodnotification/confirm")
    fun postBulkNotificationRead(@Query("ids") ids: String?): Completable

    @DELETE("api/app/period")
    fun requestDeleteNotificationSetting(@Query("id") id: String?): Single<Boolean?>

    @POST("api/app/period")
    fun requestAddNotificationSetting(@Body notificationSettingItem: NotificationSettingItem?): Single<NotificationSettingItem?>

    @POST("api/app/userdevice/activate")
    fun requestPostFCMToken(@Body fcmTokenPostData: FCMTokenPostData?): Completable

    @GET("api/app/periodnotification/count?isconfirmed=false")
    fun getUnreadNotificationCount(): Single<Int?>

    @GET("api/app/knowledgereference")
    fun getReferencesList(): Single<List<ReferenceItem?>>

    @GET("api/sys/metainfo")
    fun getSysInfo(): Single<SysInfoResponse?>


    companion object Factory {
        private val TIMEOUT: Long = 400
        var gson = GsonBuilder()
            .setLenient().create()
        private val builder = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))

        private val httpClient = OkHttpClient.Builder()
        private var retrofit: Retrofit? = null
        fun create(applicationContext: Context): ApiService {
            httpClient.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            httpClient.writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            httpClient.readTimeout(TIMEOUT, TimeUnit.SECONDS)
            //add custom header
            httpClient.addInterceptor(HeaderInterceptor(applicationContext))
            if (BuildConfig.DEBUG) {
                //enable http request log
                httpClient.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            }
            retrofit = builder.client(httpClient.build())
                .build()
            return retrofit!!.create(ApiService::class.java)
        }
    }
}

class HeaderInterceptor(applicationContext: Context) : Interceptor {
    private val mContext: Context = applicationContext
    override fun intercept(chain: Interceptor.Chain?): Response {
        val original = chain?.request()
        // Request customization: add request headers
        val requestBuilder = original?.newBuilder()
            ?.addHeader("Content-Type", "application/json")
            ?.addHeader("ZUMO-API-VERSION", Constants.API_VERSION)
        if (UserInfo.accessToken.isNotEmpty())
            requestBuilder?.header("Authorization", "Bearer ${UserInfo.accessToken}")
        val request = requestBuilder?.build()
        val response: Response = chain?.proceed(request) as Response
        if (response.code() == 401) {
            val newToken = mContext.getFreshToken()
            if (newToken != null) {
                UserInfo.accessToken = newToken
                requestBuilder?.removeHeader("Authorization")
                requestBuilder?.addHeader("Authorization", "Bearer ${UserInfo.accessToken}")
                return chain.proceed(requestBuilder?.build())
            } else {
                mContext.logoutApplication()
            }
        }
        return response
    }
}