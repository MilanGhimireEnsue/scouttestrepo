package com.industrialsmart.scoutis.service

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.industrialsmart.scoutis.models.UserInfo
import com.industrialsmart.scoutis.utils.d
import com.industrialsmart.scoutis.utils.extensions.createNotification

class ScoutMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        d("okhttp remoteMessage received $remoteMessage")
        if (remoteMessage.data?.isNotEmpty()) {
            val data = remoteMessage.data
            d("okhttp data received ${remoteMessage.data}")
            d("okhttp data received ${data}")
            val title = data["title"]
            val message = data["message"]
            var image: String? = null
            var dataProviderId: String? = null
            var dataProviderScoutId: String? = null
            if (data.containsKey("image"))
                image = data["image"]
            if (data.containsKey("dataProviderId"))
                dataProviderId = data["dataProviderId"]
            if (data.containsKey("dataProviderScoutId"))
                dataProviderScoutId = data["dataProviderScoutId"]
            createNotification(message, title, image)
        }
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        d("okhttp new token $p0")
        UserInfo.fcmToken = p0
        UserInfo.fcmTokenSentStatus = false
    }
}