package com.industrialsmart.scoutis.service

import android.content.Context
import com.industrialsmart.scoutis.interfaces.DataSource
import com.industrialsmart.scoutis.models.FCMTokenPostData
import com.industrialsmart.scoutis.models.UserInfo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FCMIdRegistration {

    companion object {
        fun requestPostFCMToken(dataSource: DataSource?, fcmTokenPostData: FCMTokenPostData?) {
            if (dataSource == null)
                return
            dataSource.requestPostFCMToken(fcmTokenPostData)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    UserInfo.fcmTokenSentStatus = true
                }, {

                })
        }
    }
}