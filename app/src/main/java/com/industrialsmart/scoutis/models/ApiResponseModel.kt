package com.industrialsmart.scoutis.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.industrialsmart.scoutis.utils.d
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.LocalDate
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class ScoutItemResponse(
    @SerializedName("assetCode")
    var assetCode: String = "",
    @SerializedName("assetName")
    var assetName: String = "",
    @SerializedName("assetOwnershipId")
    var assetOwnershipId: String = "",
    @SerializedName("code")
    var code: String = "",
    @SerializedName("content")
    var content: String = "",
    @SerializedName("coordinateAltitude")
    var coordinateAltitude: Double?,
    @SerializedName("coordinateLatitude")
    var coordinateLatitude: Double?,
    @SerializedName("coordinateLongitude")
    var coordinateLongitude: Double?,
    @SerializedName("description")
    var description: String? = "",
    @SerializedName("healthSensorId")
    var healthSensorId: String = "",
    @SerializedName("holdingId")
    var holdingId: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("notificationConfirmationCount")
    var notificationConfirmationCount: Int?
) : Parcelable

data class SensorItemResponse(
    @SerializedName("assetCode")
    var assetCode: String = "",
    @SerializedName("assetName")
    var assetName: String = "",
    @SerializedName("assetOwnershipId")
    var assetOwnershipId: String = "",
    @SerializedName("code")
    var code: String = "",
    @SerializedName("content")
    var content: Any = "",
    @SerializedName("defaultReadingTypeId")
    var defaultReadingTypeId: String = "",
    @SerializedName("description")
    var description: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("scoutCode")
    var scoutCode: String = "",
    @SerializedName("scoutId")
    var scoutId: String = "",
    @SerializedName("scoutName")
    var scoutName: String = "",
    @SerializedName("statistics")
    var statistics: String = ""
)

data class ReadingTypeItem(
    @SerializedName("categoryCode")
    var categoryCode: String? = null,
    @SerializedName("code")
    var code: String? = null,
    @SerializedName("content")
    var content: Any? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("id")
    var readingTypeId: String? = null,
    @SerializedName("levelId")
    var levelId: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("unitSymbol")
    var unitSymbol: String? = null
)

data class ReadingDataItem(
    @SerializedName("value")
    var value: Double?,
    @SerializedName("when")
    var whenX: String?
) {
    fun getUnixTimestampForDate(): Long {
        try {
            val dateTimeFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            dateTimeFormat.timeZone = TimeZone.getTimeZone("UTC")
            return dateTimeFormat.parse(whenX).time
        } catch (ex: Exception) {
            val dateTimeFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            dateTimeFormat.timeZone = TimeZone.getTimeZone("UTC")
            return dateTimeFormat.parse(whenX).time
        }
    }
}

data class UserDataResponse(
    @SerializedName("authUsername")
    var authUsername: String? = null,
    @SerializedName("code")
    var code: String? = null,
    @SerializedName("contactInfo")
    var contactInfo: Any? = null,
    @SerializedName("content")
    var content: Any? = null,
    @SerializedName("customerId")
    var customerId: String? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("languageTag")
    var languageTag: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("nameInfo")
    var nameInfo: Any? = null,
    @SerializedName("regionId")
    var regionId: String? = null,
    @SerializedName("userDevices")
    var userDevices: Any? = null,
    @SerializedName("userGroups")
    var userGroups: Any? = null,
    @SerializedName("userSettingsInfo")
    var userSettingsInfo: UserSettingsInfo? = null,
    @SerializedName("userTypeId")
    var userTypeId: String? = null
)

data class UserSettingsInfo(
    @SerializedName("languageTag")
    var languageTag: String?,
    @SerializedName("regions")
    var regions: List<IdCodeItem>?,
    @SerializedName("scoutSettings")
    var scoutSettings: MutableList<ScoutSensorSetting>?,
    @SerializedName("sensorSettings")
    var sensorSettings: MutableList<ScoutSensorSetting>?,
    @SerializedName("subjects")
    var subjects: List<IdCodeItem>?,
    @SerializedName("units")
    var units: List<Unit>?
)

data class IdCodeItem(
    @SerializedName("code")
    var code: String?,
    @SerializedName("id")
    var id: String?,
    @SerializedName("name")
    var name: String?
)

data class ScoutSensorSetting(
    @SerializedName("id")
    var id: String?,
    @SerializedName("value")
    var value: Value?
)

data class Unit(
    @SerializedName("name")
    var name: String?,
    @SerializedName("value")
    var value: String?
)

data class Value(
    @SerializedName("importance")
    var importance: String?
)

data class ValueX(
    @SerializedName("importance")
    var importance: String?
)

data class GDUResponse(
    @SerializedName("code")
    var code: String? = null,
    @SerializedName("content")
    var content: Any? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("holdingId")
    var holdingId: String? = null,
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("isHoldingDefaultSeason")
    var isHoldingDefaultSeason: Boolean? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("statistics")
    var statistics: Any? = null,
    @SerializedName("subjectBaseTemperature")
    var subjectBaseTemperature: Double? = null,
    @SerializedName("subjectId")
    var subjectId: String? = null,
    @SerializedName("subjectWhenHarvesting")
    var subjectWhenHarvesting: String? = null,
    @SerializedName("subjectWhenPlanting")
    var subjectWhenPlanting: String? = null,
    @SerializedName("whenBegin")
    var whenBegin: String? = null,
    @SerializedName("whenEnd")
    var whenEnd: Any? = null,
    @SerializedName("subjectTypeId")
    var subjectTypeId: String? = null
)

data class AppSubjectItem(
    @SerializedName("baseTemperature")
    var baseTemperature: Double?,
    @SerializedName("code")
    var code: String?,
    @SerializedName("content")
    var content: Any?,
    @SerializedName("description")
    var description: Any?,
    @SerializedName("id")
    var id: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("overrideAudienceScope")
    var overrideAudienceScope: Any?,
    @SerializedName("overrideBaseId")
    var overrideBaseId: Any?,
    @SerializedName("overrideOwnerId")
    var overrideOwnerId: Any?,
    @SerializedName("subjectTypeId")
    var subjectTypeId: String?
)

data class AppRegionItem(
    @SerializedName("code")
    var code: String?,
    @SerializedName("content")
    var content: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("id")
    var id: String?,
    @SerializedName("name")
    var name: String?
)

@Parcelize
data class OccasionItem(
    @SerializedName("audienceScope")
    var audienceScope: String? = null,
    @SerializedName("code")
    var code: String? = null,
    @SerializedName("colour")
    var colour: String? = null,
    @SerializedName("content")
    var content: String? = null,
    @SerializedName("customerId")
    var customerId: String? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("duration")
    var duration: Int? = null,
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("isAllDay")
    var isAllDay: Boolean? = null,
    @SerializedName("isPrivate")
    var isPrivate: Boolean? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("occasionTypeId")
    var occasionTypeId: String? = null,
    @SerializedName("ownerId")
    var ownerId: String? = null,
    @SerializedName("priority")
    var priority: Int? = null,
    @SerializedName("regionId")
    var regionId: String? = null,
    @SerializedName("when")
    var startDateTime: String? = null,
    @SerializedName("where")
    var `where`: String? = null,
    var endDateTime: String? = null,
    var eventDay: LocalDate? = null
) : Parcelable


data class NotificationItem(
    @SerializedName("confirmedWhen")
    var confirmedWhen: Any? = null,
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("periodEventDataProviderCode")
    var periodEventDataProviderCode: String? = null,
    @SerializedName("periodEventDataProviderId")
    var periodEventDataProviderId: String? = null,
    @SerializedName("periodEventDataProviderName")
    var periodEventDataProviderName: String? = null,
    @SerializedName("periodEventDataProviderScoutCode")
    var periodEventDataProviderScoutCode: String? = null,
    @SerializedName("periodEventDataProviderScoutId")
    var periodEventDataProviderScoutId: String? = null,
    @SerializedName("periodEventDataProviderScoutName")
    var periodEventDataProviderScoutName: String? = null,
    @SerializedName("periodEventDataProviderTypeCode")
    var periodEventDataProviderTypeCode: String? = null,
    @SerializedName("periodEventId")
    var periodEventId: String? = null,
    @SerializedName("periodEventIsPeriodNotificationConfirmed")
    var periodEventIsPeriodNotificationConfirmed: Boolean? = null,
    @SerializedName("periodEventLastConfirmedPeriodNotificationConfirmedWhen")
    var periodEventLastConfirmedPeriodNotificationConfirmedWhen: String? = null,
    @SerializedName("periodEventLastConfirmedPeriodNotificationId")
    var periodEventLastConfirmedPeriodNotificationId: String? = null,
    @SerializedName("periodEventLastConfirmedPeriodNotificationSentUserDeviceId")
    var periodEventLastConfirmedPeriodNotificationSentUserDeviceId: String? = null,
    @SerializedName("periodEventReadingTypeCode")
    var periodEventReadingTypeCode: String? = null,
    @SerializedName("periodEventReadingTypeId")
    var periodEventReadingTypeId: String? = null,
    @SerializedName("periodEventReadingTypeName")
    var periodEventReadingTypeName: String? = null,
    @SerializedName("periodEventSeverity")
    var periodEventSeverity: Int? = null,
    @SerializedName("periodEventWhen")
    var periodEventWhen: String? = null,
    @SerializedName("periodSubtitle")
    var periodSubtitle: String? = null,
    @SerializedName("periodTemplateCode")
    var periodTemplateCode: String? = null,
    @SerializedName("periodTemplateId")
    var periodTemplateId: String? = null,
    @SerializedName("periodTemplateName")
    var periodTemplateName: String? = null,
    @SerializedName("periodTitle")
    var periodTitle: String? = null,
    @SerializedName("sentUserDeviceCode")
    var sentUserDeviceCode: Any? = null,
    @SerializedName("sentUserDeviceId")
    var sentUserDeviceId: Any? = null,
    @SerializedName("sentUserDeviceName")
    var sentUserDeviceName: Any? = null,
    @SerializedName("sentWhen")
    var sentWhen: Any? = null,
    @SerializedName("text")
    var text: String? = null,
    @SerializedName("userId")
    var userId: String? = null
)

data class NotificationSettingItem(
    @SerializedName("audienceScope")
    var audienceScope: String? = null,
    @SerializedName("dataProviderId")
    var dataProviderId: String? = null,
    @SerializedName("dataProviderTypeCode")
    var dataProviderTypeCode: String? = null,
    @SerializedName("direction")
    var direction: Int? = null,
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("periodSubtitle")
    var periodSubtitle: Any? = null,
    @SerializedName("periodTemplateCode")
    var periodTemplateCode: String? = null,
    @SerializedName("periodTemplateId")
    var periodTemplateId: String? = null,
    @SerializedName("periodTemplateIsGeneric")
    var periodTemplateIsGeneric: Boolean? = null,
    @SerializedName("periodTemplateName")
    var periodTemplateName: String? = null,
    @SerializedName("periodTitle")
    var periodTitle: String? = null,
    @SerializedName("readingTypeId")
    var readingTypeId: String? = null,
    @SerializedName("referenceDataProviderId")
    var referenceDataProviderId: Any? = null,
    @SerializedName("severity")
    var severity: Int? = null,
    @SerializedName("subjectId")
    var subjectId: Any? = null,
    @SerializedName("targetId")
    var targetId: String? = null,
    @SerializedName("targetTypeCode")
    var targetTypeCode: String? = null,
    @SerializedName("tolerance")
    var tolerance: Double? = null,
    @SerializedName("watchBegin")
    var watchBegin: String? = null,
    @SerializedName("watchEnd")
    var watchEnd: Any? = null,
    @SerializedName("watchMax")
    var watchMax: Double? = null,
    @SerializedName("watchMin")
    var watchMin: Double? = null,
    @SerializedName("watchText")
    var watchText: String? = null,
    var scoutItem: ScoutModel? = null,
    var sensorItem: Sensor? = null,
    var readingTypeItem: ReadingTypeItem? = null
)

data class FCMTokenPostData(
    @SerializedName("Code") var code: String?,
    @SerializedName("Key") var key: String?,
    @SerializedName("Name") var name: String? = "Android Device",
    @SerializedName("TypeName") var typeName: String? = "Android",
    @SerializedName("isActive") var isActive: Boolean? = true
)

data class ReferenceItem(
    @SerializedName("code")
    var code: String?,
    @SerializedName("content")
    var content: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("id")
    var id: String?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("overrideAudienceScope")
    var overrideAudienceScope: Any?,
    @SerializedName("overrideBaseId")
    var overrideBaseId: Any?,
    @SerializedName("overrideOwnerId")
    var overrideOwnerId: Any?
)

data class SysInfoResponse(
    @SerializedName("version")
    var version: String?,
    @SerializedName("versionDescription")
    var versionDescription: String?,
    @SerializedName("versionDbSchemaLastMigration")
    var versionDbSchemaLastMigration: String?
)