package com.industrialsmart.scoutis.models

import android.os.Parcelable
import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.enumpref.enumValuePref
import com.chibatching.kotpref.gsonpref.GsonPref
import com.chibatching.kotpref.gsonpref.gsonNullablePref
import com.chibatching.kotpref.gsonpref.gsonPref
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.industrialsmart.scoutis.R
import com.industrialsmart.scoutis.view.scout.ScoutActivity
import com.microsoft.identity.client.IAccount
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat

object UserInfo : KotprefModel() {
    var accessToken by stringPref()
    var loginStatus by booleanPref(default = false)
    var userData by gsonNullablePref(UserDataResponse())
    var fcmToken by stringPref()
    var fcmTokenSentStatus by booleanPref(false)
    var loginIAccout by stringPref()
}

object UserSetting : KotprefModel() {
    var temperatureUnit by enumValuePref(TemperatureUnit.CELSIUS)
    var soilECUnit by enumValuePref(SoilECUnit.USCM)
    var visibilityMode by enumValuePref(VisibilityMode.ALL)
}


@Parcelize
data class ScoutModel(
    @SerializedName("id") var id: String = "",
    @SerializedName("name") var title: String = "",
    @SerializedName("Code") var code: String = "",
    @SerializedName("DateTime") var dateTime: String = "",
    @SerializedName("Sensors") var sensors: List<Sensor>? = null,
    @SerializedName("isActive") var isActive: Boolean? = true,
    @SerializedName("notificationConfirmationCount") var notificationConfirmationCount: Int? = null,
    @SerializedName("coordinateLatitude") var coordinateLatitude: Double? = null,
    @SerializedName("coordinateLongitude") var coordinateLongitude: Double? = null
) : Parcelable

@Parcelize
data class Sensor(
    @SerializedName("id") var id: String = "",
    @SerializedName("name") var title: String = "",
    @SerializedName("Code") var code: String = "",
    @SerializedName("DateTime") var dateTime: String = "",
    @SerializedName("ScoutId") var scoutId: String = "",
    @SerializedName("description") var description: String? = "",
    @SerializedName("Readings") var readings: List<SensorReading>? = null,
    @SerializedName("isActive") var isActive: Boolean? = true
) : Parcelable

@Parcelize
data class SensorReading(
    @SerializedName("Name") var name: String? = "",
    @SerializedName("Value") var value: String? = "",
    @SerializedName("Unit") var unit: String? = "",
    @SerializedName("Code") var code: String? = "",
    @SerializedName("DataSet") var dataSet: List<SensorDataSet>? = null,
    var isSelected: Boolean = false,
    var foreGroundTint: Int = R.color.colorForegroundDarkest
) : Parcelable

@Parcelize
data class SensorDataSet(
    @SerializedName("DateTime") var dateTime: String = "",
    @SerializedName("Value") var value: Double = 0.0
) : Parcelable {
    fun getUnixTimestampForDate(): Long {
        // TODO: Fix the issue of invalid date parser
        return SimpleDateFormat("yyyy-MM-dd").parse(dateTime).time
    }
}

data class GetSensorDataRequest(
    @SerializedName("whenFrom") var whenFrom: String?,
    @SerializedName("whenTo") var whenTo: String?,
    @SerializedName("readingTypeId") var readingTypeId: String?,
    @SerializedName("categoryCode") var categoryCode: String?,
    @SerializedName("sensorId") var sensorId: String?
)

data class CompareSensorData(
    var compareType: String?,
    var comparableSensorList: List<ComparableSensor?>,
    var isHidden: Boolean = false
)

data class ComparableSensor(
    var sensorName: String?,
    var sensorId: String?,
    var comparableReadingType: List<ToBeCompared?>,
    var isHidden: Boolean = false
)

data class ToBeCompared(
    var sensorName: String?,
    var sensorId: String?,
    var comparableReadingType: ReadingTypeItem?,
    var foreGroundTint: Int? = null,
    var isChecked: Boolean = false,
    var scoutChartType: ScoutActivity.Companion.SCOUT_CHART_TYPE = ScoutActivity.Companion.SCOUT_CHART_TYPE.SCOUT_CHART
)

@Parcelize
data class GDUDataItem(
    var id: String?,
    var name: String?,
    var code: String?,
    var description: String?,
    var subjectWhenHarvesting: String?,
    var subjectWhenPlanting: String?,
    var subjectId: String?,
    var holdingSensorId: String?,
    var rawScoutData: ScoutItemResponse?,
    var subjectTypeId: String?
) : Parcelable

data class GDUCategoryItem(
    var id: String?,
    var name: String?
)