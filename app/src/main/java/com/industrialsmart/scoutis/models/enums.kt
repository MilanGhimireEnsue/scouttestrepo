package com.industrialsmart.scoutis.models

enum class ViewType {
    SCOUT_SETTING, GDU_LISTING, ADD_GDU, USER_SETTING, CROP_OF_INTEREST, REGION_OF_INTEREST, ADD_EVENT, NOTIFICATION_SETTING, ADD_NOTIFICATION_SETTING, REFERENCE, NOTIFICATION,WEB_VIEW, APP_INFO, MAP_WITH_SCOUT
}

enum class TemperatureUnit(val unitCode: String?) {
    CELSIUS("C"), FAHRENHEIT("F")
}

enum class SoilECUnit(val unitCode: String?) {
    USCM("uS/cm"), DSM("dS/m")
}

enum class VisibilityMode(){
    ALL, ACTIVE_ONLY
}