package com.industrialsmart.scoutis.models

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

data class ScoutMarkerItem(val scoutModel: ScoutModel?) : ClusterItem {
    override fun getSnippet(): String? {
        return ""
    }

    override fun getTitle(): String? {
        return ""
    }

    override fun getPosition(): LatLng {
        return if (scoutModel?.coordinateLatitude != null && scoutModel.coordinateLongitude != null)
            LatLng(scoutModel.coordinateLatitude!!, scoutModel.coordinateLongitude!!)
        else
            LatLng(0.0, 0.0)
    }
}

